/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 *                         ZZZZZZZZZZZZZZ    3333333333      888888888888        000000000
 *                       ZZZZZZZZZZZZZZ    33333333333333  8888888888888888    0000000000000
 *                               ZZZZ                3333  8888        8888  0000         0000
 *                             ZZZZ           333333333      888888888888    0000         0000
 *                           ZZZZ                    3333  8888        8888  0000         0000
 *                         ZZZZZZZZZZZZZZ  33333333333333  8888888888888888    0000000000000
 *                       ZZZZZZZZZZZZZZ      3333333333      888888888888        000000000
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/


#if ! defined MPM_Z380_HEADER_
#define MPM_Z380_HEADER_

#include "datastructs.h"

/* global definitions */
enum ddiridents { ddir_w, ddir_lw, ddir_ib, ddir_iw, ddir_unknown };
enum ddirmodes { ddirmode_w, ddirmode_ibw, ddirmode_iww, ddirmode_ib, ddirmode_lw, ddirmode_iblw, ddirmode_iwlw, ddirmode_iw, ddirmode_unknown, ddirmode_notspecfied };

/* Z380 LDCTL register enumerations */
enum ctlrr { ctlr_sr, ctlr_xsr, ctlr_dsr, ctlr_ysr, ctlr_a, ctlr_hl, ctlr_unknown};


/* globally available functions */

mnemprsrfunc LookupZ380Mnemonic(enum symbols st, const char *id);

#endif
