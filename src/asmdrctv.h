/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/

#if ! defined MPM_ASMDRCTV_HEADER_
#define MPM_ASMDRCTV_HEADER_

#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

/* global variables */
extern short clineno;

/* global functions */
symfunc LookupAsmFunction (const char *ident);
mnemprsrfunc LookupIdentifier(const char *identifier, enum symbols st, const identfunc_t asmident[], size_t totalid);
mnemprsrfunc LookupDirective(enum symbols st, const char *id);

symbol_t *AsmEvalExpr (const char *name, int lineno, const void *expr);

void ALIGN(sourcefile_t *csfile);
void BINARY (sourcefile_t *csfile);
void CPU(sourcefile_t *csfile);
void DEFB (sourcefile_t *csfile);
void DEFC (sourcefile_t *csfile);
void DEFM (sourcefile_t *csfile);
void DEFMZ (sourcefile_t *csfile);
void DEFW (sourcefile_t *csfile);
void DEFP (sourcefile_t *csfile);
void DEFL (sourcefile_t *csfile);
void DEFGROUP (sourcefile_t *csfile);
void DEFVARS (sourcefile_t *csfile);
void DEFS (sourcefile_t *csfile);
void ERROR(sourcefile_t *csfile);
void EQU(sourcefile_t *csfile);
void LINE(sourcefile_t *csfile);
void ExitPass1(sourcefile_t *csfile);
void DeclExternIdent (sourcefile_t *csfile);
void DeclGlobalIdent (sourcefile_t *csfile);
void DeclLibIdent (sourcefile_t *csfile);
void DeclGlobalLibIdent (sourcefile_t *csfile);
void DeclModule (sourcefile_t *csfile);
void DefSym (sourcefile_t *csfile);
void UnDefineSym(sourcefile_t *csfile);
void IFstat (sourcefile_t *csfile);
void ELSEstat (sourcefile_t *csfile);
void ENDIFstat (sourcefile_t *csfile);
void ENDDEFstat (sourcefile_t *csfile);
void EndMacro (sourcefile_t *csfile);
void IncludeFile (sourcefile_t *csfile);
void ListingOn (sourcefile_t *csfile);
void ListingOff (sourcefile_t *csfile);
void ORG (sourcefile_t *csfile);
void Ifstatement (sourcefile_t *csfile, bool interpret);

#endif
