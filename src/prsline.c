/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "config.h"
#include "datastructs.h"
#include "main.h"
#include "options.h"
#include "symtables.h"
#include "modules.h"
#include "pass.h"
#include "asmdrctv.h"
#include "errors.h"
#include "prsline.h"
#include "macros.h"
#include "sourcefile.h"
#include "memfile.h"



/******************************************************************************
                          Global variables
 ******************************************************************************/
const char separators[] = " &\"\';,.({[\\]})+-*/%^=|:~<>!`";

/* text buffers, allocated by AllocateTextBuffers() during startup of Mpm */
char *line = NULL;


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/

static int CheckBaseType(char *ident, int chcount, enum symbols *sym);
static int ParseSeparator(sourcefile_t *csfile, int c);
static int FetchSymAsmFunc(sourcefile_t *csfile, int chindex);
static const char *GetSeparator(sourcefile_t *csfile, int c);
static char *stringrev(char *str);
static void CharToIdent(char *ident, char c, int index);
static int __gcLf (sourcefile_t *csfile);

static const enum symbols ssym[] = {
    space, bin_and, dquote, squote, semicolon, comma, fullstop,
    lparen, lcurly, lexpr, backslash, rexpr, rcurly, rparen, plus, minus, multiply, divi, mod, bin_xor,
    assign, bin_or, bin_nor, bin_not,less, greater, log_not, cnstexpr
};

/* the current CPU-specific line parser and mnemonic lookup */
static cpuassembler_t cpuasm;


static void
CharToIdent(char *ident, char c, int index)
{
    if (index < MAX_NAME_SIZE) {
        ident[index] = c;
    }
}


/* ----------------------------------------------------------------
   Identify Hex-, binary and decimal constants in [ident] of
        #xxxx or $xxxx or 0x or xxxxH (hex format)
        %xxxxx or 0Bxxxxx or xxxxB    (binary format)
        xxxxD                         (decimal format)

        and

   Identify assembler functions as $ (converted to $PC) or $name
   (which is not a legal hex constant)
   ---------------------------------------------------------------- */
static int
CheckBaseType(char *ident, int chcount, enum symbols *sym)
{
    int   i;

    if (ident[0] == '$' && strlen(ident) == 1) {
        /* it's just $, which identifies assembler PC in traditional assemblers */
        strncpy(ident,ASSEMBLERPC, 4);
        *sym = asmfnname;
        return 3; /* ident has become bigger */
    }

    if ( ident[0] == '#' && chcount > 1 ) {
        /* '#' hex constants are evaluated by GetConstant() */
        *sym = hexconst;
        return chcount;
    }

    if ( ident[0] == '%' && chcount > 1 ) {
        /* '%' binary constants are evaluated by GetConstant() */
        *sym = binconst;
        return chcount;
    }

    if (ident[0] == '$') {
        /* evaluate if it's a expression function name (contains non-hex digit) */
        for (i = 1; i < chcount; i++) {
            if (isxdigit (ident[i]) == 0) {
                *sym = asmfnname;
                return chcount;
            }
        }

        /* all characters were valid hex digits in $xxxxx sequence, assume that constant is a hexadecimal */
        *sym = hexconst;
        return chcount;
    }

    /* If it's not a hex digit straight off then reject it */
    if ( !isxdigit(ident[0]) || chcount < 2 ) {
        return chcount;
    }

    /* C style hex number */
    if ( chcount > 2 && strnicmp(ident,"0x",2) == 0 ) {
        /* 0x hex constants are evaluated by GetConstant() */
        *sym = hexconst;
        return chcount;
    }

    /* C style hex number, ambiguous constant 0bxxxH! */
    if ( chcount > 2 && strnicmp(ident,"0b",2) == 0 && ident[chcount-1] == 'H') {
        /* hex constants are evaluated by GetConstant() */
        *sym = hexconst;
        return chcount;
    }

    /* C style binary number */
    if ( chcount > 2 && strnicmp(ident,"0b",2) == 0 ) {
        /* 0b binary constants are evaluated by GetConstant() */
        *sym = binconst;
        return chcount;
    }

    /* Check for this to be a hex constant here */
    for ( i=0; i < chcount; i++ ) {
        if ( !isxdigit(ident[i])  ) {
            break;
        }
    }

    if ( i == (chcount-1) ) {
        /* Convert xxxxH hex constants to $xxxxx */
        if ( toupper(ident[i]) == 'H' ) {
            for ( i = (chcount-1); i >= 0 ; i-- ) {
                ident[i+1] = ident[i];
            }
            ident[0] = '$';
            *sym = hexconst;
            return chcount;
        } else {
            /* If we reached end of hex digits and the last one wasn't a 'h', then something is wrong */
            return chcount;
        }
    }

    /* Check for binary constant (ends in b), abort if it might be another constant type */
    for ( i = 0; i <  chcount ; i++ ) {
        /* allowed chars are digit '0', '1' and separators ' and _ in binary constants */
        if ( ident[i] != '0' && ident[i] != '1' && ident[i] != '\'' && ident[i] != '_' ) {
            break;
        }
    }

    if ( i == (chcount-1) && toupper(ident[i]) == 'B' ) {
        /* Convert xxxxB binary constants to @xxxx constants */
        for ( i = (chcount-1); i >= 0 ; i-- ) {
            ident[i+1] = ident[i];
        }
        ident[0] = '@';
        *sym = binconst;
        return chcount;
    }

    /* Check for decimal (we default to it in anycase.. but */
    for ( i = 0; i <  chcount ; i++ ) {
        if ( !isdigit(ident[i]) ) {
            break;
        }
    }
    if ( i == (chcount-1) && toupper(ident[i]) == 'D' ) {
        *sym = decmconst;
        return chcount-1; /* chop off the 'D' trailing specifier for decimals */
    }

    /* No hex, binary or decimal base types were recognized, return without change */
    return chcount;
}


static char *stringrev(char *str)
{
      char *p1;
      char *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}


/* ----------------------------------------------------------------
    int GetChar (sourcefile_t *csfile)

    Return a character from current (cached) memory source file with
    CR/LF/CRLF parsing capability.

    Handles continuous line '\' marker (skip physical EOL)
    and returns value of escape sequences (\n, \\, \r, \t, \a, \b, \f, \', \")

    '\n' byte is returned if a CR/LF/CRLF variation line feed is found.
    EOL (-1) is returned if <csfile> is NULL
   ---------------------------------------------------------------- */
static int
__gcLf (sourcefile_t *csfile)
{
    int c = MemfGetc(csfile->mf);

    if (c == 13) {
        /* Mac line feed found, poll for MS-DOS / WINDOWS line feed */
        if ( MemfGetc(csfile->mf) != 10) {
            MemfUngetc(csfile->mf);  /* push non-line-feed character back into file */
        }
        c = '\n';    /* always return UNIX line feed for CR or CRLF */
    }

    return c;
}


static int
ParseSeparator(sourcefile_t *csfile, int c)
{
    const unsigned char *fptr;
    int digit;
    int totbindigits = 0;
    int totdigits = 0;

    switch (csfile->sym) {
        case multiply:
            c = GetChar (csfile);
            if (c == '*') {
                csfile->sym = power;    /* '**' */
            } else if (c == '/') {
                /* c-style end-comment, continue parsing after this marker */
                csfile->cstylecomment = false;
                csfile->sym = GetSym(csfile);
            } else {
                /* push this character back for next read */
                MemfUngetc(csfile->mf);
            }
            break;

        case divi:         /* c-style comment begin */
            c = GetChar (csfile);
            if (c == '*') {
                csfile->cstylecomment = true;
                SkipLine (csfile);    /* ignore comment block */
                csfile->sym = GetSym(csfile);
            } else {
                /* push this character back for next read */
                MemfUngetc(csfile->mf);
            }
            break;

        case mod:
            /* evaluate if % is used as a MOD operator or it is a binary constant identifier */
            fptr = MemfTellPtr(csfile->mf);

            digit = GetChar (csfile);
            if (isspace(digit) || GetSeparator(csfile, digit)) {
                /* quick interpretation: if char immediately after '%' is is a white space or a separator, then its a MOD operator */
                csfile->sym = mod;
            } else {
                while ( isdigit(digit) ) {
                    if ((digit - '0' == 0) || (digit - '0' == 1))
                        totbindigits++;
                    else
                        totdigits++;

                    digit = GetChar (csfile);
                    if (isspace(digit) || GetSeparator(csfile, digit)) {
                        break;
                    }
                }
                if (totbindigits >= 2 && totdigits == 0)
                    csfile->sym = binconst; /* typically, a binary constant is specified with two or more binary digits eg %11 or %010101 */
                else
                    /* if its less (or no binary digits), it could at best by interpreted as another constant, eg. %9 or %105 */
                    csfile->sym = mod;
            }

            MemfSeekPtr(csfile->mf, fptr);
            csfile->eol = false; /* EOL might have been set if a ';' was reached evaluating binary constant or MOD operator */
            break;

        case less:         /* '<' */
            c = GetChar (csfile);
            switch (c) {
            case '<':
                csfile->sym = lshift;       /* '<<' */
                break;

            case '>':
                csfile->sym = notequal;    /* '<>' */
                break;

            case '=':
                csfile->sym = lessequal;       /* '<=' */
                break;

            default:
                /* '<' was found, push this character back for next read */
                MemfUngetc(csfile->mf);
                break;
            }
            break;

        case greater:          /* '>' */
            c = GetChar (csfile);
            switch (c) {
            case '>':
                csfile->sym = rshift;       /* '>>' */
                break;

            case '=':
                csfile->sym = greatequal;      /* '>=' */
                break;

            default:
                /* '>' was found, push this character back for next read */
                MemfUngetc(csfile->mf);
                break;
            }
            break;

        default:
            break;
    }

    return c;
}


static const char *
GetSeparator(sourcefile_t *csfile, int c)
{
    const char *instr = strchr (separators, c);

    if (instr != NULL) {
        csfile->sym = ssym[instr - separators]; /* index of found char in separators[] */
        if (csfile->sym == semicolon) {
            /* ';' or '#', ignore comment line, prepare for next line */
            SkipLine (csfile);
            csfile->sym = newline;
        }
    }

    return instr;
}


/*!
 * \brief Fetch a name/label symbol (allow trailing to swallow ':' character)
 * \param csfile
 * \param chindex
 * \return
 */
static int
FetchSymName(sourcefile_t *csfile, int chindex)
{
    int c;

    while (!MemfEof (csfile->mf)) {
        c = GetChar (csfile);
        if ((c == MFEOF) || (iscntrl (c)) || (strchr (separators, c) != NULL)) {
            if ( c != ':' ) {
                MemfUngetc(csfile->mf);   /* puch character back for next read */
            } else {
                csfile->sym = colonlabel;
            }
            return chindex;
        }

        if (!isalnum (c)) {
            if (c != '_') {
                csfile->sym = nil;
                break;
            } else {
                /* underscore in identifier */
                CharToIdent(csfile->ident, '_', chindex);
                chindex++;
            }
        } else {
            CharToIdent(csfile->ident, (char) toupper (c), chindex);
            chindex++;
        }
    }

    return chindex;
}


/*!
 * \brief Fetch a '%' or '@' binary constant (swallow allowed ' and _ separators)
 * \param csfile
 * \param chindex
 * \return
 */
static int
FetchSymBinConst(sourcefile_t *csfile, int chindex)
{
    int c;

    while (true) {
        if (MemfEof (csfile->mf)) {
            return chindex;
        } else {
            c = GetChar (csfile);
            if (c == '0' || c == '1') {
                /* collect binary digit */
                CharToIdent(csfile->ident, (char) toupper (c), chindex);
                chindex++;
                continue;
            }

            if (c == '\'' || c == '_') {
                /* swallow binary constant separators */
                continue;
            }

            if ((c == MFEOF) || (iscntrl (c)) || (strchr (separators, c) != NULL)) {
                /* end of bianry constant reached, puch character back for next read */
                MemfUngetc(csfile->mf);
                break;
            }
        }
    }

    return chindex;
}


/*!
 * \brief An assembler function name was recognized
 * \details include function body as part of name, if it is specified
 * \return total chars collected of assembler function name and arguments
 */
static int
FetchSymAsmFunc(sourcefile_t *csfile, int chindex)
{
    int c;
    const unsigned char *ptr;
    int endbracket = 0;

    ptr = MemfTellPtr(csfile->mf);

    /* skip white space until first real character after function name.. */
    /* check if a [ ] is placed after the function name, and read it as part of the name.. */
    while ( (c = GetChar(csfile)) != MFEOF ) {
        if (!isspace(c))
            break;
    }

    if (c == '(') { endbracket = ')'; }
    if (c == '[') { endbracket = ']'; }

    if (c == '(' || c == '[') {
        CharToIdent(csfile->ident, '[', chindex);
        chindex++;

        /* read function body, collect all valid chars and skip all white spaces until ')' or ']' */
        while ( ((c = GetChar(csfile)) != MFEOF) && (c != endbracket) ) {
            if (!iscntrl(c) && !isspace(c)) {
                CharToIdent(csfile->ident, (char) toupper (c), chindex);
                chindex++;
            }
        }

        CharToIdent(csfile->ident, ']', chindex);
        chindex++;
    } else {
        /* optional function body was not specified */
        /* resume read position, just at byte after function name */
        MemfSeekPtr(csfile->mf, ptr);
    }

    return chindex;
}


static int
FetchSymIdent(sourcefile_t *csfile, int chindex)
{
    int c;

    while (!MemfEof (csfile->mf)) {

        c = GetChar(csfile);
        if ((c != MFEOF) && !iscntrl (c) && (strchr (separators, c) == NULL)) {
            CharToIdent(csfile->ident, (char) toupper (c), chindex);
            chindex++;
        } else {
            MemfUngetc(csfile->mf);   /* puch character back for next read */
            CharToIdent(csfile->ident, 0, chindex);

            /* validate if ident might be a number constant as with a trailing h, d or b */
            chindex = CheckBaseType(csfile->ident, chindex, &csfile->sym);

            if (csfile->sym == asmfnname && LookupAsmFunction (csfile->ident) != NULL) {
                chindex = FetchSymAsmFunc(csfile, chindex);
            }
            break;
        }
    }

    return chindex;
}


/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Set the CPU-specific lineparser and mnemonic lookup functions
 * \param cpuid
 * \param cpulineparser
 * \param cpumnemlookup
 */
void
SetCpuAssembler(enum cpu cpuid, lineparserfunc cpulineparser, cpumnemfunc cpumnemlookup)
{
    cpuasm.identifier = cpuid;
    cpuasm.lpf = cpulineparser;
    cpuasm.cmf = cpumnemlookup;
}


/*!
 * \brief Know which CPU assembler is currently active
 * \return the CPU parser enumeration
 */
enum cpu
CpuAssembler(void)
{
    return cpuasm.identifier;
}


/*!
 * \brief Returns pointer to CPU-specific mnemonic lookup functions
 * \param st symbol type
 * \param id pointer to identifier
 * \return
 */
mnemprsrfunc
LookupCpuMnemonic(enum symbols st, const char *id)
{
    return cpuasm.cmf(st, id);
}


/*!
 * \brief CPU-specific line parser
 * \param csfile the current source file
 * \param interpret parse or skip current line
 */
void
ParseLine (sourcefile_t *csfile, bool interpret)
{
    cpuasm.lpf(csfile, interpret);
}


/*!
 * \brief Parse source code line, defined CPU mnemonic <ident> name
 * \details
    The symbol type must be name, which can be either a directive, a cpu mnemonic
    or a macro.
 *
 * \param csfile the current source file being parsed
 * \param interpret skip or interpret line
 */
void
ParseCpuMnemonic (sourcefile_t *csfile, const bool interpret)
{
    mnemprsrfunc function;
    macro_t *macro;
    sourcefile_t *macroInstance;

    if ((function = LookupCpuMnemonic (csfile->sym, csfile->ident)) != NULL) {
        /* current identifier contains a CPU mnemonic */
        do {
            if (interpret == false) {
                break;
            }

            /* Parse & code generate for specific CPU Mnemonic... */
            function(csfile);

            GetSym(csfile);
            while (csfile->sym == colon) {
                /* ':' after completed instruction mnemonic indicates that another instruction mnemonic may follow */
                GetSym(csfile);
            }

            if (csfile->sym == newline) {
                /* parsing reached end of line after instruction mnemonic or ':' */
                break;
            }

            if (csfile->sym != name) {
                /* the symbol after a ':' can only be another inctruction mnemonic (name) */
                ReportSrcAsmMessage (csfile, "Incomplete instruction syntax");
                break;
            }

        } while ((function = LookupCpuMnemonic (csfile->sym, csfile->ident)) != NULL);
        SkipLine (csfile);
        return;
    }

    /* Mnemonic was not found, try to execute a directive... */
    if ((function = LookupDirective(csfile->sym, csfile->ident)) != NULL) {
        /* current identifier contains a directive */
        if (function == &IFstat) {
            if (interpret == false) {
                SkipLine (csfile);
            }
            Ifstatement (csfile, interpret);
        } else if ((function == &ELSEstat) || (function == &ENDIFstat)) {
            function(csfile);
            SkipLine (csfile);
        } else {
            if (interpret == true) {
                function(csfile);
            }
            SkipLine (csfile);
        }

        return;
    }

    if (interpret == false) {
        SkipLine (csfile);
        return;
    }

    /* Neither Mpm mnemonic or directive was recognized. Is it a macro? */
    if ( (macro = FindMacro (csfile->ident, csfile->module->macros)) != NULL) {
        /* We have a macro, execute it to generate source code output */
        macroInstance = ExecuteMacro(csfile, macro);
        if (macroInstance != NULL) {
            /* focus to new macro source code as the current "file" to be parsed... */
            csfile = macroInstance;
            SourceFilePass1 (csfile);        /* parse source code of macro output */
            csfile = Prevfile (csfile);      /* then get back to current file... */
            csfile->eol = false;
            csfile->sym = newline;
            writeline = false;    /* don't write current source line to listing file (rest of MACRO line) */
        }

        SkipLine (csfile);
        return;
    }

    /* Neither Mpm mnemonic, directive or a macro */
    /* Interpret it as an address label or a constant definition IF the current line */
    /* did not already define a label using .xxx or xxx: syntax */
    if (csfile->labelname == NULL) {
        /* no label yet defined on line, lets interpret this name as a label */
        csfile->labelname = DefineSymbol (csfile, csfile->ident, (symvalue_t) GetStreamCodePC(csfile), SYMADDR | SYMTOUCHED);
        if (csfile->labelname != NULL) {
            AddAddress (csfile->labelname, &addresses);
        }

        /* after a label (or a constant name definition), there might be a directive or an instruction... */
        if (GetSym(csfile) != newline)
            ParseCpuMnemonic(csfile, interpret);
    } else {
        /* A label was already defined on this line - it means this identifer is unknown */
        /* (it is neither a CPU mnemonic or a directive */
        ReportError (csfile, Err_UnknownIdent);
    }

    SkipLine (csfile);
}


/* Copyright (c) 2012, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
* * Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* * Redistributions in binary form must reproduce the above
* copyright notice, this list of conditions and the following
* disclaimer in the documentation and/or other materials provided
* with the distribution.
* * Neither the name of The Linux Foundation nor the names of its
* contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Edited for long integer, by Gunther Strube, 2014.
*/
int ltoasc(long num, char *str, int len, int base)
{
    int i = 0;
    int digit;

    if (len == 0)
        return -1;

    do {
        digit = num % base;
        if (digit < 0xA)
            str[i++] = (char) ('0' + digit);
        else
            str[i++] = (char) ('A' + digit - 0xA);
        num /= base;
    } while (num && (i < (len - 1)));

    if (i == (len - 1) && num)
        return -1;

    str[i] = '\0';
    stringrev(str);

    return 0;
}


/*!
 * \brief Trim leading and trailing white spaces of string and retain original pointer
 * to beginning of string
 * \details Implemented by http://stackoverflow.com/users/19719/indiv
 * \param str
 * \return
 */
char *trim(char *str)
{
    size_t len = 0;
    const char *frontp = str - 1;
    char *endp = NULL;

    if ( str == NULL )
        return NULL;

    if ( str[0] == '\0' )
        return str;

    len = strlen(str);
    endp = str + len;

    /* Move the front and back pointers to address
     * the first non-whitespace characters from
     * each end.
     */
    while( isspace(*(++frontp)) );
    while( isspace(*(--endp)) && endp != frontp );

    if( str + len - 1 != endp )
        *(endp + 1) = '\0';
    else if( frontp != str &&  endp == frontp )
        *str = '\0';

    /* Shift the string so that it starts at str so
     * that if it's dynamically allocated, we can
     * still free it on the returned pointer.  Note
     * the reuse of endp to mean the front of the
     * string buffer now.
     */
    endp = str;
    if ( frontp != str ) {
        while( *frontp ) *endp++ = *frontp++;
        *endp = '\0';
    }

    return str;
}


/*!
 * \brief Duplicate S, returning an identical malloc'd string
 * \param s
 * \return
 */
char *
strclone (const char *s)
{
    size_t len = strlen (s) + 1;
    void *new = malloc (len);

    if (new == NULL)
        return NULL;

    return (char *) memcpy (new, s, len);
}


/*!
 * \brief Compare no more than N characters of S1 and S2
 * \details
    Compare no more than N characters of S1 and S2,
    returning less than, equal to or greater than zero
    if S1 is lexicographically less than, equal to or
    greater than S2.

    Original algorithm, Copyright GNU LIBC, adapted with toupper()
 *
 * \param s1
 * \param s2
 * \param n
 * \return
 */
int
strnicmp (const char *s1, const char *s2, size_t n)
{
  unsigned char c1 = '\0';
  unsigned char c2 = '\0';

  if (n >= 4)
    {
      size_t n4 = n >> 2;
      do
      {
        c1 = (unsigned char) toupper((unsigned char) *s1++);
        c2 = (unsigned char) toupper((unsigned char) *s2++);
        if (c1 == '\0' || c1 != c2)
          return c1 - c2;
        c1 = (unsigned char) toupper((unsigned char) *s1++);
        c2 = (unsigned char) toupper((unsigned char) *s2++);
        if (c1 == '\0' || c1 != c2)
          return c1 - c2;
        c1 = (unsigned char) toupper((unsigned char) *s1++);
        c2 = (unsigned char) toupper((unsigned char) *s2++);
        if (c1 == '\0' || c1 != c2)
          return c1 - c2;
        c1 = (unsigned char) toupper((unsigned char) *s1++);
        c2 = (unsigned char) toupper((unsigned char) *s2++);
        if (c1 == '\0' || c1 != c2)
          return c1 - c2;
      } while (--n4 > 0);
      n &= 3;
    }

  while (n > 0)
    {
      c1 = (unsigned char) toupper((unsigned char) *s1++);
      c2 = (unsigned char) toupper((unsigned char) *s2++);
      if (c1 == '\0' || c1 != c2)
        return c1 - c2;
      n--;
    }

  return c1 - c2;
}


/*!
 * \brief Locate <find> string in s, case independent
 * \details Original strstr() algorithm, Copyright GNU LIBC, adapted with toupper()
 * \param s
 * \param find
 * \return
 */
char *substr(char *s, const char *find)
{
    char c;
    char sc;
    size_t len;

    if ((c = (char) toupper(*find++)) != 0) {
        len = strlen(find);
        do {
            do {
                if ((sc = (char) toupper(*s++)) == 0)
                    return NULL;
            } while (sc != c);
        } while (strnicmp(s, find, len) != 0);
        s--;
    }
    return s;
}


/*!
 * \brief search and replace pattern or char in string
 * \details
 *  Algorithm copied from here: https://stackoverflow.com/a/31775567
 *  (renamed and edited for C-99 compliance)
 *
 *  1) Replace the pattern regardless of whether is was longer or shorter.
 *  2) Not use any malloc (explicit or implicit) to intrinsically avoid memory leaks.
 *  3) Replace any number of occurrences of pattern.
 *  4) Tolerate the replace string having a substring equal to the search string.
 *  5) Does not have to check that the Line array is sufficient in size to hold the
 *     replacement. e.g. This does not work unless the caller knows that line is of
 *     sufficient size to hold the new string.
 *
 * \param str string to perform search and replace (null-terminated)
 * \param search find pattern in string
 * \param replace pattern to replace in string
 * \return count of pattern replaced in string
 */
int strrpl(const char *str, const char *search, const char *replace)
{
    int count;   /* count of pattern replaced in string */
    size_t sLen;
    size_t rLen;
    char *sp;    /* start of pattern */

    if ((sp = strstr(str, search)) == NULL) {
        /* pattern was not found */
        return 0;
    }

    count = 1;
    sLen = strlen(search);
    rLen = strlen(replace);

    if (sLen > rLen) {
        /* move from right to left */
        const char *src = sp + sLen;
        char *dst = sp + rLen;
        while ((*dst = *src) != '\0') { dst++; src++; }
    } else if (sLen < rLen) {
        /* move from left to right */
        size_t tLen = strlen(sp) - sLen;
        const char *stop = sp + rLen;
        const char *src = sp + sLen + tLen;
        char *dst = sp + rLen + tLen;
        while (dst >= stop) { *dst = *src; dst--; src--; }
    }

    memcpy(sp, replace, rLen);
    count += strrpl(sp + rLen, search, replace);

    return count;
}


/* ----------------------------------------------------------------
    unsigned char *GetLine (sourcefile_t *csfile, char *line, unsigned char *startlineptr)

    From <startlineptr> in specified memory file, read a source
    code line into <line> buffer (truncated if line is bigger than
    line buffer).

    Current file position of <csfile> is unchanged.

    '\n' byte is applied to buffer if a CR/LF/CRLF variation line
    feed is found.
   ---------------------------------------------------------------- */
void
GetLine (sourcefile_t *csfile, char *const linebuf, const unsigned char *startlineptr)
{
    const unsigned char *origlineptr = MemfTellPtr (csfile->mf); /* remember current file position */
    int l;
    int c;

    linebuf[0] = '\0';
    if (startlineptr != NULL && MemfSeekPtr(csfile->mf, startlineptr) == 0) {
        /* position to specified file pointer in current file */

        l=0;
        do {
            c = GetChar(csfile);
            if (c != MFEOF) {
                linebuf[l] = (char) c;    /* line feed inclusive */
            } else {
                break;
            }

            l++;
        } while (c != '\n' && l<MAX_LINE_BUFFER_SIZE);
        linebuf[l] = '\0';

        /* resume file position */
        MemfSeekPtr(csfile->mf, origlineptr);
    }
}



/* ----------------------------------------------------------------
    int GetChar (sourcefile_t *csfile)

    Return character from specified source file (typically current)
    Handles multi-platform newline and meta control characters
    such as \r, \n, \b, \' ...
   ---------------------------------------------------------------- */
int
GetChar (sourcefile_t *csfile)
{
    int c = __gcLf(csfile);

    /* continuous line or escape sequence? */
    if ( csfile != NULL && csfile->parsingmacro == false && c == '\\') {
        /* only interpret '\' outside a macro body declaration scope ('\' identifies macro local names and arguments) */
        c = __gcLf(csfile);

        /* Also handle escape sequences, http://en.wikipedia.org/wiki/Escape_sequences_in_C  */
        switch (c) {
            case '\n':
                /* There was an EOL just after the \, return a space and update line counter */
                c = 0x20;
                csfile->eol = false;
                break;
            case '\\':
                c = '\\'; /* interpret \\ as \ */
                break;
            case 'n':
                c = 0x0a; /* interpret as Ascii Line feed */
                break;
            case 'r':
                c = 0x0d; /* interpret as Ascii Carriage return */
                break;
            case 't':
                c = 0x09; /* interpret as Ascii horisontal tab */
                break;
            case 'a':
                c = 0x07; /* interpret as Ascii Alarm */
                break;
            case 'b':
                c = 0x08; /* interpret as Ascii Backspace */
                break;
            case 'f':
                c = 0x0c; /* interpret as Ascii Formfeed */
                break;
            case '\'':
                c = '\''; /* ' */
                break;
            case '\"':
                c = '\"'; /* " */
                break;
            default:
                /* continuous line marker, skip until EOL */
                MemfUngetc(csfile->mf);
                SkipLine(csfile);
                if (csfile->eol == true) {
                    csfile->eol = false;
                }
        }
    }

    return c; /* return all other characters (or EOL) */
}


/*!
 * \brief fGetChar
 * \details '\n' byte is return if a CR/LF/CRLF variation line feed is found
 * \param fptr
 * \return  return a character from opened file with CR/LF/CRLF parsing capability
 */
int
fGetChar (FILE *fptr)
{
    int c;

    c = fgetc (fptr);
    if (c == 13) {
        /* Mac line feed found, poll for MS-DOS / WINDOWS line feed */
        c = fgetc (fptr);
        if (c != 10) {
            ungetc (c, fptr);    /* push non-line-feed character back into file */
        }

        c = '\n'; /* always return the symbolic '\n' for line feed */
    } else if (c == 10) {
        c = '\n';    /* UNIX line feed */
    }

    return c; /* return all other characters */
}


/*!
 * \brief Fetch next symbol on current source file and return symbolic mnemonic type
 * \param csfile
 * \return
 */
enum symbols
GetSym (sourcefile_t *csfile)
{
    int c;
    int chindex = 0;

    csfile->ident[0] = '\0';
    csfile->identlen = 0;

    if (csfile->eol == true) {
        return csfile->sym = newline;
    }

    while (1) {
        /* Ignore leading white spaces, if any... */
        if (MemfEof (csfile->mf)) {
            csfile->eol = true;
            return csfile->sym = newline;
        } else {
            c = GetChar (csfile);
            if ((c == '\n') || (c == MFEOF) || (c == '\x1A')) {
                csfile->eol = true;
                return csfile->sym = newline;
            } else if (!isspace (c)) {
                break;
            }
        }
    }

    if (GetSeparator(csfile, c) != NULL) {
        /* parse special separator symbols, incl. interpretation of % as either MOD operator or binary constant */
        c = ParseSeparator(csfile, c);

        if (csfile->sym == newline)
            return newline;

        if (csfile->cstylecomment == true) {
            SkipLine (csfile);
            return GetSym(csfile);
        } else {
            /* if % was interpreted as MOD operator, return as separator symbol, otherwise skip and parse as binary constant */
            if (csfile->sym != binconst)
                return csfile->sym;
        }
    }

    /* before going deeper into symbol parsing, check if we're in a c-style comment block... */
    if (csfile->cstylecomment == true) {
        SkipLine (csfile);
        return GetSym(csfile);
    }

    CharToIdent(csfile->ident, (char) toupper (c), chindex);
    chindex++;

    switch (c) {
        case '$':
        case '#':
            csfile->sym = hexconst;
            chindex = FetchSymIdent(csfile, chindex);
            break;

        case '@':
        case '%':
            csfile->sym = binconst;
            chindex = FetchSymBinConst(csfile, chindex);
            break;

        case '_':
            /* leading '_' allowed for name definitions */
            /* Read name identifier until space or legal separator is found */
            csfile->sym = name;
            chindex = FetchSymName(csfile, chindex);
            break;

        default:
            if (isdigit (c)) {
                /* a decimal number found */
                csfile->sym = decmconst;
                chindex = FetchSymIdent(csfile, chindex);
            } else {
                if (isalpha (c)) {
                    /* a name identifier found */
                    csfile->sym = name;
                    chindex = FetchSymName(csfile, chindex);
                } else {
                    /* rubbish ... */
                    csfile->sym = nil;
                    chindex = FetchSymIdent(csfile, chindex);
                }
            }
            break;
    }

    csfile->ident[chindex] = '\0';
    csfile->identlen = chindex;
    return csfile->sym;
}


void
SkipLine (sourcefile_t *csfile)
{
    int c;

    if (csfile->eol == false) {
        while (!MemfEof (csfile->mf)) {

            c = MemfGetc (csfile->mf);
            if (c == 13) {
                /* Mac line feed found, poll for MS-DOS / WINDOWS line feed */
                c = MemfGetc (csfile->mf);
                if (c != 10) {
                    MemfUngetc(csfile->mf);  /* push non-line-feed character back into file */
                }

                c = '\n'; /* always return the symbolic '\n' for line feed */
            } else if (c == 10) {
                c = '\n';    /* UNIX line feed */
            }

            if ((c == '\n') || (c == MFEOF)) {
                break;    /* get to beginning of next line... */
            }
            if ( c == '*' ) {
                c = MemfGetc (csfile->mf);
                if ( c == '/' ) {
                    if (csfile->cstylecomment == true) {
                        csfile->cstylecomment = false;
                        return;
                    }
                } else {
                    MemfUngetc(csfile->mf);    /* puch character back for next read */
                }
            }
        }

        csfile->eol = true;
    }
}


void
fSkipLine (FILE *fptr, bool *cstylecomment)
{
    int c;

    while (!feof (fptr)) {
        c = fGetChar (fptr);
        switch(c) {
            case '\n':
            case EOF:
                /* get to beginning of next line... */
                return;

            case '*':
                c = fGetChar (fptr);
                if ( c == '/' ) {
                    if (*cstylecomment == true) {
                        *cstylecomment = false;
                        return;
                    }
                } else {
                    ungetc (c, fptr);    /* puch character back for next read */
                }
                break;

            default:
                /* skip character of current line */
                break;
        }
    }
}


/*!
 * \brief Evaluate the current [ident] via csfile and symbol for integer constant
 * \details
 *    The following type specifiers are recognized:

        0xhhh , $hhhh , #hhhh   hex constant
        @bbbb                   binary constant
        %bbbb                   binary constant

        constant is evaluated by default as decimal, if no type specifier is used.

 * \param csfile
 * \param evalerr byref argument is set to 0 when constant was successfully evaluated, otherwise 1
 * \return The evaluated constant is returned as a long integer.
 */
long
GetConstant (const sourcefile_t *csfile, char *evalerr)
{
    int size;
    char *temp = NULL;
    long lv;

    errno = 0;            /* reset global error number */
    lv = 0;
    *evalerr = 0;         /* preset evaluation return code to no errors */
    size = (int) strlen (csfile->ident);

    if ((csfile->sym != hexconst) && (csfile->sym != binconst) && (csfile->sym != decmconst)) {
        *evalerr = 1;
        return lv;       /* syntax error - illegal constant definition */
    }

    if ( csfile->ident[0] == '0' && toupper(csfile->ident[1]) == 'X') {
        /* fetch hex constant specified as 0x... */
        lv = (long) strtoll((csfile->ident + 2), &temp, 16);
        if (*temp != '\0' || errno == ERANGE) {
            *evalerr = 1;
        }

        return lv; /* returns 0 on error */
    }

    if ( csfile->ident[0] == '0' && toupper(csfile->ident[1]) == 'B' && toupper(csfile->ident[size-1] == 'H')) {
        /* fetch hex constant specified as 0b..H (truncate 'H' specifier) */
        csfile->ident[size-1] = '\0';
        lv = (long) strtoll(csfile->ident, &temp, 16);
        if (*temp != '\0' || errno == ERANGE) {
            *evalerr = 1;
        }

        return lv; /* returns 0 on error */
    }

    if ( csfile->ident[0] == '0' && toupper(csfile->ident[1]) == 'B' && toupper(csfile->ident[size-1] != 'H')) {
        /* fetch binary constant specified as 0b... */
        lv = (long) strtoll((csfile->ident + 2), &temp, 2);
        if (*temp != '\0' || errno == ERANGE) {
            *evalerr = 1;
        }

        return lv; /* returns 0 on error */
    }

    if (csfile->sym != decmconst) {
        /* adjust size of non decimal constants without leading type specifier */
        --size;
        if (size == 0) {
            *evalerr = 1;
            /* signal syntax error - no constant specified */
            return lv;
        }
    }

    switch (csfile->ident[0]) {
        case '@':
        case '%':
            /* Binary integer are identified with leading @ or % */
            lv = (long) strtoll((csfile->ident + 1), &temp, 2);
            if (*temp != '\0' || errno == ERANGE) {
                *evalerr = 1;
            }

            return lv; /* returns 0 on error */

        case '#':
        case '$':
            /* Hexadecimal integers may be specified with leading $ or # */
            lv = (long) strtoll((csfile->ident + 1), &temp, 16);
            if (*temp != '\0' || errno == ERANGE) {
                *evalerr = 1;
            }

            return lv; /* returns 0 on error */

            /* Parse default decimal integers */
        default:
            lv = (long) strtoll(csfile->ident, &temp, 10);
            if (*temp != '\0' || errno == ERANGE) {
                *evalerr = 1;
            }

            return lv; /* returns 0 on error */
    }
}


/*!
 * \brief Allocate global dynamic memory for line buffer
 * \return Return 1 if allocated, or if no room in system
 */
int AllocateTextBuffers()
{
    if ( (line = AllocIdentifier(MAX_LINE_BUFFER_SIZE+1)) == NULL ) {
        ReportError (NULL, Err_Memory);
        FreeTextBuffers();
        return 0;
    }

    return 1;
}


/*!
 * \brief Release previously allocated dynamic memory for line buffer
 */
void FreeTextBuffers()
{
    if ( line != NULL ) {
        free(line);
        line = NULL;
    }
}
