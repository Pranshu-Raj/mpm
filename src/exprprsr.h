/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/

#if ! defined MPM_EXPRPRSR_HEADER_
#define MPM_EXPRPRSR_HEADER_

#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

/* global functions */
expression_t *ParseNumExpr (sourcefile_t * const csfile);
expressions_t *AllocExprHdr (void);
expression_t *CloneExprPlusOne(expression_t *expr);
int ProcessExpr (sourcefile_t *csfile, expression_t *pfixexpr, unsigned char exprtype, int listoffset);
int ProcessPcRelativeExpr (sourcefile_t *csfile, unsigned char exprtype, int listoffset);
long EvalNumExpr (expression_t *pfixexpr);
long EvalFreeNumExpr (expression_t *pfixlist);
long ParseMnemConstant (sourcefile_t *csfile);
void ReleaseExprns (expressions_t *express);
void FreeNumExpr (expression_t *pfixexpr);

#endif
