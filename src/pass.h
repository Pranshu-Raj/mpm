/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#if ! defined MPM_PASS_HEADER_
#define MPM_PASS_HEADER_

#include <stdio.h>
#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

/* global variables */
extern pathlist_t *gIncludePath;
extern pathlist_t *gLibraryPath;
extern labels_t *addresses;

/* global functions */
int AssembleSourceFile (sourcefile_t *csfile);
int GetListingFilePageNo(void);
int GetStreamCodePC (const sourcefile_t *csfile);
bool UpdateStreamCodeByte (sourcefile_t *csfile, int codeidx, int byte);
bool UpdateStreamCodeWord(sourcefile_t *csfile, int codeidx, unsigned short word);
bool UpdateStreamCodeWordMSB (sourcefile_t *csfile, int codeidx, unsigned short word);
bool UpdateStreamCodeInt24 (sourcefile_t *csfile, int codeidx, int lw);
bool UpdateStreamCodeInt24MSB (sourcefile_t *csfile, int codeidx, int lw);
bool UpdateStreamCodeInt32 (sourcefile_t *csfile, int codeidx, long lw);
bool UpdateStreamCodeInt32MSB (sourcefile_t *csfile, int codeidx, long lw);
bool UpdateMemExpr(sourcefile_t *csfile, const expression_t *parsedexpr, long exprvalue);
void StreamCodeByte (const sourcefile_t *csfile, unsigned char byte);
void StreamCodeWord (const sourcefile_t *csfile, unsigned short w);
void StreamCodeWordMSB (const sourcefile_t *csfile, unsigned short word);
void StreamCodeInt24 (const sourcefile_t *csfile, size_t lw);
void StreamCodeInt24MSB (const sourcefile_t *csfile, size_t lw);
void StreamCodeInt32 (const sourcefile_t *csfile, size_t lw);
void StreamCodeInt32MSB (const sourcefile_t *csfile, size_t lw);
void CloseListingFile(memfile_t *lstfile);
void InitializeListingFile(sourcefile_t *csfile);
void SourceFilePass1 (sourcefile_t *csfile);
void AddAddress (symbol_t *label, labels_t **stackpointer);
void PreserveExpr (expression_t *expression, unsigned char constrange, long lfileptr);
void ReleasePathInfo(void);
void WriteListFileLine (sourcefile_t *csfile);
void Stream2ListingFile(module_t *module, const char *str);
void UpdateListingFileLineCounter (module_t *module);
void WriteMapFile (void);
void CloseErrorFile(memfile_t *errfile);
memfile_t *CreateErrFile(const char *errflname, const char *ext);
memfile_t *CreateLstFile( const char *srcflname, const char *ext);
symbol_t *GetAddress (labels_t **stackpointer);

#endif
