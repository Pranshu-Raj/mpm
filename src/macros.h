/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#if ! defined MPM_MACROS_HEADER_
#define MPM_MACROS_HEADER_

#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

/* global functions */
int DiscardMacroArgBrackets(sourcefile_t *csfile);
void fetchMacroArgument(sourcefile_t *csfile, char *string, int maxsize);
void fetchMacroArgumentString(sourcefile_t *csfile, char *string, int maxsize, char terminator);
void FreeMacroNameParameters(int argc, char *argv[]);
void ParseMacro(sourcefile_t *csfile, const char *macroname, int argc, char *argv[]);
void FreeMacro (macro_t *node);
macro_t *FindMacro (const char *identifier, const avltree_t *treeptr);
sourcefile_t *ExecuteMacro(sourcefile_t *csfile, macro_t *macrodef);

#define MACRO_INSTANCECOUNTER_NAME "MACINDEX"
#define MACRO_INSTANCEARGCOUNTER_NAME "MACARGC"
#define MACRO_INSTANCEARGARRAY_NAME "MACARGV"

#endif
