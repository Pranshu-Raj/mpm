/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#if ! defined MPM_OBJFILE_HEADER_
#define MPM_OBJFILE_HEADER_

#include <stdio.h>
#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

/* MPM object file watermark and length */
#define MPMOBJECTVERSION "02"
#define MPMOBJECTHEADER  "MPMRMF" MPMOBJECTVERSION
#define SIZEOF_MPMOBJHDR 8

extern FILE *objfile;

FILE *ObjFileCreate(char *objfname);
FILE *ObjFileOpen(char *filename, const char **objversion);
int ObjFileCheckWaterMark(const char *string, const char *watermark);
int ObjFileWaterMark(const char *watermark);
void ObjFileWriteExpr (expression_t *pfixexpr, const unsigned char exprtype);
void ObjFileWriteHeader(sourcefile_t *csfile, FILE *objf, size_t moduleCodesize);
void ObjFileWriteLong (long int32, FILE * fileid);
void ObjFileReadString (FILE *objf, char * const string);
void ObjFileReadSymbols (module_t *curmodule, FILE *objf, size_t orgaddr, long nextname, long endnames);
long ObjFileReadLong (FILE *fileid);
bool ObjFileExpressionRange(sourcefile_t *csfile, long exprvalue, unsigned char exprtype);
memfile_t *ObjFileLoadExpr(sourcefile_t *cfile, FILE *objf, unsigned char *type, size_t *patchcodeidx, long *nextexpr);

#endif
