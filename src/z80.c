/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 *                         ZZZZZZZZZZZZZZ    888888888888        000000000
 *                       ZZZZZZZZZZZZZZ    8888888888888888    0000000000000
 *                               ZZZZ      8888        8888  0000         0000
 *                             ZZZZ          888888888888    0000         0000
 *                           ZZZZ          8888        8888  0000         0000
 *                         ZZZZZZZZZZZZZZ  8888888888888888    0000000000000
 *                       ZZZZZZZZZZZZZZ      888888888888        000000000
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include "datastructs.h"
#include "avltree.h"
#include "modules.h"
#include "main.h"
#include "options.h"
#include "exprprsr.h"
#include "symtables.h"
#include "pass.h"
#include "asmdrctv.h"
#include "errors.h"
#include "prsline.h"
#include "memfile.h"
#include "z80.h"


/******************************************************************************
                               Local functions and variables
 ******************************************************************************/
static unsigned char *AllocRelocTable( void );
static relocoffset_t *AllocRelocOffset (void);
static void FreeRelocOffset (relocoffset_t *node);
static void GenerateRelocTable( const relocoffset_t *ro );
static void IncDecIndrct8bitZ80 (sourcefile_t *csfile, int opcode);
static void ADCSBC (sourcefile_t *csfile, int opc8bitreg, int opc16bitreg);
static void ArithLog8bitIndrctZ80(sourcefile_t *csfile, int opcode);
static void IncDecZ80(sourcefile_t *csfile, int opc8, int opc16);
static void BitSrcRg8BitIndrct(sourcefile_t *csfile, int opcode, int bitnumber);
static void LD_r_8bit_indrct (sourcefile_t *csfile, enum z80r reg);
static void RLWr16(sourcefile_t *csfile, enum z80rr dstreg16);
static void RRWr16(sourcefile_t *csfile, enum z80rr dstreg16);
static void SLAWr16(sourcefile_t *csfile, enum z80rr dstreg16);
static void SRAWr16(sourcefile_t *csfile, enum z80rr dstreg16);
static void SRLWr16(sourcefile_t *csfile, enum z80rr dstreg16);
static void LD_DestIndexIndrct (sourcefile_t *csfile, enum z80ia destreg);
static void LD_address_indrct (sourcefile_t *csfile);
static void LD_16bitDestReg (sourcefile_t *csfile);
static void LD_16bitDestReg_SrcindDirect(sourcefile_t *csfile, enum z80rr destreg);
static void LD_16bitDestReg_SrcDirect (sourcefile_t *csfile, enum z80rr destreg);
static void LD_Meta_8bitDestReg_SrcIndirectIndex(sourcefile_t *csfile, enum z80r dstreg8, enum z80rr srcidxreg, expression_t *offsetexpr, int offset);
static void LD_Meta_16bitDestReg_SrcIndirectIndex(sourcefile_t *csfile, enum z80r dstreg8, enum z80rr srcidxreg);
static void LD_DstIndirectIndex_8bitSrcReg(sourcefile_t *csfile, enum z80rr dstidxreg, enum z80r srcreg8, expression_t *offsetexpr, int offset);
static void LD_Meta_DestIndirectIndex_16bitSrcReg(sourcefile_t *csfile, enum z80rr dstidxreg, expression_t *offsetexpr, enum z80r srcreg8);
static void LDMeta_16bitDestReg_16bitSrcReg(sourcefile_t *csfile, enum z80rr destreg, enum z80rr srcreg);
static void LD_dst_indrct(sourcefile_t *csfile);
static void BitSetResZ80 (sourcefile_t *csfile, int opcode);
static void RotShiftZ80 (sourcefile_t *csfile, int opcode);
static int cmprelocoffset (const relocoffset_t *ro, const relocoffset_t *p);
static enum z80rr FetchHLIndirect(sourcefile_t *csfile);
static relocoffset_t *LookupRelocOffset (unsigned short reloffset);

static avltree_t *relocationoffsets = NULL;

/*
   This binary code sequense is a compiled version of the 'relocate.asm' file
   (bundled with these C source files for reference) that represents the program
   address patch initialisation, performed a single time on the program binary.
   Subsequent calls to the start of the relocatable code just jumps straight to
   the first instruction of the real program.
 */
static unsigned char reloc_routine[] =
    "\x08\xD9\xFD\xE5\xE1\x01\x49\x00\x09\x5E\x23\x56\xD5\x23\x4E\x23"
    "\x46\x23\xE5\x09\x44\x4D\xE3\x7E\x23\xB7\x20\x06\x5E\x23\x56\x23"
    "\x18\x03\x16\x00\x5F\xE3\x19\x5E\x23\x56\xEB\x09\xEB\x72\x2B\x73"
    "\xD1\xE3\x2B\x7C\xB5\xE3\xD5\x20\xDD\xF1\xF1\xFD\x36\x00\xC3\xFD"
    "\x71\x01\xFD\x70\x02\xD9\x08\xFD\xE9";
static size_t sizeof_relocroutine = 73;
static unsigned char *reloctable = NULL;
static unsigned char *relocptr = NULL;
static unsigned short totalrelocaddr;
static unsigned short sizeof_reloctable;


/* ------------------------------------------------------------------------------
   Pre-sorted array of Z80 instruction mnemonics.
   ------------------------------------------------------------------------------ */
static idf_t z80idents[] = {
    {"ADC", {.mpf = ADC}},
    {"ADD", {.mpf = ADD}},
    {"AND", {.mpf = AND}},
    {"BIT", {.mpf = BIT}},
    {"CALL", {.mpf = CALL}},
    {"CCF", {.cmpf = CCF}},
    {"CP", {.mpf = CP}},
    {"CPD", {.cmpf = CPD}},
    {"CPDR", {.cmpf = CPDR}},
    {"CPI", {.cmpf = CPI}},
    {"CPIR", {.cmpf = CPIR}},
    {"CPL",  {.cmpf = CPL}},
    {"DAA", {.cmpf = DAA}},
    {"DEC", {.mpf = DEC}},
    {"DI", {.cmpf = DI}},
    {"DJNZ", {.mpf = DJNZ}},
    {"EI", {.cmpf = EI}},
    {"EX", {.mpf = EX}},
    {"EXX", {.cmpf = EXX}},
    {"HALT", {.cmpf = HALT}},
    {"IM", {.mpf = IM}},
    {"IN", {.mpf = IN}},
    {"INC", {.mpf = INC}},
    {"IND", {.cmpf = IND}},
    {"INDR", {.cmpf = INDR}},
    {"INI", {.cmpf = INI}},
    {"INIR", {.cmpf = INIR}},
    {"JP", {.mpf = JP}},
    {"JR", {.mpf = JR}},
    {"LD", {.mpf = LD}},
    {"LDD", {.cmpf = LDD}},
    {"LDDR", {.cmpf = LDDR}},
    {"LDI", {.cmpf = LDI}},
    {"LDIR", {.cmpf = LDIR}},
    {"NEG", {.cmpf = NEG}},
    {"NOP", {.cmpf = NOP}},
    {"OR", {.mpf = OR}},
    {"OTDR", {.cmpf = OTDR}},
    {"OTIR", {.cmpf = OTIR}},
    {"OUT", {.mpf = OUT}},
    {"OUTD", {.cmpf = OUTD}},
    {"OUTI", {.cmpf = OUTI}},
    {"POP", {.mpf = POP}},
    {"PUSH", {.mpf = PUSH}},
    {"RES", {.mpf = RES}},
    {"RET", {.mpf = RET}},
    {"RETI", {.cmpf = RETI}},
    {"RETN", {.cmpf = RETN}},
    {"RL", {.mpf = RL}},
    {"RLA", {.cmpf = RLA}},
    {"RLC", {.mpf = RLC}},
    {"RLCA", {.cmpf = RLCA}},
    {"RLD", {.cmpf = RLD}},
    {"RLW", {.mpf = RLW}},      /* meta instruction */
    {"RR", {.mpf = RR}},
    {"RRA", {.cmpf = RRA}},
    {"RRC", {.mpf = RRC}},
    {"RRCA", {.cmpf = RRCA}},
    {"RRD", {.cmpf = RRD}},
    {"RRW", {.mpf = RRW}},      /* meta instruction */
    {"RST", {.mpf = RST}},
    {"SBC", {.mpf = SBC}},
    {"SCF", {.cmpf = SCF}},
    {"SET", {.mpf = SET}},
    {"SLA", {.mpf = SLA}},
    {"SLAW", {.mpf = SLAW}},    /* meta instruction */
    {"SLL", {.mpf = SLL}},
    {"SRA", {.mpf = SRA}},
    {"SRAW", {.mpf = SRAW}},    /* meta instruction */
    {"SRL", {.mpf = SRL}},
    {"SRLW", {.mpf = SRLW}},    /* meta instruction */
    {"SUB", {.mpf = SUB}},
    {"SUBW", {.mpf = SUBW}},    /* meta instruction */
    {"XOR", {.mpf = XOR}}
};


static int
cmprelocoffset (const relocoffset_t *ro, const relocoffset_t *p)
{
    return ro->offset - p->offset;
}


static relocoffset_t *
LookupRelocOffset (unsigned short reloffset)
{
    relocoffset_t ro;

    ro.offset = reloffset;
    if (relocationoffsets == NULL) {
        return NULL;
    } else {
        return Find (relocationoffsets, &ro, (compfunc_t) cmprelocoffset);
    }
}


static relocoffset_t *
AllocRelocOffset (void)
{
    return (relocoffset_t *) malloc (sizeof (relocoffset_t));
}


static unsigned char *
AllocRelocTable( void )
{
    return (unsigned char *) malloc (32768U);
}


static void
FreeRelocOffset (relocoffset_t *node)
{
    if (node != NULL)
        free(node); /* free memory of unsigned short wrapper object */
}


static void
GenerateRelocTable( const relocoffset_t *ro )
{
    static unsigned short currelocoffset = 0;
    long constant;

    /* define distance distance between current and previous relocatable address */
    constant = ro->offset - currelocoffset;

    if ((constant >= 0) && (constant <= 255)) {
        *relocptr++ = (unsigned char) constant;
        sizeof_reloctable++;
    } else {
        *relocptr++ = 0;
        *relocptr++ = (unsigned char) (ro->offset - currelocoffset) % 256U;
        *relocptr++ = (unsigned char) (ro->offset - currelocoffset) / 256U;
        sizeof_reloctable += 3;
    }

    totalrelocaddr++;
    currelocoffset = ro->offset;
}


/*!
 * \brief LD (IX|IY [offset expr]),r8
 * \param csfile
 * \param dstidxreg
 * \param srcreg8
 * \param offsetexpr
 * \param offset
 */
static void
LD_DstIndirectIndex_8bitSrcReg(sourcefile_t *csfile, enum z80rr dstidxreg, enum z80r srcreg8, expression_t *offsetexpr, int offset)
{
    StreamCodeByte (csfile, (dstidxreg == z80r_ix) ? 0xdd : 0xfd);
    StreamCodeByte (csfile, (unsigned char) (112 + srcreg8));
    if (offsetexpr == NULL) {
        /* no displacement expression use specified offset */
        StreamCodeByte (csfile, (unsigned char) offset);
    } else {
        /* update PC of pre-parsed expression to point at actual PC position, where index offset
           expression will be placed by the expression evaluation (now or later in pass 2 or linking */
        offsetexpr->codepos = (size_t) GetStreamCodePC(csfile);
        ProcessExpr (csfile, offsetexpr, RANGE_8SIGN, 2);
    }
}


/*!
 * \brief LD (IX|IY [offset expr]),BC|DE|HL
 * \details
 * The 16bit source is specified as B for BC, D for DE, H for HL,
 * because two individual 8bit instructions are generated
 * \param csfile
 * \param dstreg8
 * \param srcidxreg
 */
static void
LD_Meta_DestIndirectIndex_16bitSrcReg(sourcefile_t *csfile, enum z80rr dstidxreg, expression_t *offsetexpr, enum z80r srcreg8)
{
    expression_t *offsetexpr1;

    if (offsetexpr == NULL) {
        /* no offset expression were specified, use 0 and 1 as constant offsets */
        LD_DstIndirectIndex_8bitSrcReg(csfile, dstidxreg, srcreg8+1, NULL, 0); /* first lower 8bit of 16bit register */
        LD_DstIndirectIndex_8bitSrcReg(csfile, dstidxreg, srcreg8, NULL, 1); /* then upper 8bit of 16bit register */
    } else {
        offsetexpr1 = CloneExprPlusOne(offsetexpr);
        LD_DstIndirectIndex_8bitSrcReg(csfile, dstidxreg, srcreg8+1, offsetexpr, 0);
        LD_DstIndirectIndex_8bitSrcReg(csfile, dstidxreg, srcreg8, offsetexpr1, 1);
    }
}


/*!
 * \brief Load Index Indirect destination of 8bit or 16bit source
 * \details
 *  LD (IX|IY+d),r
 *  LD (IX|IY+d),n
 *  LD (IX|IY+d),bc|de|hl (optional meta instructions)
 * \param csfile
 * \param destreg
 */
static void
LD_DestIndexIndrct (sourcefile_t *csfile, enum z80ia destreg)
{
    enum z80r srcreg8;
    enum z80rr srcreg16;
    expression_t *indexexpr = NULL;

    if (csfile->sym != rparen) {
        indexexpr = ParseNumExpr(csfile);
        if (indexexpr == NULL) {
            return;    /* IX/IY offset contained a syntax error */
        }
    }

    if (indexexpr != NULL && csfile->sym != rparen) {
        ReportError (csfile, Err_Syntax);   /* ')' wasn't found in after expression */
        return;
    }

    if (GetSym(csfile) == comma) {
        GetSym(csfile);
        switch (srcreg8 = CheckReg8Mnem (csfile)) {
            case z80r_f:     /* xxx ,F illegal */
            case z80r_i:     /* xxx ,I illegal */
            case z80r_r:     /* xxx ,R illegal */
                ReportError (csfile, Err_IllegalIdent);
                break;

            case z80r_unknown:
                if ((srcreg16 = CheckReg16Mnem(csfile)) != z80rr_unknown) {
                    /* LD (IX|IY+d), bc|de|hl */
                    switch(srcreg16) {
                        case z80r_bc:
                            LD_Meta_DestIndirectIndex_16bitSrcReg(csfile, (enum z80rr) destreg, indexexpr, z80r_b);
                            break;
                        case z80r_de:
                            LD_Meta_DestIndirectIndex_16bitSrcReg(csfile, (enum z80rr) destreg, indexexpr, z80r_d);
                            break;
                        case z80r_hl:
                            LD_Meta_DestIndirectIndex_16bitSrcReg(csfile, (enum z80rr) destreg, indexexpr, z80r_h);
                            break;
                        default:
                            ReportError (csfile, Err_IllegalIdent);
                    }
                    return;
                }

                /* LD (IX|IY+d),n */
                StreamCodeByte (csfile, (destreg == z80ia_ix) ? 0xdd : 0xfd);
                StreamCodeByte (csfile, 0x36);                        /* opcode for LD (IX|IY+d),n */
                if (indexexpr == NULL) {
                    /* (IX|IY) was specified, implicit offset is 0 */
                    StreamCodeByte (csfile, 0);
                } else {
                    indexexpr->codepos = (size_t) GetStreamCodePC(csfile); /* update to current PC for offset expression */
                    ProcessExpr (csfile, indexexpr, RANGE_8UNSIGN, 2);  /* offset expression for IX/IY */
                }
                ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 3);       /* expression for n */
                break;

            default:                
                /* check that IXh/l and IYh/l are not mixed, which is not supported by Z80 */
                if (destreg == z80ia_ix && (srcreg8 == z80r_iyh || srcreg8 == z80r_iyl) ) {
                    /* LD (IX+d),iyh/l */
                    ReportError (csfile, Err_IllegalIdent);
                    return;
                }
                if (destreg == z80ia_iy && (srcreg8 == z80r_ixh || srcreg8 == z80r_ixl) ) {
                    /* LD (IY+d),ixh/l */
                    ReportError (csfile, Err_IllegalIdent);
                    return;
                }

                /* LD (IX|IY+d),r */
                LD_DstIndirectIndex_8bitSrcReg(csfile, (enum z80rr) destreg, srcreg8, indexexpr, 0);
        }
    } else {
        ReportError (csfile, Err_Syntax);
    }
}


/**
 * @brief "LD (register), xxx" or "LD (nn),xxx" instructions
 * @param csfile
 */
static void
LD_dst_indrct(sourcefile_t *csfile)
{
    enum z80ia iareg;

    switch (iareg = CheckIndirectAddrMnem (csfile)) {
        case z80ia_hl:
            /* LD  (HL),X */
            if (csfile->sym == comma) {
                GetSym(csfile);
                switch (CheckReg16Mnem (csfile)) {
                    case z80r_unknown:
                        LD_HL8bit_indrct (csfile, CheckReg8Mnem (csfile));
                        break;
                    case z80ia_bc:
                        /* Meta instruction LD (HL),BC */
                        StreamCodeByte (csfile, 0x71); /* ld (hl),c */
                        StreamCodeByte (csfile, 0x23); /* inc hl */
                        StreamCodeByte (csfile, 0x70); /* ld (hl),b */
                        StreamCodeByte (csfile, 0x2b); /* dec hl */
                        break;
                    case z80ia_de:
                        /* Meta instruction LD (HL),DE */
                        StreamCodeByte (csfile, 0x73); /* ld (hl),e */
                        StreamCodeByte (csfile, 0x23); /* inc hl */
                        StreamCodeByte (csfile, 0x72); /* ld (hl),d */
                        StreamCodeByte (csfile, 0x2b); /* dec hl */
                        break;

                    default:
                        ReportError (csfile, Err_IllegalIdent);
                }
            } else {
                ReportError (csfile, Err_Syntax);
            }
            break;

        case z80ia_ix:
        case z80ia_iy:
            /* LD  (IX|IY+d),X  */
            LD_DestIndexIndrct (csfile, iareg);
            break;

        case z80ia_bc:
            if (csfile->sym != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            /* LD  (BC),A  */
            GetSym(csfile);
            if (CheckReg8Mnem (csfile) == z80r_a) {
                StreamCodeByte (csfile, 2);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80ia_de:
            if (csfile->sym != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            /* LD  (DE),A  */
            GetSym(csfile);
            if (CheckReg8Mnem (csfile) == z80r_a) {
                StreamCodeByte (csfile, 18);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80ia_nn:
            /* LD  (nn),rr  ;  LD  (nn),A  */
            LD_address_indrct (csfile);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief LD BC|DE|HL|IX|IY,BC|DE|HL|IX|IY
 * \details
 * These are meta instructions, generated by two 8bit opcode instructions,
 * for example LD BC,HL would be LD B,H : LD C,L
 * \param csfile
 * \param destreg
 * \param destreg
 */
static void
LDMeta_16bitDestReg_16bitSrcReg(sourcefile_t *csfile, enum z80rr destreg, enum z80rr srcreg)
{
    switch(destreg) {
        case z80r_bc:
            switch(srcreg) {
                case z80r_bc:
                    StreamCodeByte (csfile, 0x40); /* ld b,b */
                    StreamCodeByte (csfile, 0x49); /* ld c,c */
                    break;

                case z80r_de:
                    StreamCodeByte (csfile, 0x42); /* ld b,d */
                    StreamCodeByte (csfile, 0x4B); /* ld c,e */
                    break;

                case z80r_hl:
                    StreamCodeByte (csfile, 0x44); /* ld b,h */
                    StreamCodeByte (csfile, 0x4D); /* ld c,l */
                    break;

                case z80r_ix:
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x44); /* ld b,ixh */
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x4D); /* ld c,ixl */
                    break;

                case z80r_iy:
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x44); /* ld b,iyh */
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x4D); /* ld c,iyl */
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80r_de:
            switch(srcreg) {
                case z80r_bc:
                    StreamCodeByte (csfile, 0x50); /* ld d,b */
                    StreamCodeByte (csfile, 0x59); /* ld e,c */
                    break;

                case z80r_de:
                    StreamCodeByte (csfile, 0x52); /* ld d,d */
                    StreamCodeByte (csfile, 0x5B); /* ld e,e */
                    break;

                case z80r_hl:
                    StreamCodeByte (csfile, 0x54); /* ld d,h */
                    StreamCodeByte (csfile, 0x5D); /* ld e,l */
                    break;

                case z80r_ix:
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x54); /* ld d,ixh */
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x5D); /* ld e,ixl */
                    break;

                case z80r_iy:
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x54); /* ld d,iyh */
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x5D); /* ld e,iyl */
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80r_hl:
            switch(srcreg) {
                case z80r_bc:
                    StreamCodeByte (csfile, 0x60); /* ld h,b */
                    StreamCodeByte (csfile, 0x69); /* ld l,c */
                    break;

                case z80r_de:
                    StreamCodeByte (csfile, 0x62); /* ld h,d */
                    StreamCodeByte (csfile, 0x6B); /* ld l,e */
                    break;

                case z80r_hl:
                    StreamCodeByte (csfile, 0x64); /* ld h,h */
                    StreamCodeByte (csfile, 0x6D); /* ld h,l */
                    break;

                case z80r_ix:
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0xE5); /* push ix */
                    StreamCodeByte (csfile, 0xE1); /* pop hl */
                    break;

                case z80r_iy:
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0xE5); /* push iy */
                    StreamCodeByte (csfile, 0xE1); /* pop hl */
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80r_ix:
            switch(srcreg) {
                case z80r_bc:
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x60); /* ld ixh,b */
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x69); /* ld ixl,c */
                    break;

                case z80r_de:
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x62); /* ld ixh,d */
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x6B); /* ld ixl,e */
                    break;

                case z80r_hl:
                    StreamCodeByte (csfile, 0xE5); /* push hl */
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0xE1); /* pop ix */
                    break;

                case z80r_ix:
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x64); /* ld ixh,ixh */
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0x6D); /* ld ixl,ixl */
                    break;
                case z80r_iy:
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0xE5); /* push iy */
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0xE1); /* pop ix */
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80r_iy:
            switch(srcreg) {
                case z80r_bc:
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x60); /* ld iyh,b */
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x69); /* ld iyl,c */
                    break;

                case z80r_de:
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x62); /* ld iyh,d */
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x6B); /* ld iyl,e */
                    break;

                case z80r_hl:
                    StreamCodeByte (csfile, 0xE5); /* push hl */
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0xE1); /* pop iy */
                    break;

                case z80r_ix:
                    StreamCodeByte (csfile, 0xDD);
                    StreamCodeByte (csfile, 0xE5); /* push ix */
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0xE1); /* pop iy */
                    break;
                case z80r_iy:
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x64); /* ld iyh,iyh */
                    StreamCodeByte (csfile, 0xFD);
                    StreamCodeByte (csfile, 0x6D); /* ld iyl,iyl */
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief
 * Generate opcodes for:
 * LD bc|de|hl|sp|ix|iy , nn
 * LD sp, hl|ix,iy
 * \param csfile
 * \param destreg
 */
static void
LD_16bitDestReg_SrcDirect (sourcefile_t *csfile, enum z80rr destreg)
{
    int bytepos;
    enum z80rr srcreg = CheckReg16Mnem (csfile);

    switch (srcreg) {
        case z80rr_unknown:
            switch (destreg) {
                /* LD  ix|iy , nn */
                case z80r_ix:
                case z80r_iy:
                    StreamCodeByte (csfile, (destreg == z80r_ix) ? 0xdd : 0xfd);
                    StreamCodeByte (csfile, 33);
                    bytepos = 2;
                    break;

                default:
                    /* LD  bc|de|hl|sp , nn */
                    StreamCodeByte (csfile, (unsigned char) (destreg * 16 + 1));
                    bytepos = 1;
                    break;
            }

            ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, bytepos);
            break;

        case z80r_hl:
            if (destreg == z80r_sp) {
                /* LD  SP,HL  */
                StreamCodeByte (csfile, 249);
            } else {
                LDMeta_16bitDestReg_16bitSrcReg(csfile, destreg, z80r_hl);
            }
            break;

        case z80r_ix:
            /* LD  SP,IX */
            if (destreg == z80r_sp) {
                StreamCodeByte (csfile, 0xDD);
                StreamCodeByte (csfile, 249);
            } else {
                LDMeta_16bitDestReg_16bitSrcReg(csfile, destreg, z80r_ix);
            }
            break;

        case z80r_iy:
            /* LD  SP,IY */
            if (destreg == z80r_sp) {
                StreamCodeByte (csfile, 0xFD);
                StreamCodeByte (csfile, 249);
            } else {
                LDMeta_16bitDestReg_16bitSrcReg(csfile, destreg, z80r_iy);
            }
            break;

        default:
            LDMeta_16bitDestReg_16bitSrcReg(csfile, destreg, srcreg);
            break;
    }
}


/*!
 * \brief LD r8,(IX|IY [offset expr])
 * \param csfile
 * \param dstreg8
 * \param srcidxreg
 */
static void
LD_Meta_8bitDestReg_SrcIndirectIndex(sourcefile_t *csfile, enum z80r dstreg8, enum z80rr srcidxreg, expression_t *offsetexpr, int offset)
{
    StreamCodeByte (csfile, (srcidxreg == z80r_ix) ? 0xdd : 0xfd);
    StreamCodeByte (csfile, (unsigned char) (64 + dstreg8 * 8 + 6));
    if (offsetexpr == NULL) {
        /* no displacement expression use specified offset */
        StreamCodeByte (csfile, (unsigned char) offset);
    } else {
        /* update PC of pre-parsed expression to point at actual PC position, where index offset
           expression will be placed by the expression evaluation (now or later in pass 2 or linking */
        offsetexpr->codepos = (size_t) GetStreamCodePC(csfile);
        ProcessExpr (csfile, offsetexpr, RANGE_8SIGN, 2);
    }
}


/*!
 * \brief LD BC|DE|HL,(IX|IY [offset expr])
 * \details
 * The 8bit destination is specified as B for BC, D for DE, H for HL,
 * because two individual 8bit instructions are generated
 * \param csfile
 * \param dstreg8
 * \param srcidxreg
 */
static void
LD_Meta_16bitDestReg_SrcIndirectIndex(sourcefile_t *csfile, enum z80r dstreg8, enum z80rr srcidxreg)
{
    expression_t *offsetexpr;
    expression_t *offsetexpr1;

    if (GetSym(csfile) == rparen) {
        /* no offset expression were specified, use 0 and 1 as constant offsets */
        LD_Meta_8bitDestReg_SrcIndirectIndex(csfile, dstreg8+1, srcidxreg, NULL, 0); /* first lower 8bit of 16bit register */
        LD_Meta_8bitDestReg_SrcIndirectIndex(csfile, dstreg8, srcidxreg, NULL, 1); /* then upper 8bit of 16bit register */
    } else {
        offsetexpr = ParseNumExpr (csfile);
        offsetexpr1 = CloneExprPlusOne(offsetexpr);
        LD_Meta_8bitDestReg_SrcIndirectIndex(csfile, dstreg8+1, srcidxreg, offsetexpr, 0);
        LD_Meta_8bitDestReg_SrcIndirectIndex(csfile, dstreg8, srcidxreg, offsetexpr1, 1);
    }
}


/*!
 * \brief
 * Generate opcodes for:
 * LD bc|de|hl|sp|ix|iy , (nn)
 * LD bc|de|hl,(ix+n) meta instruction
 * \param csfile
 * \param destreg
 */
static void
LD_16bitDestReg_SrcindDirect(sourcefile_t *csfile, enum z80rr destreg)
{
    int bytepos;
    enum z80rr srcreg16;

    GetSym(csfile);
    switch (destreg) {
        case z80r_hl:
            srcreg16 = CheckReg16Mnem (csfile);
            if (srcreg16 == z80r_ix || srcreg16 == z80r_iy) {
                /* ld hl,(ix+n) meta instruction */
                /* ld hl,(iy+n) meta instruction */
                LD_Meta_16bitDestReg_SrcIndirectIndex(csfile, z80r_h, srcreg16);
                return;
            } else {
                /* LD HL,(nn) */
                StreamCodeByte (csfile, 42);
                bytepos = 1;
            }
            break;

        case z80r_bc:
            srcreg16 = CheckReg16Mnem (csfile);
            switch(srcreg16) {
                case z80r_hl:
                    /* ld bc,(hl) meta instruction */
                    StreamCodeByte (csfile, 0x4E); /* ld c,(hl) */
                    StreamCodeByte (csfile, 0x23); /* inc hl */
                    StreamCodeByte (csfile, 0x46); /* ld b,(hl) */
                    StreamCodeByte (csfile, 0x2b); /* dec hl */
                    GetSym(csfile); /* fetch ')' */
                    return;

                case z80r_ix:
                case z80r_iy:
                    /* ld bc,(ix+n) meta instruction */
                    /* ld bc,(iy+n) meta instruction */
                    LD_Meta_16bitDestReg_SrcIndirectIndex(csfile, z80r_b, srcreg16);
                    return;

                default:
                    /* ld bc,(nn) */
                    StreamCodeByte (csfile, 0xED);
                    StreamCodeByte (csfile, (unsigned char) (75 + destreg * 16));
                    bytepos = 2;
            }
            break;

        case z80r_de:
            srcreg16 = CheckReg16Mnem (csfile);
            switch(srcreg16) {
                case z80r_hl:
                    /* ld de,(hl) meta instruction */
                    StreamCodeByte (csfile, 0x5E); /* ld e,(hl) */
                    StreamCodeByte (csfile, 0x23); /* inc hl */
                    StreamCodeByte (csfile, 0x56); /* ld d,(hl) */
                    StreamCodeByte (csfile, 0x2b); /* dec hl */
                    GetSym(csfile); /* fetch ')' */
                    return;

                case z80r_ix:
                case z80r_iy:
                    /* ld de,(ix+n) meta instruction */
                    /* ld de,(iy+n) meta instruction */
                    LD_Meta_16bitDestReg_SrcIndirectIndex(csfile, z80r_d, srcreg16);
                    return;

                default:
                    /* ld de,(nn) */
                    StreamCodeByte (csfile, 0xED);
                    StreamCodeByte (csfile, (unsigned char) (75 + destreg * 16));
                    bytepos = 2;
            }
            break;

        case z80r_ix:
        case z80r_iy:
            /* LD IX|IY, (nn) */
            StreamCodeByte (csfile, (destreg == z80r_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, 42);
            bytepos = 2;
            break;

        case z80r_sp:
            /* ld sp,(nn) */
            StreamCodeByte (csfile, 0xED);
            StreamCodeByte (csfile, (unsigned char) (75 + destreg * 16));
            bytepos = 2;
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            return;
    }

    /* process address expression for LD BC|DE|HL|SP|IX|IY,(nn) */
    ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, bytepos);
    GetSym(csfile); /* fetch ')' */
}


/*!
 * \brief
 * Generate opcodes for:
 * LD rr,(nn)
 * LD rr,nn
 * LD SP,HL|IX|IY
 *
 * \param csfile
 */
static void
LD_16bitDestReg (sourcefile_t *csfile)
{
    enum z80rr destreg = GetLD16bitDestReg(csfile);

    if (asmerror == true)
        return;

    if (GetSym(csfile) != lparen) {
        LD_16bitDestReg_SrcDirect(csfile, destreg);
    } else {
        LD_16bitDestReg_SrcindDirect(csfile, destreg);
    }
}


static void
PUSHPOP (sourcefile_t *csfile, int opcode)
{
    do {
        if (GetSym(csfile) == name) {
            PushPop_instr (csfile, opcode, CheckReg16Mnem (csfile));
        } else {
            ReportError (csfile, Err_Syntax);
            break;
        }
    } while (GetSym(csfile) == comma);
}


static void
BitSrcRg8BitIndrct(sourcefile_t *csfile, int opcode, int bitnumber)
{
    enum z80ia iareg;
    enum z80r reg;

    switch ((iareg = CheckIndirectAddrMnem (csfile))) {
        case z80ia_hl:
            /* (HL)  */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, (unsigned char) (opcode + (bitnumber * 8 + 6)));
            break;

        case z80ia_ix:
        case z80ia_iy:
            StreamCodeByte (csfile, (iareg == z80ia_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, 0xCB);

            if (csfile->sym == rparen) {
                /* no displacement specified, use 0 */
                StreamCodeByte (csfile, 0);
            } else {
                ProcessExpr (csfile, NULL, RANGE_8SIGN, 2);
            }

            if (GetSym(csfile) != comma) {
                StreamCodeByte (csfile, (unsigned char) (opcode + (bitnumber * 8 + 6)));
                break;
            }

            /* Support undocumented instructions XXX (IX/IY+d),r */
            GetSym(csfile);
            reg = CheckReg8Mnem (csfile);
            if (reg <= 7) {
                StreamCodeByte (csfile, (unsigned char) opcode + (unsigned char) (bitnumber * 8) + (unsigned char) reg);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


/* BIT, SET, RES (Main & IX/IY registers) */
static void
BitSetResZ80 (sourcefile_t *csfile, int opcode)
{
    int bitnumber;

    GetSym(csfile);
    bitnumber = (int) ParseMnemConstant(csfile);
    if (bitnumber < 0 || bitnumber > 7) {
        ReportError (csfile, Err_IntegerRange);
        return;
    }

    /* bit 0 - 7 */
    if (csfile->sym != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (GetSym(csfile) == lparen) {
        BitSrcRg8BitIndrct(csfile, opcode, bitnumber);
    } else {
        /* no indirect addressing, try to get an 8bit register */
        BitSrcRg8bit(csfile, opcode, bitnumber);
    }
}


/* RLC, RRC, RL, RR, SLA, SRA, SRL, SLL */
static void
RotShiftZ80 (sourcefile_t *csfile, int opcode)
{
    enum z80r reg;
    enum z80rr dstreg16;
    enum z80ia iareg;

    if (GetSym(csfile) == lparen) {
        switch ((iareg = CheckIndirectAddrMnem (csfile))) {
            case z80ia_hl:
                StreamCodeByte (csfile, 0xCB);
                StreamCodeByte (csfile, (unsigned char) (opcode * 8 + 6));
                break;

            case z80ia_ix:
            case z80ia_iy:
                /* XXX (IX/IY+d), DDCBnnXX or FDCBnnXX instruction opcode */
                StreamCodeByte (csfile, (iareg == z80ia_ix) ? 0xdd : 0xfd);
                StreamCodeByte (csfile, 0xCB);

                if (csfile->sym == rparen) {
                    /* no displacement specified, use 0 */
                    StreamCodeByte (csfile, 0);
                } else {
                    ProcessExpr (csfile, NULL, RANGE_8SIGN, 2);
                }

                if (GetSym(csfile) != comma) {
                    StreamCodeByte (csfile, (unsigned char) (opcode * 8 + 6));
                    break;
                }

                /* Support undocumented instructions XXX (IX/IY+d),r */
                GetSym(csfile);
                reg = CheckReg8Mnem (csfile);
                if (reg <= 7) {
                    StreamCodeByte (csfile, (unsigned char) opcode * 8 + (unsigned char) reg);
                } else {
                    ReportError (csfile, Err_IllegalIdent);
                }
                break;

            default:
                ReportError (csfile, Err_IllegalIdent);
                break;
        }
    } else {
        /* no indirect addressing, try to get an 8bit register */
        dstreg16 = CheckReg16Mnem (csfile);
        if (dstreg16 == z80rr_unknown) {
            /* 8bit register shift rotate instruction group */
            RotShift8bitReg (csfile, opcode);
        } else {
            /* 16bit shift/rotate meta instructions */
            switch(opcode) {
                case 2: /* RLW r16 meta instruction */
                    RLWr16(csfile, dstreg16);
                    break;
                case 3: /* RRW r16 meta instruction */
                    RRWr16(csfile, dstreg16);
                    break;
                case 4: /* SLAW r16 meta instruction */
                    SLAWr16(csfile, dstreg16);
                    break;
                case 5: /* SRAW r16 meta instruction */
                    SRAWr16(csfile, dstreg16);
                    break;
                case 7: /* SRLW r16 meta instruction */
                    SRLWr16(csfile, dstreg16);
                    break;
                default:
                    ReportError (csfile, Err_IllegalIdent);
                    break;
            }
        }
    }
}


/*!
 * \brief Parsing & code generation of ADD | ADC | SBC | SUB | AND | OR | XOR | CP  [A,] (HL) | (IX|IY [+offset])
 * \param csfile
 * \param opcode
 */
static void
ArithLog8bitIndrctZ80(sourcefile_t *csfile, int opcode)
{
    enum z80ia reg16 = CheckIndirectAddrMnem (csfile);

    switch (reg16) {
        case z80ia_hl:
            /* xxx  A,(HL) */
            StreamCodeByte (csfile, (unsigned char) (128 + opcode * 8 + 6));
            break;

        case z80ia_ix:
        case z80ia_iy:
            /* xxx A,(IX|IY+d) */
            StreamCodeByte (csfile, (reg16 == z80ia_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (unsigned char) (128 + opcode * 8 + 6));
            if (csfile->sym == rparen) {
                /* no displacement specified, use 0 */
                StreamCodeByte (csfile, 0);
            } else {
                ProcessExpr (csfile, NULL, RANGE_8SIGN, 2);
            }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


static void
ADCSBC (sourcefile_t *csfile, int opc8bitreg, int opc16bitreg)
{
    const unsigned char *ptr = MemfTellPtr(csfile->mf);

    GetSym(csfile);
    switch (CheckReg16Mnem (csfile)) {
        case z80rr_unknown:
            /* 16 bit register wasn't found - try to evaluate the 8 bit version */
            MemfSeekPtr(csfile->mf, ptr);
            ArithLog8bitZ80(csfile, opc8bitreg);
            break;

        case z80r_hl:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }
            AdcSbcHL(csfile, opc16bitreg);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


static void
IncDecIndrct8bitZ80 (sourcefile_t *csfile, int opcode)
{
    enum z80ia rr = CheckIndirectAddrMnem (csfile);

    switch (rr) {
        case z80ia_hl:
            /* INC/DEC (HL) */
            StreamCodeByte (csfile, (unsigned char) (48 + opcode));
            break;

        case z80ia_ix:
        case z80ia_iy:
            /* INC/DEC (IX|IY+d) */
            StreamCodeByte (csfile, (rr == z80ia_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (unsigned char) (48 + opcode));
            if (csfile->sym == rparen) {
                /* no displacement specified, use 0 */
                StreamCodeByte (csfile, 0);
            } else {
                ProcessExpr (csfile, NULL, RANGE_8SIGN, 2);
            }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


static void
LD_address_indrct (sourcefile_t *csfile)
{
    enum z80rr sourcereg;
    expression_t *addrexpr = ParseNumExpr (csfile);
    int bytepos;

    if (addrexpr == NULL) {
        return;    /* parse to right bracket */
    }

    if (csfile->sym != rparen) {
        ReportError (csfile, Err_Syntax);   /* Right bracket missing! */
        FreeNumExpr (addrexpr);             /* remove this expression again */
        return;
    }

    if (GetSym(csfile) == comma) {
        GetSym(csfile);
        switch (sourcereg = CheckReg16Mnem (csfile)) {
            case z80r_hl:
                /* LD  (nn),HL  */
                StreamCodeByte (csfile, 34);
                bytepos = 1;
                break;

            case z80r_bc:
            case z80r_de:     /* LD  (nn),dd  => dd: BC,DE,SP  */
            case z80r_sp:
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, (unsigned char) (67 + sourcereg * 16));
                bytepos = 2;
                break;

            case z80r_ix:
                /* LD  (nn),IX */
                StreamCodeByte (csfile, 0xDD);
                StreamCodeByte (csfile, 34);
                bytepos = 2;
                break;

            case z80r_iy:
                /* LD (nn),IY */
                StreamCodeByte (csfile, 0xFD);
                StreamCodeByte (csfile, 34);
                bytepos = 2;
                break;

            case z80rr_unknown:
                if (CheckReg8Mnem (csfile) == z80r_a) {
                    /* LD (nn),A */
                    StreamCodeByte (csfile, 50);
                    bytepos = 1;
                } else {
                    ReportError (csfile, Err_IllegalIdent);
                    FreeNumExpr (addrexpr);
                    return;
                }
                break;

            default:
                ReportError (csfile, Err_IllegalIdent);
                FreeNumExpr (addrexpr);
                return;
        }
    } else {
        ReportError (csfile, Err_Syntax);
        FreeNumExpr (addrexpr);
        return;
    }

    /* update PC of pre-parsed expression to point at actual PC position
     * (where address will be placed by the expression evaluation (now or later) */
    addrexpr->codepos = (size_t) GetStreamCodePC(csfile);
    ProcessExpr (csfile, addrexpr, RANGE_LSB_16CONST, bytepos);
}


/*
 * LD  r,(HL)
 * LD  r,(IX|IY+d)
 * LD  A,(nn)
 * LD  A,(BC|DE)
 */
static void
LD_r_8bit_indrct (sourcefile_t *csfile, enum z80r destreg)
{
    enum z80ia sourcereg = CheckIndirectAddrMnem (csfile);

    switch (sourcereg) {
        case z80ia_hl:
            /* LD r,(HL) */
            StreamCodeByte (csfile, (unsigned char) (64 + destreg * 8 + 6));
            break;

        case z80ia_ix:
        case z80ia_iy:
            /* check that IXh/l and IYh/l are not mixed, which is not supported by Z80 */
            if ( destreg == z80r_ixh || destreg == z80r_ixl || destreg == z80r_iyh || destreg == z80r_iyl) {
                /* LD ixh/l,(IY+d) */
                ReportError (csfile, Err_IllegalIdent);
                return;
            }

            StreamCodeByte (csfile, (sourcereg == z80ia_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (unsigned char) (64 + destreg * 8 + 6));
            if (csfile->sym == rparen) {
                /* no displacement specified, use 0 */
                StreamCodeByte (csfile, 0);
            } else {
                ProcessExpr (csfile, NULL, RANGE_8SIGN, 2);
            }
            break;

        case z80ia_nn:
            /* LD  A,(nn) */
            if (destreg == z80r_a) {
                StreamCodeByte (csfile, 58);
                ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, 1);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80ia_bc:
            if (destreg == z80r_a) {
                /* LD A,(BC) */
                StreamCodeByte (csfile, 10);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80ia_de:
            if (destreg == z80r_a) {
                /* LD A,(DE) */
                StreamCodeByte (csfile, 26);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


static void
IncDecZ80(sourcefile_t *csfile, int opc8, int opc16)
{
    enum z80rr reg16;

    GetSym(csfile);
    reg16 = CheckReg16Mnem (csfile);
    if (reg16 == z80rr_unknown) {
        /* 16 bit register wasn't found - try to evaluate the 8bit version */
        if (csfile->sym == lparen) {
            IncDecIndrct8bitZ80 (csfile, opc8);
        } else {
            /* no indirect addressing, try to get an 8bit register */
            IncDec8bitRegZ80(csfile, opc8);
        }
    } else
        IncDec16bit(csfile, reg16, opc16);
}


/*!
 * \brief RLW BC|DE|HL meta instructions
 * \param csfile
 * \param dstreg16
 */
static void
RLWr16(sourcefile_t *csfile, enum z80rr dstreg16)
{
    switch(dstreg16) {
        case z80r_bc:
            /* rl c : rl b */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x11);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x10);
            break;

        case z80r_de:
            /* rl e : rl d */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x13);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x12);
            break;

        case z80r_hl:
            /* rl l : rl h */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x15);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x14);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief RRW BC|DE|HL meta instructions
 * \param csfile
 * \param dstreg16
 */
static void
RRWr16(sourcefile_t *csfile, enum z80rr dstreg16)
{
    switch(dstreg16) {
        case z80r_bc:
            /* rr b : rr c */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x18);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x19);
            break;

        case z80r_de:
            /* rr d : rr e */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x1A);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x1B);
            break;

        case z80r_hl:
            /* rr h : rr l */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x1C);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x1D);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief SLAW BC|DE|HL meta instructions
 * \param csfile
 * \param dstreg16
 */
static void
SLAWr16(sourcefile_t *csfile, enum z80rr dstreg16)
{
    switch(dstreg16) {
        case z80r_bc:
            /* sla c : rl b */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x21);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x10);
            break;

        case z80r_de:
            /* sla e : rl d */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x23);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x12);
            break;

        case z80r_hl:
            /* add hl,hl */
            StreamCodeByte (csfile, 0x29);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief SRAW BC|DE|HL meta instructions
 * \param csfile
 * \param dstreg16
 */
static void
SRAWr16(sourcefile_t *csfile, enum z80rr dstreg16)
{
    switch(dstreg16) {
        case z80r_bc:
            /* sra b : rr c */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x28);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x19);
            break;

        case z80r_de:
            /* sra d : rr e */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x2A);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x1B);
            break;

        case z80r_hl:
            /* sra h : rr l */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x2C);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x1D);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief SRLW BC|DE|HL meta instructions
 * \param csfile
 * \param dstreg16
 */
static void
SRLWr16(sourcefile_t *csfile, enum z80rr dstreg16)
{
    switch(dstreg16) {
        case z80r_bc:
            /* srl b : rr c */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x38);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x19);
            break;

        case z80r_de:
            /* srl d : rr e */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x3A);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x1B);
            break;

        case z80r_hl:
            /* srl h : rr l */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x3C);
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, 0x1D);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief Parse & validate "(HL)" syntax
 * \param csfile
 * \return z80r_hl if syntax is valid, or z80rr_unknown
 */
static enum z80rr
FetchHLIndirect(sourcefile_t *csfile)
{
    enum z80rr rhl = z80rr_unknown;

    if (GetSym(csfile) == lparen) {
        /* validate (HL) */
        GetSym(csfile);
        rhl = CheckReg16Mnem (csfile);
        if (CheckReg16Mnem (csfile) == z80r_hl) {
            GetSym(csfile); /* silently fetch ')' */
            return rhl;
        } else {
            ReportError (csfile, Err_Syntax);
        }
    }

    return rhl;
}


/******************************************************************************
                               Public functions
 ******************************************************************************/


unsigned char *
InitRelocTable( void )
{
    if ((reloctable = AllocRelocTable()) != NULL) {
        relocptr = reloctable;
        relocptr += 4;                /* point at first offset to store */
        totalrelocaddr = 0;
        sizeof_reloctable = 0;        /* relocation table, still 0 elements .. */
    }

    return reloctable;
}


void
RegisterRelocEntry( unsigned short progcnt )
{
    static const char *errmsg = "Insufficient memory for allocating relocation entry";
    relocoffset_t *p = AllocRelocOffset();

    if (p == NULL) {
        ReportRuntimeErrMsg  (NULL, errmsg);
        return;
    }

    p->offset = progcnt;

    if (LookupRelocOffset (progcnt) == NULL) {
        /* Insert new relocation offset object into AVL tree */
        if (!Insert (&relocationoffsets, p, (compfunc_t) cmprelocoffset)) {
            ReportRuntimeErrMsg  (NULL, errmsg);
            free(p);
        }
    } else {
        /* offset already registered - shouldn't happen */
        free(p);
    }
}


void
WriteRelocTable( char *filename )
{
    if (relocationoffsets != NULL && filename != NULL) {
        /* only write relocation table if a relocation entries were registered */
        /* Generate relocation offsets in chronological order */
        InOrder (relocationoffsets, (void (*)(void *)) GenerateRelocTable);

        reloctable[0] = (unsigned char) totalrelocaddr % 256U;              /* create 4 byte header */
        reloctable[1] = (unsigned char) totalrelocaddr / 256U;              /* - total of relocation elements */
        reloctable[2] = (unsigned char) sizeof_reloctable % 256U;
        reloctable[3] = (unsigned char) sizeof_reloctable / 256U;           /* - total size of relocation table elements */

        WriteBuffer2HostFile(filename, "ab", reloctable, sizeof_reloctable + 4);    /* then write/append relocation table inclusive 4 byte header */

        if (verbose == true)
            printf ("Relocation table is %d bytes.\n", (sizeof_reloctable + 4));
    }
}


void
WriteRelocRoutine( char *filename )
{
    if (filename != NULL) {
        /* the relocate routine (first part of relocation header) */
        WriteBuffer2HostFile(filename, "wb", reloc_routine, sizeof_relocroutine);

        if (verbose == true)
            printf ("Relocation routine is %d bytes.\n", (int) sizeof_relocroutine);
    }
}


void
FreeRelocTable ( void )
{
    if ( reloctable != NULL) {
        free (reloctable);
    }

    if ( relocationoffsets != NULL ) {
        DeleteAll(&relocationoffsets, (void (*)(void *)) FreeRelocOffset);
    }
}


/*!
 * \brief Return pointer to function that parses Z80 mnemonic source line and generates code
 * \param st symbol type
 * \param id pointer to identifier
 * \return pointer to mnemonic parser function
 */
mnemprsrfunc
LookupZ80Mnemonic(enum symbols st, const char *id)
{
    return LookupIdentifier (id, st, (identfunc_t *) z80idents, sizeof(z80idents)/sizeof(idf_t));
}


void
ParseLineZ80 (sourcefile_t *csfile, const bool interpret)
{
    /* indicate no label (yet) on current line */
    csfile->labelname = NULL;

    /* preset line buffer to being empty */
    line[0] = '\0';

    /* update assembler program counter for '$PC' */
    gAsmpcPtr->symvalue = (symvalue_t) GetStreamCodePC(csfile);

    /* preserve the beginning of the current line, for reference */
    csfile->lineptr = MemfTellPtr (csfile->mf);
    ++csfile->lineno;

    if (uselistingfile == true) {
        /* get a copy of current source line in a separate buffer */
        GetLine (csfile, line, csfile->lineptr);
    }

    csfile->eol = false;   /* reset END OF LINE flag */
    GetSym(csfile);        /* and fetch first symbol on line */

    if (csfile->sym == colonlabel) {
        if (interpret == true) {
            /* Generate only possible label declaration if line parsing is allowed */
                csfile->labelname = DefineSymbol (csfile, csfile->ident, gAsmpcPtr->symvalue, SYMADDR | SYMTOUCHED);
                if (csfile->labelname != NULL) {
                    AddAddress (csfile->labelname, &addresses);
                }

                GetSym(csfile);    /* after a label, there might be a directive or an instruction... */
        } else {
            /* line interpretation disabled - colon label and rest of line */
            SkipLine (csfile);
            csfile->sym = newline;
        }
    } else {
        if (csfile->sym == fullstop) {
            if (interpret == true) {
                if (GetSym(csfile) == name) {
                    /* .nnnn can either be a directive or a label */
                    if (LookupDirective(csfile->sym, csfile->ident) == NULL) {
                        /* directive wasn't recognized - define it as a label */
                        csfile->labelname = DefineSymbol (csfile, csfile->ident, gAsmpcPtr->symvalue, SYMADDR | SYMTOUCHED);
                        if (csfile->labelname != NULL) {
                            AddAddress (csfile->labelname, &addresses);
                        }

                        GetSym(csfile);    /* after a label, there might be a directive or an instruction... */
                    }
                } else {
                    /* a name must follow a directive or label specifier */
                    ReportError (csfile, Err_IllegalIdent);
                    return;
                }
            } else {
                /* line interpretation disabled - ignore label/directive and rest of line */
                SkipLine (csfile);
                csfile->sym = newline;
            }
        }
    }

    switch (csfile->sym) {
        case name:
            /* Parse source code line based on current <ident> name, which can be either a directive, a cpu mnemonic or a macro. */
            ParseCpuMnemonic (csfile, interpret);
            break;

        case newline:
            break;        /* empty line, get next... */

        default:
            if ( interpret == true ) { /* only report error if line parsing is enabled */
                ReportError (csfile, Err_Syntax);
            }
    }

    if (uselistingfile == true && writeline == true) {
        WriteListFileLine (csfile);    /* Write current source line to list file, if allowed */
    }
}


/*!
 * \brief Validate current identifier as a valid CC mnemonic
 * \details
    Via current (source) file identifier, validate Z80 instruction
    condition codes:
    Z, NZ, C, NC, PE, PO, P, M, S, NS, V & NV
 *
 * \param csfile
 * \return return a recognized enumerated type or <z80f_unknown>
 */
enum z80cc
CheckCondMnem (const sourcefile_t *csfile)
{
    if (csfile->sym != name)
        return z80f_unknown;

    switch(csfile->identlen) {
        case 1:
            switch(csfile->ident[0]) {
                case 'Z':
                    return z80f_z;
                case 'C':
                    return z80f_c;
                case 'P':
                    return z80f_p;
                case 'M':
                    return z80f_m;
                case 'S':
                    return z80f_m; /* "S" used by Z380 as alias for "M", but also valid alternative for Z80 */
                case 'V':
                    return z80f_pe; /* "V" used by Z380 as alias for "PE", but also valid alternative for Z80 */
                default:
                    return z80f_unknown;
            }

        case 2:
            switch(csfile->ident[0]) {
                case 'N':
                    switch (csfile->ident[1]) {
                        case 'Z':
                            return z80f_nz;
                        case 'C':
                            return z80f_nc;
                        case 'S':
                            return z80f_p; /* "NS", used by Z380 as alias for "P", but also valid alternative for Z80 */
                        case 'V':
                            return z80f_po; /* "NV", used by Z380 as alias for "PO", but also valid alternative for Z80 */
                        default:
                            return z80f_unknown;
                    }

               case 'P':
                    switch (csfile->ident[1]) {
                        case 'O':
                            return z80f_po;
                        case 'E':
                            return z80f_pe;
                        default:
                            return z80f_unknown;
                    }

                default:
                    return z80f_unknown;
            }

        default:
            return z80f_unknown;
    }
}


/*!
 * \brief validate Z80 8bit register mnemonic
 * \details
 * Via current (source) file identifier, validate Z80 8bit register
    A, B, C, D, E, H, L, I, R, F, IXL, IXH, IYL, IYH
 *
 * \param csfile
 * \return return a recognized enumerated type, or <z80r_unknown>
 */
enum z80r
CheckReg8Mnem (const sourcefile_t *csfile)
{
    if (csfile->sym != name)
        return z80r_unknown;

    switch(csfile->identlen) {
        case 1:
            switch (csfile->ident[0]) {
                case 'A':
                    return z80r_a;
                case 'H':
                    return z80r_h;
                case 'B':
                    return z80r_b;
                case 'L':
                    return z80r_l;
                case 'C':
                    return z80r_c;
                case 'D':
                    return z80r_d;
                case 'E':
                    return z80r_e;
                case 'I':
                    return z80r_i;
                case 'R':
                    return z80r_r;
                case 'F':
                    return z80r_f;
                default:
                    return z80r_unknown;
            }

        case 2:
            if (strcmp (csfile->ident, "XL") == 0) {
                return z80r_ixl;
            } else if (strcmp (csfile->ident, "XH") == 0) {
                return z80r_ixh;
            } else if (strcmp (csfile->ident, "YL") == 0) {
                return z80r_iyl;
            } else if (strcmp (csfile->ident, "YH") == 0) {
                return z80r_iyh;
            }
            return z80r_unknown;

        case 3:
            if (strcmp (csfile->ident, "IXL") == 0) {
                return z80r_ixl;
            } else if (strcmp (csfile->ident, "IXU") == 0) {
                return z80r_ixh;
            } else if (strcmp (csfile->ident, "IXH") == 0) {
                return z80r_ixh;
            } else if (strcmp (csfile->ident, "IYL") == 0) {
                return z80r_iyl;
            } else if (strcmp (csfile->ident, "IYU") == 0) {
                return z80r_iyh;
            } else if (strcmp (csfile->ident, "IYH") == 0) {
                return z80r_iyh;
            }
            return z80r_unknown;

        default:
            return z80r_unknown;
    }
}


/*!
 * \brief validate Z80 16bit register mnemonic
 * \details
 * Via current (source) file identifier, validate Z80 16bit register
    BC, DE, HL, IX, IY, AF, SP

 * \param csfile
 * \return return a recognized enumerated type, or <z80rr_unknown>
 */
enum z80rr
CheckReg16Mnem (const sourcefile_t *csfile)
{
    if (csfile->identlen != 2) {
        return z80rr_unknown;
    }

    switch (csfile->ident[0]) {
        case 'H':
            if (csfile->ident[1] == 'L') {
                return z80r_hl;
            }
            break;

        case 'B':
            if (csfile->ident[1] == 'C') {
                return z80r_bc;
            }
            break;

        case 'D':
            if (csfile->ident[1] == 'E') {
                return z80r_de;
            }
            break;

        case 'A':
            if (csfile->ident[1] == 'F') {
                return z80r_af;
            }
            break;

        case 'S':
            if (csfile->ident[1] == 'P') {
                return z80r_sp;
            }
            break;

        case 'I':
            switch (csfile->ident[1]) {
                case 'X':
                    return z80r_ix;

                case 'Y':
                    return z80r_iy;

                default:
                    return z80rr_unknown;
            }

        default:
            return z80rr_unknown;
    }

    return z80rr_unknown;
}


/* ---------------------------------------------------------------------------
    enum z80ia CheckIndirectAddrMnem (sourcefile_t *csfile)

    This function will parse the current line for an indirect addressing mode.
    The returned valid enumeration identifies:

    (BC); (DE); (HL)
    (SP <+|- expr.> ) Z380 only
    (IX <+|- expr.> )
    (IY <+|- expr.> )
    (nn), nn = 16bit address expression

    otherwise
    z80ia_unknown
   --------------------------------------------------------------------------- */
enum z80ia
CheckIndirectAddrMnem (sourcefile_t *csfile)
{
    enum z80rr reg16;

    GetSym(csfile);
    switch (reg16 = CheckReg16Mnem (csfile)) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
            if (GetSym(csfile) == rparen) {
                GetSym(csfile);
                return (enum z80ia) reg16;   /* indicate (BC), (DE), (HL) */
            } else {
                ReportError (csfile, Err_Syntax);   /* Right bracket missing! */
                return z80ia_unknown;
            }

        case z80r_sp: /* (SP + d) on Z380 only */
            GetSym(csfile);   /* prepare expression evaluation for displacement */
            return z80ia_sp;

        case z80r_ix:
        case z80r_iy:
            GetSym(csfile);   /* prepare expression evaluation for displacement */
            return (enum z80ia) reg16;

        case z80rr_unknown:   /* sym could be a '+', '-' or a symbol of an address expression... */
            return z80ia_nn;

        default:
            ReportError (csfile, Err_IllegalIdent);
            return z80ia_unknown;
    }
}


/**
 * @brief Get "rr16," (16bit destination register and ',' for LD
 * @param csfile
 * @return the 16bit register opcode enumeration
 */
enum z80rr
GetLD16bitDestReg (sourcefile_t *csfile)
{
    enum z80rr destreg = CheckReg16Mnem (csfile);

    if (destreg == z80rr_unknown) {
        ReportError (csfile, Err_Syntax);
        return destreg;
    }

    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return destreg;
    }

    if (destreg == z80r_af) {
        /* LD AF, xxx */
        ReportError (csfile, Err_IllegalIdent);
    }

    return destreg;
}


/*!
 * \brief LD i,a or LD r,a
 * \details "LD I" or "LD r" has been processed, validate ",A" and generate instruction opcode
 * \param csfile
 */
void
LD_ira(sourcefile_t *csfile, unsigned char opcode)
{
    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    GetSym(csfile);
    if (CheckReg8Mnem (csfile) == z80r_a) {
        /* LD  I,A or LD R,A */
        StreamCodeByte (csfile, 0xED);
        StreamCodeByte (csfile, opcode);
    } else {
        ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief Parsing & code generation of 8bit "<instr> [A,]xxx" for ADD, ADC, SBC, SUB, AND, OR, XOR & CP instructions
 * \param csfile
 * \param opcode
 */
void
ArithLog8bitZ80 (sourcefile_t *csfile, int opcode)
{
    const unsigned char *ptr = MemfTellPtr(csfile->mf);

    if (GetSym(csfile) == name && CheckReg8Mnem (csfile) == z80r_a) {
        if (GetSym(csfile) == comma) {
            /* parsed optional "A," */
            ptr = MemfTellPtr(csfile->mf);
        }
    }

    /* optional "A," may have been parsed, otherwise just be after 'XXX' mnemonic */
    MemfSeekPtr(csfile->mf, ptr);
    csfile->sym = nil;
    csfile->eol = false;

    if (GetSym(csfile) == lparen) {
        ArithLog8bitIndrctZ80(csfile, opcode);
    } else {
        /* no indirect addressing, parse 8bit source register */
        ArithLogSrc8RegZ80(csfile, opcode);
    }
}


void
PushPop_instr (sourcefile_t *csfile, int opcode, enum z80rr qq)
{
    switch (qq) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
            StreamCodeByte (csfile, (unsigned char) opcode + (unsigned char) qq * 16);
            break;

        case z80r_af:
            StreamCodeByte (csfile, (unsigned char) (opcode + 48));
            break;

        case z80r_ix:
        case z80r_iy:
            StreamCodeByte (csfile, (qq == z80r_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (unsigned char) (opcode + 32));
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/* ---------------------------------------------------------------------------
    EX (SP), HL | IX | IY

    "EX (" has already been parsed by EX().
    Finalize validation and generate byte code.
--------------------------------------------------------------------------- */
void
EX_SP(sourcefile_t *csfile)
{
    enum z80rr qq;

    if (GetSym(csfile) != name) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (CheckReg16Mnem (csfile) != z80r_sp) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    if (GetSym(csfile) != rparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (GetSym(csfile) != name) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    switch ( (qq = CheckReg16Mnem (csfile))) {
        case z80r_hl:
            /* EX  (SP),HL */
            StreamCodeByte (csfile, 227);
            break;

        case z80r_ix:
        case z80r_iy:
            /* EX  (SP),IX|IY */
            StreamCodeByte (csfile, (qq == z80r_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, 227);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


void
EX_AFaf(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 8);
}


/*!
 * \brief Generate 1st opcode for either CALL | JP or CALL cc | JP cc
 * \details 16bit address opcode is generated by caller
 * \param csfile
 * \param opcodenn
 * \param opcodecc
 */
void
CallJpZ80 (sourcefile_t *csfile, int opcodenn, int opcodecc)
{
    enum z80cc cc;

    if ((cc = CheckCondMnem (csfile)) != z80f_unknown) {
        /* JP CC,nn, CALL CC,nn, */
        StreamCodeByte (csfile, (unsigned char) opcodecc + (unsigned char) cc * 8);
        if (GetSym(csfile) == comma) {
            GetSym(csfile);
        } else {
            ReportError (csfile, Err_Syntax);
            return;
        }
    } else {
        /* JP nn, CALL nn */
        StreamCodeByte (csfile, (unsigned char) opcodenn);
    }
}


void
JRccOpcode(sourcefile_t *csfile)
{
    enum z80cc cc;

    switch (cc = CheckCondMnem (csfile)) {
        /* check for a condition */
        case z80f_nz:
        case z80f_z:
        case z80f_nc:
        case z80f_c:
            StreamCodeByte (csfile, 32 + (unsigned char) (cc * 8));
            if (GetSym(csfile) == comma) {
                GetSym(csfile);    /* point at start of address expression */
            } else {
                ReportError (csfile, Err_Syntax);   /* comma missing */
                return;
            }
            break;

        case z80f_unknown:
            /* opcode for JR  e */
            StreamCodeByte (csfile, 24);
            break;        /* identifier not a condition id - check for legal expression */

        default:
            ReportError (csfile, Err_Syntax);   /* illegal condition, syntax error */
            return;
    }
}


void
DJNZopcode(sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 16);

    if (csfile->sym == comma) {
        /* swallow optional comma and point at address expression */
        GetSym(csfile);
    }
}


/*!
 * \brief
 * Parsing & code generation of
 * ADD | ADC | SBC | SUB | AND | OR | XOR | CP  [A,] B | C | D | E | H | L | A | IXH | IXL | IYH | IYL
 * \param csfile
 * \param opcode
 */
void
ArithLogSrc8RegZ80(sourcefile_t *csfile, int opcode)
{
    long reg = CheckReg8Mnem (csfile);

    switch (reg) {
        /* 8bit register wasn't found, try to evaluate an expression */
        case z80r_unknown:
            /* xxx  A,n */
            StreamCodeByte (csfile, (unsigned char) (192 + opcode * 8 + 6));
            ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 1);
            break;

        case z80r_f:     /* xxx A,F illegal */
        case z80r_i:     /* xxx A,I illegal */
        case z80r_r:     /* xxx A,R illegal */
            ReportError (csfile, Err_IllegalIdent);
            break;

        default:
            if (reg & 8) {
                /* IXl or IXh */
                StreamCodeByte (csfile, 0xDD);
            } else if (reg & 16) {
                /* IYl or IYh */
                StreamCodeByte (csfile, 0xFD);
            }
            reg &= 7;

            /* xxx  A,r */
            StreamCodeByte (csfile, (unsigned char) (128 + opcode * 8 + reg));
            break;
    }
}


void
AddIxIyReg16(sourcefile_t *csfile, enum z80rr acc16, int opcode)
{
    enum z80rr reg16;

    GetSym(csfile);
    switch (reg16 = CheckReg16Mnem (csfile)) {
        case z80r_bc:
        case z80r_de:
        case z80r_sp:
            break;

        case z80r_ix:
        case z80r_iy:
            if (acc16 == reg16) {
                reg16 = z80r_hl;
            } else {
                ReportError (csfile, Err_IllegalIdent);
                return;
            }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            return;
    }

    StreamCodeByte (csfile, (acc16 == z80r_ix) ? 0xdd : 0xfd);
    StreamCodeByte (csfile, (unsigned char) opcode + (unsigned char) reg16 * 16);
}


void
AddHL(sourcefile_t *csfile, int opcode)
{
    enum z80rr reg16;

    GetSym(csfile);
    switch(reg16 = CheckReg16Mnem (csfile)) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
        case z80r_sp:
            /* ADD HL,rr */
            StreamCodeByte (csfile, (unsigned char) opcode + (unsigned char) reg16 * 16);
            break;
        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


void
AdcSbcHL (sourcefile_t *csfile, int opcode)
{
    enum z80rr reg16;

    GetSym(csfile);
    switch(reg16 = CheckReg16Mnem (csfile)) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
        case z80r_sp:
            StreamCodeByte (csfile, 0xed);
            StreamCodeByte (csfile, (unsigned char) opcode + (unsigned char) reg16 * 16);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


void
IncDec8bitRegZ80 (sourcefile_t *csfile, int opcode)
{
    enum z80r r8 = CheckReg8Mnem (csfile);

    switch (r8) {
        case z80r_f:     /* xxx A,F illegal */
        case z80r_i:     /* xxx A,I illegal */
        case z80r_r:     /* xxx A,R illegal */
            ReportError (csfile, Err_IllegalIdent);
            break;

        case z80r_ixh:
        case z80r_ixl:
        case z80r_iyh:
        case z80r_iyl:
            /* INC/DEC  ixh,ixl */
            /* INC/DEC  iyh,iyl */
            StreamCodeByte (csfile, (r8 == z80r_ixh || r8 == z80r_ixl) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (unsigned char) ((r8 & 7) * 8) + (unsigned char) opcode);
            break;

        case z80r_unknown:
            /* unknown register */
            ReportError (csfile, Err_IllegalIdent);
            break;

        default:
            /* INC/DEC r */
            StreamCodeByte (csfile, (unsigned char) (r8 * 8) + (unsigned char) opcode);
            break;
    }
}


void
IncDec16bit(sourcefile_t *csfile, const enum z80rr reg16, int opcode)
{
    switch (reg16) {
        case z80rr_unknown:   /* AF,BC,DE,HL,SP,IX,IY not found */
        case z80r_af:         /* not allowed here */
            ReportError (csfile, Err_IllegalIdent);
            break;

        case z80r_ix:
        case z80r_iy:
            StreamCodeByte (csfile, (reg16 == z80r_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (opcode == 3) ? 35: 43);
            break;

        default:
            StreamCodeByte (csfile, (unsigned char) (opcode) + (unsigned char) reg16 * 16);
            break;
    }
}


/*!
 * \brief BIT | SET | RES <bit>, A | B | C | D | E | H | L
 * \param csfile
 * \param opcode
 * \param bitnumber
 */
void BitSrcRg8bit(sourcefile_t *csfile, int opcode, int bitnumber)
{
    enum z80r reg = CheckReg8Mnem (csfile);

    switch (reg) {
        case z80r_f:
        case z80r_i:
        case z80r_r:
        case z80r_unknown:
            ReportError (csfile, Err_IllegalIdent);
            break;

        default:
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, (unsigned char) (opcode + bitnumber * 8 + (unsigned char) reg));
    }
}


/*!
 * \brief RLC | RRC | RL | RR | SLA | SRA | SLL | SRL A [B | C | D | E | H | L]
 * \param csfile
 * \param opcode
 */
void
RotShift8bitReg (sourcefile_t *csfile, int opcode)
{
    enum z80r reg;

    switch (reg = CheckReg8Mnem (csfile)) {
        case z80r_f:
        case z80r_i:
        case z80r_r:
        case z80r_unknown:
            ReportError (csfile, Err_IllegalIdent);
            break;

        default:
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, (unsigned char) opcode * 8 + (unsigned char) reg);
    }
}


void
ADD (sourcefile_t *csfile)
{
    enum z80rr acc16;
    const unsigned char *ptr = MemfTellPtr(csfile->mf);

    GetSym(csfile);
    switch (acc16 = CheckReg16Mnem (csfile)) {
        case z80rr_unknown:
            /* 16 bit register wasn't found - try to evaluate the 8 bit version */
            MemfSeekPtr(csfile->mf, ptr);
            ArithLog8bitZ80(csfile, 0);
            break;

        case z80r_hl:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }
            AddHL(csfile, 9);
            break;

        case z80r_ix:
        case z80r_iy:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }
            AddIxIyReg16(csfile, acc16, 9);
            break;

        default:
            ReportError (csfile, Err_UnknownIdent);
            break;
    }
}


void
SBC (sourcefile_t *csfile)
{
    ADCSBC (csfile, 3, 0x42);
}


void
ADC (sourcefile_t *csfile)
{
    ADCSBC (csfile, 1, 74);
}


void
INC (sourcefile_t *csfile)
{
    IncDecZ80(csfile, 4, 3);
}


void
DEC (sourcefile_t *csfile)
{
    IncDecZ80(csfile, 5, 11);
}


void
NOP (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0);
}


void
HALT (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 118);
}


/*!
 * \brief EX rr,rr group
 * \details
    Standard instructions:
        EX AF,AF'
        EX DE,HL
        EX (SP), HL|IX|IY

    Meta instructions:
        EX BC,DE
        EX BC,HL
        EX BC,IX
        EX BC,IY
        EX DE,IX
        EX DE,IY
        EX HL,IX
        EX HL,IY
        EX IX,IY
 *
 * \param csfile
 */
void
EX (sourcefile_t *csfile)
{
    enum z80rr src16;
    enum z80rr dst16;

    switch(GetSym(csfile)) {
        case lparen:
            /* finalize parsing and code generation of EX (SP),xx */
            EX_SP(csfile);
            break;

        case name:
            /* pre-validate a 16bit destination register */
            dst16 = CheckReg16Mnem (csfile);

            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }
            if (GetSym(csfile) != name) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            /* pre-validate a 16bit source register */
            src16 = CheckReg16Mnem (csfile);

            switch (dst16) {
                case z80r_bc:
                    switch(src16) {
                        case z80r_de:
                            /* EX BC,DE meta instruction */
                            StreamCodeByte (csfile, 0xC5);  /* push bc */
                            StreamCodeByte (csfile, 0x42);  /* ld b,d */
                            StreamCodeByte (csfile, 0x4B);  /* ld c,e */
                            StreamCodeByte (csfile, 0xD1);  /* pop de */
                            break;

                        case z80r_hl:
                            /* EX BC,HL meta instruction */
                            StreamCodeByte (csfile, 0xC5);  /* push bc */
                            StreamCodeByte (csfile, 0x44);  /* ld b,h */
                            StreamCodeByte (csfile, 0x4D);  /* ld c,l */
                            StreamCodeByte (csfile, 0xE1);  /* pop hl */
                            break;

                        case z80r_ix:
                            /* EX BC,IX meta instruction*/
                            StreamCodeByte (csfile, 0xC5);  /* push bc */
                            StreamCodeByte (csfile, 0xDD);  /* ex (sp),ix */
                            StreamCodeByte (csfile, 0xE3);
                            StreamCodeByte (csfile, 0xC1);  /* pop bc */
                            break;

                        case z80r_iy:
                            /* EX BC,IY meta instruction*/
                            StreamCodeByte (csfile, 0xC5);  /* push bc */
                            StreamCodeByte (csfile, 0xFD);  /* ex (sp),iy */
                            StreamCodeByte (csfile, 0xE3);
                            StreamCodeByte (csfile, 0xC1);  /* pop bc */
                            break;

                        default:
                            ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                case z80r_de:
                    switch(src16) {
                        case z80r_hl:
                            /* EX DE,HL */
                            StreamCodeByte (csfile, 0xEB);
                            break;

                        case z80r_ix:
                            /* EX DE,IX meta instruction*/
                            StreamCodeByte (csfile, 0xD5);  /* push de */
                            StreamCodeByte (csfile, 0xDD);  /* ex (sp),ix */
                            StreamCodeByte (csfile, 0xE3);
                            StreamCodeByte (csfile, 0xD1);  /* pop de */
                            break;

                        case z80r_iy:
                            /* EX DE,IY meta instruction*/
                            StreamCodeByte (csfile, 0xC5);  /* push de */
                            StreamCodeByte (csfile, 0xFD);  /* ex (sp),iy */
                            StreamCodeByte (csfile, 0xE3);
                            StreamCodeByte (csfile, 0xD1);  /* pop de */
                            break;

                        default:
                            ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                case z80r_hl:
                    switch(src16) {
                        case z80r_ix:
                            /* EX HL,IX meta instruction*/
                            StreamCodeByte (csfile, 0xE5);  /* push hl */
                            StreamCodeByte (csfile, 0xDD);  /* ex (sp),ix */
                            StreamCodeByte (csfile, 0xE3);
                            StreamCodeByte (csfile, 0xE1);  /* pop hl */
                            break;

                        case z80r_iy:
                            /* EX HL,IY meta instruction*/
                            StreamCodeByte (csfile, 0xE5);  /* push hl */
                            StreamCodeByte (csfile, 0xFD);  /* ex (sp),iy */
                            StreamCodeByte (csfile, 0xE3);
                            StreamCodeByte (csfile, 0xE1);  /* pop hl */
                            break;

                        default:
                            ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                case z80r_ix:
                    if (src16 == z80r_iy) {
                        /* EX IX,IY meta instruction*/
                        StreamCodeByte (csfile, 0xDD);  /* push ix */
                        StreamCodeByte (csfile, 0xE5);
                        StreamCodeByte (csfile, 0xFD);  /* ex (sp),iy */
                        StreamCodeByte (csfile, 0xE3);
                        StreamCodeByte (csfile, 0xDD);
                        StreamCodeByte (csfile, 0xE1);  /* pop ix */
                    } else {
                        ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                case z80r_af: /* only variation is EX AF,AF' */
                    if (src16 == z80r_af) {
                        EX_AFaf(csfile);
                        GetSym(csfile); /* silently fetch ' */
                    } else {
                        ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        default:
            ReportError (csfile, Err_Syntax);
    }
}


/*
 * LD (HL),r
 * LD (HL),n
 */
void
LD_HL8bit_indrct (sourcefile_t *csfile, enum z80r sourcereg)
{
    switch (sourcereg) {
        case z80r_f:
        case z80r_i:
        case z80r_r:
            ReportError (csfile, Err_IllegalIdent);
            break;

        case z80r_unknown:        /* LD  (HL),n  */
            StreamCodeByte (csfile, 54);
            ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 1);
            break;

        default:
            /* LD  (HL),r */
            StreamCodeByte (csfile, (unsigned char) (112 + sourcereg));
            break;
    }
}


void
LD (sourcefile_t *csfile)
{
    enum z80r sourcereg;
    enum z80r destreg;

    if (GetSym(csfile) == lparen) {
        LD_dst_indrct(csfile);
        return;
    }

    switch (destreg = CheckReg8Mnem (csfile)) {
        case z80r_unknown:
            /* LD rr,(nn)   ;  LD  rr,nn   ;   LD  SP,HL|IX|IY   */
            LD_16bitDestReg (csfile);
            break;

        case z80r_f:
            /* LD F,? */
            ReportError (csfile, Err_IllegalIdent);
            break;

        case z80r_i:
            /* LD  I,A */
            LD_ira(csfile, 71);
            break;

        case z80r_r:
            /* LD  R,A */
            LD_ira(csfile, 79);
            break;

        default:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            if (GetSym(csfile) == lparen) {
                /* LD  r,(HL)  ;   LD  r,(IX|IY+d)  */
                LD_r_8bit_indrct (csfile, destreg);
                return;
            }

            sourcereg = CheckReg8Mnem (csfile);
            if (sourcereg == z80r_unknown) {
                /* LD  r,n */
                if (destreg & 8) {
                    /* LD IXl,n or LD IXh,n */
                    StreamCodeByte (csfile, 0xDD);
                } else if (destreg & 16) {
                    /* LD  IYl,n or LD  IYh,n */
                    StreamCodeByte (csfile, 0xFD);
                }
                destreg &= 7;
                StreamCodeByte (csfile, (unsigned char) destreg * 8 + 6);
                ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 1);
                return;
            }

            if (sourcereg == z80r_f) {
                /* LD x, F */
                ReportError (csfile, Err_IllegalIdent);
                return;
            }

            if ((sourcereg == z80r_i) && (destreg == z80r_a)) {
                /* LD A,I */
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, 87);
                return;
            }

            if ((sourcereg == z80r_r) && (destreg == z80r_a)) {
                /* LD A,R */
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, 95);
                return;
            }

            if ((destreg & 8) || (sourcereg & 8)) {
                /* IXl or IXh */
                StreamCodeByte (csfile, 0xDD);
            } else if ((destreg & 16) || (sourcereg & 16)) {
                /* IYl or IYh */
                StreamCodeByte (csfile, 0xFD);
            }
            sourcereg &= 7;
            destreg &= 7;

            /* LD  r,r */
            StreamCodeByte (csfile, (unsigned char) (64 + destreg * 8 + sourcereg));
    }
}


void
LDI (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 160);
}


void
LDIR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 176);
}


void
LDD (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 168);
}


void
LDDR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 184);
}


void
CPI (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 161);
}


void
CPIR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 177);
}


void
CPD (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 169);
}


void
CPDR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 185);
}


void
IN (sourcefile_t *csfile)
{
    enum z80r inreg;

    if (GetSym(csfile) != name) {
        ReportError (csfile, Err_Syntax);
    }

    switch (inreg = CheckReg8Mnem (csfile)) {
        case z80r_i:
        case z80r_r:
        case z80r_unknown:
            ReportError (csfile, Err_IllegalIdent);
            return;

        default:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }
            if (GetSym(csfile) != lparen) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            GetSym(csfile);
            switch (CheckReg8Mnem (csfile)) {
                case z80r_c:
                    /* IN r,(C) */
                    GetSym(csfile); /* silently fetch ')' */
                    StreamCodeByte (csfile, 0xED);
                    StreamCodeByte (csfile, 64 + (unsigned char) (inreg * 8));
                    break;

                case z80r_unknown:
                    if (inreg != z80r_a) {
                        ReportError (csfile, Err_IllegalIdent);
                        return;
                    }

                    StreamCodeByte (csfile, 219);
                    if (ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 1) && csfile->sym != rparen)
                        ReportError (csfile, Err_Syntax);
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
                    break;
            }
    }
}


void
OUT_C_r8 (sourcefile_t *csfile, enum z80r reg8)
{
    switch (reg8) {
        case z80r_f:
        case z80r_i:
        case z80r_r:
        case z80r_unknown:
            ReportError (csfile, Err_IllegalIdent);
            break;

        default:
            /* OUT (C),r */
            StreamCodeByte (csfile, 0xED);
            StreamCodeByte (csfile, 65 + (unsigned char) (reg8 * 8));
            break;
    }
}


void
OUT_N_A (sourcefile_t *csfile)
{
    /* OUT (n),A */
    StreamCodeByte (csfile, 211);
    if (!ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 1)) {
        return;
    }

    if (csfile->sym != rparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }
    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    GetSym(csfile);
    if (CheckReg8Mnem (csfile) != z80r_a) {
        ReportError (csfile, Err_IllegalIdent);
    }
}


void
OUT (sourcefile_t *csfile)
{
    if (GetSym(csfile) != lparen) {
        ReportError (csfile, Err_Syntax);
    }

    GetSym(csfile);
    if (CheckReg8Mnem (csfile) == z80r_c) {
        /* OUT (C) */
        if (GetSym(csfile) != rparen) {
            ReportError (csfile, Err_Syntax);
            return;
        }
        if (GetSym(csfile) != comma) {
            ReportError (csfile, Err_Syntax);
            return;
        }

        GetSym(csfile);
        OUT_C_r8(csfile, CheckReg8Mnem (csfile));
    } else {
        OUT_N_A(csfile);
    }
}


void
IND (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 170);
}


void
INDR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 186);
}


void
INI (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 162);
}


void
INIR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 178);
}


void
OUTI (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 163);
}


void
OUTD (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 171);
}


void
OTIR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 179);
}


void
OTDR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 187);
}


void
CP (sourcefile_t *csfile)
{
    ArithLog8bitZ80 (csfile, 7);
}


void
AND (sourcefile_t *csfile)
{
    ArithLog8bitZ80 (csfile, 4);
}


void
OR (sourcefile_t *csfile)
{
    ArithLog8bitZ80 (csfile, 6);
}


void
XOR (sourcefile_t *csfile)
{
    ArithLog8bitZ80 (csfile, 5);
}


void
SUB (sourcefile_t *csfile)
{
    const unsigned char *ptr = MemfTellPtr(csfile->mf);

    if (GetSym(csfile) == name && CheckReg16Mnem (csfile) == z80r_hl) {
        /* SUB[W] HL,BC|DE|HL|SP meta instruction */
        MemfSeekPtr(csfile->mf, ptr);
        SUBW(csfile);
        return;
    }

    MemfSeekPtr(csfile->mf, ptr);
    ArithLog8bitZ80 (csfile, 2);
}


void
SET (sourcefile_t *csfile)
{
    BitSetResZ80 (csfile, 192);
}


void
RES (sourcefile_t *csfile)
{
    BitSetResZ80 (csfile, 128);
}


void
BIT (sourcefile_t *csfile)
{
    BitSetResZ80 (csfile, 64);
}


/*!
 * \brief RLW r16 | (HL) meta instruction
 * \param csfile
 */
void RLW (sourcefile_t *csfile)
{
    if (FetchHLIndirect(csfile) == z80r_hl) {
        /* RLW (HL) - only syntax allowed for Z80 meta instruction */
        /* rl (hl) : inc hl : rl (hl) : dec hl */
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x16);
        StreamCodeByte (csfile, 0x23);
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x16);
        StreamCodeByte (csfile, 0x2B);
    } else {
        RLWr16(csfile, CheckReg16Mnem (csfile));
    }
}


/*!
 * \brief RRW r16 | (HL) meta instruction
 * \param csfile
 */
void RRW (sourcefile_t *csfile)
{
    if (FetchHLIndirect(csfile) == z80r_hl) {
        /* RRW (HL) - only syntax allowed for Z80 meta instruction */
        /* inc hl : rr (hl) : dec hl : rr (hl) */
        StreamCodeByte (csfile, 0x23);
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x1E);
        StreamCodeByte (csfile, 0x2B);
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x1E);
    } else {
        RRWr16(csfile, CheckReg16Mnem (csfile));
    }
}


/*!
 * \brief SLAW r16 | (HL) meta instruction
 * \param csfile
 */
void SLAW (sourcefile_t *csfile)
{
    if (FetchHLIndirect(csfile) == z80r_hl) {
        /* SLAW (HL) - only syntax allowed for Z80 meta instruction */
        /* sla (hl) : inc hl : rl (hl) : dec hl */
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x26);
        StreamCodeByte (csfile, 0x23);
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x16);
        StreamCodeByte (csfile, 0x2B);
    } else {
        SLAWr16(csfile, CheckReg16Mnem (csfile));
    }
}


/*!
 * \brief SRAW r16 | (HL) meta instruction
 * \param csfile
 */
void SRAW (sourcefile_t *csfile)
{
    if (FetchHLIndirect(csfile) == z80r_hl) {
        /* SRAW (HL) - only syntax allowed for Z80 meta instruction */
        /* inc hl : sra (hl) : dec hl : rr (hl) */
        StreamCodeByte (csfile, 0x23);
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x2E);
        StreamCodeByte (csfile, 0x2B);
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x1E);
    } else {
        SRAWr16(csfile, CheckReg16Mnem (csfile));
    }
}


/*!
 * \brief SRLW r16 | (HL) meta instruction
 * \param csfile
 */
void SRLW (sourcefile_t *csfile)
{
    if (FetchHLIndirect(csfile) == z80r_hl) {
        /* SRLW (HL) - only syntax allowed for Z80 meta instruction */
        /* inc hl : srl (hl) : dec hl : rr (hl) */
        StreamCodeByte (csfile, 0x23);
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x3E);
        StreamCodeByte (csfile, 0x2B);
        StreamCodeByte (csfile, 0xCB);
        StreamCodeByte (csfile, 0x1E);
    } else {
        SRLWr16(csfile, CheckReg16Mnem (csfile));
    }
}


/*!
 * \brief SUBW HL,BC|DE|HL|SP meta instruction
 * \param csfile
 * \param r16
 */
void
SUBWr16(sourcefile_t *csfile, enum z80rr r16)
{
    switch(r16) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
        case z80r_sp:
            /* or a: sbc hl,bc|de|hl|sp */
            StreamCodeByte (csfile, 0xB7);
            StreamCodeByte (csfile, 0xed);
            StreamCodeByte (csfile, 0x42 + (unsigned char) r16 * 16);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief SUBW HL,BC|DE|HL|SP meta instruction
 * \param csfile
 */
void SUBW (sourcefile_t *csfile)
{
    GetSym(csfile);
    if (CheckReg16Mnem (csfile) != z80r_hl) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    GetSym(csfile);
    SUBWr16(csfile, CheckReg16Mnem (csfile));
}


void
RLC (sourcefile_t *csfile)
{
    RotShiftZ80 (csfile, 0);
}


void
RRC (sourcefile_t *csfile)
{
    RotShiftZ80 (csfile, 1);
}


void
RL (sourcefile_t *csfile)
{
    RotShiftZ80 (csfile, 2);
}


void
RR (sourcefile_t *csfile)
{
    RotShiftZ80 (csfile, 3);
}


void
SLA (sourcefile_t *csfile)
{
    RotShiftZ80 (csfile, 4);
}


void
SRA (sourcefile_t *csfile)
{
    RotShiftZ80 (csfile, 5);
}


void
SLL (sourcefile_t *csfile)
{
    RotShiftZ80 (csfile, 6);
}


void
SRL (sourcefile_t *csfile)
{
    RotShiftZ80 (csfile, 7);
}


void
CPL (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 47);
}


void
RLA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 23);
}


void
RRA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 31);
}


void
RRCA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 15);
}


void
RLCA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 7);
}


void
EXX (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 217);
}


void
PUSH (sourcefile_t *csfile)
{
    PUSHPOP (csfile, 197);
}


void
POP (sourcefile_t *csfile)
{
    PUSHPOP (csfile, 193);
}


void
RETI (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 77);
}


void
RETN (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 69);
}


void
RLD (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 111);
}


void
RRD (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 103);
}


void
NEG (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 68);
}


/*!
 * \brief IM 0 | 1 | 2
 * \param csfile
 * \param imType
 */
void
ImZ80(sourcefile_t *csfile, int imType)
{
    switch(imType) {
        case 0:
            StreamCodeByte (csfile, 0x46);
            break;
        case 1:
            StreamCodeByte (csfile, 0x56);
            break;
        case 2:
            StreamCodeByte (csfile, 0x5E);
            break;
        default:
            ReportError (csfile, Err_IntegerRange);
    }
}


void
IM (sourcefile_t *csfile)
{
    int imType;

    GetSym(csfile);
    if ((imType = (int) ParseMnemConstant(csfile)) == -1)
        return;

    StreamCodeByte (csfile, 0xED);
    ImZ80(csfile, imType);
}


void
RST (sourcefile_t *csfile)
{
    int rstVector;

    GetSym(csfile);
    rstVector = (int) ParseMnemConstant(csfile);
    if ((rstVector >= 0 && rstVector <= 56) && (rstVector % 8 == 0)) {
        /* RST 00H ... 38H */
        StreamCodeByte (csfile, (unsigned char) (199 + rstVector));
    } else {
        ReportError (csfile, Err_IntegerRange);
    }
}


void
CALL (sourcefile_t *csfile)
{
    GetSym(csfile);
    CallJpZ80 (csfile, 0xCD, 0xC4);
    ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, 1);
}


void
RET (sourcefile_t *csfile)
{
    enum z80cc cc;

    switch (GetSym(csfile)) {
        case name:
            if ((cc = CheckCondMnem (csfile)) != z80f_unknown) {
                /* RET <cc> instruction opcode */
                StreamCodeByte (csfile, 192 + (unsigned char) (cc * 8));
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case newline:
            StreamCodeByte (csfile, 201);
            break;

        default:
            ReportError (csfile, Err_Syntax);
    }
}


void
JR (sourcefile_t *csfile)
{
    GetSym(csfile);
    JRccOpcode(csfile);
    ProcessPcRelativeExpr (csfile, RANGE_PCREL8, 1);
}


void
DJNZ (sourcefile_t *csfile)
{
    GetSym(csfile);
    DJNZopcode(csfile);
    ProcessPcRelativeExpr (csfile, RANGE_PCREL8, 1);
}


/*!
 * \brief JP (HL), JP (IX) or JP (IY)
 * \param csfile
 */
void
JP_IndReg16(sourcefile_t *csfile)
{
    enum z80rr qq;

    switch ( (qq = CheckReg16Mnem (csfile)) ) {
        case z80r_hl:
            /* JP (HL) */
            StreamCodeByte (csfile, 233);
            break;

        case z80r_ix:
        case z80r_iy:
            /* JP (IX|IY) */
            StreamCodeByte (csfile, (qq == z80r_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, 233);
            break;

        case z80rr_unknown:
            ReportError (csfile, Err_Syntax);
            return;

        default:
            ReportError (csfile, Err_IllegalIdent);
            return;
    }

    /* fetch ')' */
    GetSym(csfile);
}


void
JP (sourcefile_t *csfile)
{
    if (GetSym(csfile) == lparen) {
        /* (HL), (IX) or (IY) */
        GetSym(csfile);
        JP_IndReg16(csfile);
    } else {
        /* no indirect register were found, point at expr. after 'JP' */
        CallJpZ80 (csfile, 195, 194);  /* base opcode for <instr> nn; <instr> cc, nn */
        ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, 1);
    }
}


void
CCF (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 63);
}


void
SCF (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 55);
}


void
DI (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 243);
}


void
EI (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 251);
}


void
DAA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 39);
}
