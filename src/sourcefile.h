/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#if ! defined MPM_SRCFILE_HEADER_
#define MPM_SRCFILE_HEADER_

#include <sys/stat.h>
#include "datastructs.h"    /* core data structures use in Mpm */

/* global variables */
extern avltree_t *cachedincludefiles;

/* global functions */
FILE *OpenFile(char *filename, const pathlist_t *pathlist, bool expandfilename);
char *AdjustPlatformFilename(char * const filename);
char *AdjustPlatformFilename(char * const filename);
char *AddFileExtension(const char *oldfilename, const char *extension);
char *Truncate2BaseFilename(char *filename);
int StatFile(const char *filename, const pathlist_t *pathlist, struct stat * const fstat);
includefile_t *CacheIncludeFile (sourcefile_t *csfile, char *fname);
sourcefile_t *FindFile (sourcefile_t *srcfile, char *fname);
sourcefile_t *Newfile (module_t *curmodule, sourcefile_t *curfile, const char *fname);
sourcefile_t *Prevfile (sourcefile_t *csfile);
sourcefile_t *CreateFileFromString (const char *name, int lineno, const char *string);
includefile_t *AllocIncludeFile (void);
void FreeCachedIncludeFile(const includefile_t *inclfile);
long FetchModuleFilename(const char *projectfilename, long projfptr, char *filename, filelist_t **dependencies);
void AddPathNode (char * const path, pathlist_t **plist);
void AddFileNameNode (const char *filename, filelist_t **flist);
void ReleaseFile (sourcefile_t *srcfile);
void ReleaseSourceMemFile (sourcefile_t *srcfile);
void ReleaseSourceMemFileCache(sourcefile_t *srcfile);
void ReleaseCachedIncludeFiles(void);

#endif
