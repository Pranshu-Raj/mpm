/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/

#if !defined MPM_DATASTRUCTS_HEADER_
#define MPM_DATASTRUCTS_HEADER_

#include <stdbool.h>
#include <stdio.h>

/* Structured data types : */
typedef long symvalue_t;                  /* symbol value is a 32bit integer */

enum cpu            { z80, zxn, z180, z380 };

enum symbols        {   /* enumerations mapped to prsline.separators[] */
                        space = 0,          /* ' ' */
                        bin_and = 1,        /* & */
                        dquote = 2,         /* " */
                        squote = 3,         /* ' */
                        semicolon = 4,      /* ; */
                        comma = 5,          /* , */
                        fullstop = 6,       /* . */
                        strconq = fullstop, /* . */
                        lparen = 7,         /* ( */
                        lcurly = 8,         /* { */
                        lexpr = 9,          /* [ */
                        backslash = 10,     /* \ */
                        rexpr = 11,         /* ] */
                        rcurly = 12,        /* } */
                        rparen = 13,        /* ) */
                        plus = 14,          /* + */
                        minus = 15,         /* - */
                        multiply = 16,      /* * */
                        divi = 17,          /* / */
                        mod = 18,           /* % */
                        bin_xor = 19,       /* ^ */
                        assign = 20,        /* = */
                        bin_or = 21,        /* | */
                        bin_nor = 22,       /* : */
                        colon = bin_nor,    /* : */
                        bin_not = 23,       /* ~ */
                        less = 24,          /* < */
                        greater = 25,       /* > */
                        log_not = 26,       /* ! */
                        cnstexpr = 27,      /* ` */

                        /* rest of enumerations represents other syntactic symbols */
                        newline = 32,       /* \n */
                        power = 33,         /* ** */
                        lshift = 34,        /* >> */
                        rshift = 35,        /* << */
                        lessequal = 36,     /* <= */
                        greatequal = 37,    /* >= */
                        notequal = 38,      /* <> */

                        name = 39, number = 40, decmconst = 41, hexconst = 42, binconst = 43, charconst = 44,
                        registerid = 45, negated = 46, mod256 = 47, div256 = 48, nil = 49, ifstatm = 50,
                        elsestatm = 51, endifstatm = 52, enddefstatm = 53, colonlabel = 54, asmfnname = 55
                    };

typedef struct avlnode {
    short           height;        /* height of avltree (max search levels from node) */
    void           *data;          /* pointer to data of node */
    struct avlnode *left, *right;  /* pointers to left and right avl subtrees */
} avltree_t;

typedef
struct pfixstack    {
    long                stackconstant;    /* stack structure used to evaluate postfix expressions */
    struct pfixstack   *prevstackitem;    /* pointer to previous element on stack */
} pfixstack_t;

typedef
struct postfixexpr  {
    struct postfixexpr *nextoperand;      /* pointer to next element in postfix expression */
    long               operandconst;
    enum symbols       operatortype;
    char               *id;               /* pointer to identifier */
    unsigned long      type;              /* type of identifier (local, global, rel. address or constant) */
} postfixexpr_t;

struct sourcefile;

typedef
struct expression   {
    struct expression  *nextexpr;         /* pointer to next expression */
    postfixexpr_t      *firstnode;
    postfixexpr_t      *currentnode;
    size_t             rangetype;         /* range type of evaluated expression */
    bool               linkexpr;          /* Flag to indicate that expression needs to be evaluated during linking */
    bool               wrobjfile;         /* Flag to indicate that expression has been written to object file */
    char               *infixexpr;        /* pointer to a copy of compact, parsed ASCII infix expression */
    char               *infixptr;         /* pointer to current char in infix expression */
    size_t             codepos;           /* rel. position in module code to patch (in pass 2) */
    struct sourcefile  *srcfile;          /* expr. in file 'srcfile' (also macro) */
    unsigned char      *exprlineptr;      /* pointer to expression in source file */
    int                curline;           /* expression in line of source file */
    long               listpos;           /* position in listing file to patch (in pass 2) */
} expression_t;

typedef
struct exprlist     {
    expression_t        *firstexpr;       /* header of list of expressions in current module */
    expression_t        *currexpr;        /* point at last expression in list */
} expressions_t;


struct macronode;

typedef enum {
                       callparameter, localvariable, constant
} macrovartype_t;

typedef
struct macrovariable {
    macrovartype_t     type;              /* a \name in a macro body can be a call-parameter, local variable, label or constant */
    struct macronode   *macroref;         /* reference to macro definition */
    char               *name;             /* pointer to macro variable name */
    char               *value;            /* pointer to macro variable value */
    char               *defaultvalue;     /* pointer to macro variable default value */
    int                pvLen;             /* length of variable value string */
    int                dfPvLen;           /* length of default variable value string */
} macrovariable_t;

struct macrofunction;

typedef
struct macrotext    {
    char               *text;             /* pointer to text (a part of a source code line) without a macro parameter */
    int                textLen;           /* length of text string */
    macrovariable_t    *macrovariable;    /* pointer to macro variable that was embedded in original source code line */
    struct macrofunction *macrofunction;  /* pointer to macro function that was embedded in original source code line */
    struct macrotext   *nextmacrotext;    /* pointer to next link of a source code line with macro parameters embedded */
} macrotext_t;

typedef char           *(*macroFnPtr)(void *); /* pointer to macro function, returning a constant string */

typedef
struct macrofuncexpr {
    struct macronode   *macroref;         /* reference to macro definition */
    macrotext_t        *expression;       /* the optional expression - which is a linked list of text components and possible place holders */
} macrofuncexpr_t;

typedef
struct macrofunction {
    macroFnPtr         fnPtr;             /* the executing function of this macro object */
    macrofuncexpr_t    *functionargs;     /* the macro function arguments - which is the macro def + linked list of text components and possible place holders */
    char               *result;           /* the result of the output (a macro function is only executed once) */
} macrofunction_t;

typedef
struct macrofnlookup {
    char *macrofnid;                       /* macro function name definition & implementation element for lookup table */
    macroFnPtr fnPtr;
} macrofnlookup_t;

typedef
struct macrobody    {
    macrotext_t        *textline;         /* pointer to linked list of source code line and embedded macro parameters */
    struct macrobody   *nextmacroline;    /* pointer to next line of macro body text */
} macrobody_t;

typedef
struct macronode    {
    char               *macroname;        /* pointer to macro name */
    int                instanceCounter;   /* the counter increases for every time the macro is executed in source code, reflected in \@ */
    int                totalParameters;   /* total of macro parameters, or 0 if none are specified */
    int                lineNo;            /* current line number of parsed / expanded macro body (used for error reporting) */
    macrovariable_t   **parameters;       /* pointer to array of macro parameter names & values, or NULL if none are specified */
    avltree_t          *variableNvps;     /* fast lookup of macro parameter name/value pairs */
    macrobody_t        *macrotext;        /* linked list of lines comprising the macro text body with embedded parameters */
    macrobody_t        *currentline;      /* points to current line of macro body (typically final line) */
} macro_t;

typedef
struct usedfile     {
    struct usedfile    *nextusedfile;
    struct sourcefile  *ownedsourcefile;
} usedsrcfile_t;

typedef
struct filelist     {
    struct filelist   *nextfile;          /* pointer to next file in list */
    char              *filename;          /* name of file */
} filelist_t;

typedef
struct includefile   {
    char               *fname;            /* pointer to file name of source file */
    long               filesize;          /* size of file in bytes */
    unsigned char      *filedata;         /* pointer to complete copy of file content */
} includefile_t;

typedef
struct memfile   {
    char               *fname;            /* pointer to name of memory file */
    long               filespace;         /* current size of memory file space (accumulated contents and buffer) in bytes */
    long               filesize;          /* current size of memory file in bytes */
    long               blocksize;         /* size of block to increase file space when exhausted during output */
    unsigned char      *filedata;         /* pointer to complete copy of memory file content */
    long               memfileptr;        /* index to current character in memory file */
    bool               eof;               /* indicate if End Of Memory File has been reached */
} memfile_t;


typedef
struct sct  {
    unsigned char     *area;              /* pointer to beginning of allocated section area */
    char              *name;              /* name of section */
    int                sizelimit;         /* max size of (allocated) section, updated, if section size is increased */
    int                pc;                /* index offset (program counter) of current location to write into section, 0 - (maxlimit-1) */
} section_t;

typedef
struct module       {
    struct module      *nextmodule;       /* pointer to next module */
    char               *mname;            /* pointer to string of module name */
    size_t             startoffset;       /* this module's start offset from start of linked code buffer */
    size_t             origin;            /* address origin of current machine code module during linking */
    struct sourcefile  *cfile;            /* pointer to current file record */
    memfile_t          *errfile;          /* error output via memory file */
    memfile_t          *lstfile;          /* listing output via memory file */
    section_t          *csection;         /* current, active code section */
    avltree_t          *notdeclsymbols;   /* pointer to root of symbols tree not yet declared/defined */
    avltree_t          *localsymbols;     /* pointer to root of local symbols tree */
    avltree_t          *globalsymbols;    /* pointer to root of copied global symbols tree (after pass 2) */
    avltree_t          *libsymbols;       /* pointer to root of library reference symbols tree */
    avltree_t          *macros;           /* pointer to root of macro definitions tree */
    expressions_t      *expressions;      /* pointer to expressions in this module */
    bool               inobjfile;         /* local/global/lib symbols, expressions & code are to be read from object file */
} module_t;

typedef
struct modules      {
    module_t           *first;            /* pointer to first module */
    module_t           *last;             /* pointer to current/last module */
} modules_t;

typedef
struct node         {
    unsigned long      type;              /* type of symbol */
    char               *symname;          /* pointer to symbol identifier */
    symvalue_t         symvalue;          /* value of symbol (size dependents on type) */
    module_t           *owner;            /* pointer to module which owns symbol */
} symbol_t;

typedef
struct sourcefile   {
    struct module      *module;           /* this source file is part of specified module */
    struct sourcefile  *prevsrcfile;      /* pointer to previously parsed source file */
    struct sourcefile  *newsrcfile;       /* pointer to new source file to be parsed */
    usedsrcfile_t      *usedsrcfile;      /* list of pointers to used files owned by this file */
    filelist_t         *dependencies;     /* optional file dependencies used with -d option) */
    memfile_t          *mf;               /* pointer to cached source file and filename in memory for fast parsing */
    unsigned char      *lineptr;          /* pointer to beginning of current line being parsed */
    enum symbols       sym;               /* current symbol mnemonic of parsed source code */
    symbol_t           *labelname;        /* References the parsed label or name definition for EQU */
    char               *ident;            /* current symbol string identifier of parsed source code */
    int                identlen;          /* length in bytes of current symbol string identifier */
    int                lineno;            /* current line number of current source file */
    int                asmerror;          /* latest reported error */
    bool               eol;               /* indicate if End Of Line has been reached */
    bool               cstylecomment;     /* c-style (multi-)line comment enabled/disabled */
    bool               includedfile;      /* if this is an INCLUDE'd file or not */
    bool               parsingmacro;      /* currently parsing macro body (ON) */
    macro_t            *macro;            /* this is an executing macro, otherwise NULL */
} sourcefile_t;


typedef int (*compfunc_t) (const void *, const void *); /* generic compare function pointer */
typedef void (*ptrfunc) (void);           /* ptr to function returning void */
typedef void (*mnemprsrfunc) (sourcefile_t *csfile);  /* ptr to mnemonic parser function returning void */
typedef mnemprsrfunc (*cpumnemfunc) (enum symbols st, const char *);
typedef void (*lineparserfunc) (sourcefile_t *csfile, const bool);

typedef
struct {
    lineparserfunc lpf;                   /* function pointer to CPU-specific syntax line parser */
    cpumnemfunc cmf;                      /* function pointer to CPU-specific mnemonic lookup */
    enum cpu identifier;                  /* the current assembler cpu identifier */
} cpuassembler_t;

typedef
struct asmsym       {
    char *mnem;                           /* identifier definition & function implementation */
    mnemprsrfunc mnemfn;                  /* mnemonic parser (of current file) function */
} identfunc_t;

/*
 * These typedef's identify when a mnemonic function is called as
 *      sourcefile_t *csf
 * or
 *      const sourcefile_t *csf
*/
typedef
union {
    void (*mpf) (sourcefile_t *csf);         /* either mnemonic function is called when source file structure is modified */
    void (*cmpf) (const sourcefile_t *csf);  /* or when mnemonic function is called and source file structure is only referenced */
} mnemprsrfunc_t;

/* the mnemonic lookup function table then individually defines if source file is going to be modified, or not */
typedef
struct {
    char *mnem;                              /* identifier definition & function implementation */
    mnemprsrfunc_t mnemfn;                   /* mnemonic parser (of current file) function */
} idf_t;

typedef symbol_t * (*symfunc) (const sourcefile_t *csfile,const void *);   /* ptr to symbol function returning pointer to symbol node */

typedef
struct asmsymfunc   {
    char *asm_mnem;                       /* assembler inline function definition & implementation */
    symfunc asm_func;
} exprfunc_t;

typedef
struct labels       {
    struct labels      *prevlabel;        /* pointer to previous label occurence on stack */
    symbol_t           *labelsym;         /* pointer to label in symbol table */
} labels_t;

typedef
struct pathlist     {
    struct pathlist   *nextdir;           /* pointer to next directory path in list */
    char              *directory;         /* name of directory */
} pathlist_t;

typedef
struct libfile      {
    struct libfile    *nextlib;           /* pointer to next library file in list */
    char              *libfilename;       /* filename of library (incl. extension) */
    const char        *libwatermark;      /* pointer to watermark identifier identified at start of library file */
} libfile_t;

typedef
struct liblist      {
    libfile_t         *firstlib;          /* pointer to first library file specified from command line */
    libfile_t         *currlib;           /* pointer to current library file specified from command line */
} libraries_t;

typedef
struct libobject    {
    char              *libname;           /* name of library module (the LIB reference name) */
    libfile_t         *library;           /* pointer to library file information */
    long              modulestart;        /* base pointer of beginning of object module inside library file */
} libobject_t;

typedef
struct linkedmod    {
    struct linkedmod  *nextlink;          /* pointer to next module link */
    char              *objfilename;       /* filename of library/object file (incl. extension) */
    long              modulestart;        /* base pointer of beginning of object module */
    module_t          *moduleinfo;        /* pointer to main module information */
} tracedmodule_t;

typedef
struct linkmodlist  {
    tracedmodule_t    *firstlink;         /* pointer to first linked object module */
    tracedmodule_t    *lastlink;          /* pointer to last linked module in list */
} tracedmodules_t;


/* Bitmasks for symtype */
#define SYMDEFINED      0x01000000                          /* bitmask 00000001 00000000 00000000 00000000 */
#define SYMTOUCHED      0x02000000                          /* bitmask 00000010 00000000 00000000 00000000 */
#define SYMDEF          0x04000000                          /* bitmask 00000100 00000000 00000000 00000000 */
#define SYMADDR         0x08000000                          /* bitmask 00001000 00000000 00000000 00000000 */
#define SYMLOCAL        0x10000000                          /* bitmask 00010000 00000000 00000000 00000000 */
#define SYMXDEF         0x20000000                          /* bitmask 00100000 00000000 00000000 00000000 */
#define SYMXREF         0x40000000                          /* bitmask 01000000 00000000 00000000 00000000 */
#define SYMFUNC         0x80000000                          /* bitmask 10000000 00000000 00000000 00000000 */


/* #define SYM_BASE32      0x00000100                        symbol value is defined as 32bit native integer (long) */
/* #define SYM_BASE64      0x00000200                        symbol value is defined as 64bit integer (dlong_t) */
/* #define SYM_BASE128     0x00000400                        symbol value is defined as 128bit integer (qlong_t) */

#define SYMLOCAL_OFF    0xEFFFFFFF                          /* bitmask 11101111 11111111 11111111 11111111 */
#define XDEF_OFF        0xDFFFFFFF                          /* bitmask 11011111 11111111 11111111 11111111 */
#define XREF_OFF        0xBFFFFFFF                          /* bitmask 10111111 11111111 11111111 11111111 */
#define SYMTYPE         0x78000000                          /* bitmask 01111000 00000000 00000000 00000000 */
#define SYM_NOTDEFINED  0x00000000

/* bitmasks for expression evaluation in rangetype */
#define RANGE           0x000000FF                          /* bitmask 00000000 00000000 00000000 11111111   Range types are 0 - 255 */
#define NOTEVALUABLE    0x80000000                          /* bitmask 10000000 00000000 00000000 00000000   Expression is not evaluable */
#define EVALUATED       0x7FFFFFFF                          /* bitmask 01111111 11111111 11111111 11111111   Expression is not evaluable */
#define CLEAR_EXPRADDR  0xF7FFFFFF                          /* bitmask 11110111 11111111 11111111 11111111   Convert to constant expression */

/* Mpm Object file expression types */
#define RANGE_8UNSIGN       'U'                             /* Mpm V1.x */
#define RANGE_8SIGN         'S'                             /* Mpm V1.x */
#define RANGE_PCREL8        'J'                             /* Mpm V2.x */
#define RANGE_LSB_16CONST   'C'                             /* Mpm V1.x, LSB = Least Significant Byte format */
#define RANGE_MSB_16CONST   'c'                             /* Mpm V2.x, MSB = Most Significant Byte format  */
#define RANGE_LSB_16SIGN    'W'                             /* Mpm V2.x */
#define RANGE_MSB_16SIGN    'w'                             /* Mpm V2.x */
#define RANGE_LSB_PCREL16   'P'                             /* Mpm V2.x */
#define RANGE_MSB_PCREL16   'p'                             /* Mpm V2.x */
#define RANGE_LSB_PCREL24   'Q'                             /* Mpm V2.x */
#define RANGE_MSB_PCREL24   'q'                             /* Mpm V2.x */
#define RANGE_LSB_24SIGN    'I'                             /* Mpm V2.x */
#define RANGE_MSB_24SIGN    'i'                             /* Mpm V2.x */
#define RANGE_LSB_32SIGN    'L'                             /* Mpm V1.x */
#define RANGE_MSB_32SIGN    'l'                             /* Mpm V2.x */
#define RANGE_LSB_32CONST   'u'                             /* Mpm V2.x */
#define RANGE_MSB_32CONST   'v'                             /* Mpm V2.x */
#endif
