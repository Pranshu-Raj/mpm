/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 *                         ZZZZZZZZZZZZZZ    888888888888        000000000
 *                       ZZZZZZZZZZZZZZ    8888888888888888    0000000000000
 *                               ZZZZ      8888        8888  0000         0000
 *                             ZZZZ          888888888888    0000         0000
 *                           ZZZZ          8888        8888  0000         0000
 *                         ZZZZZZZZZZZZZZ  8888888888888888    0000000000000
 *                       ZZZZZZZZZZZZZZ      888888888888        000000000
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/

#if ! defined MPM_Z80_HEADER_
#define MPM_Z80_HEADER_

/* globally available functions */
#include "datastructs.h"

/* Z80 8bit register enumerations */
enum z80r  { z80r_b = 0,
             z80r_c = 1,
             z80r_d = 2,
             z80r_e = 3,
             z80r_h = 4,
             z80r_l = 5,
             z80r_f = 6,
             z80r_a = 7,
             z80r_i = 8,
             z80r_r = 9,
             z80r_ixh = 8+4,
             z80r_ixl = 8+5,
             z80r_iyh = 16+4,
             z80r_iyl = 16+5,
             z80r_unknown = 127
           };

/* Z80 16bit register enumerations */
enum z80rr { z80r_bc = 0, z80r_de = 1, z80r_hl = 2, z80r_hl_z380 = 3, z80r_sp = 3, z80r_af = 4, z80r_ix = 5, z80r_iy = 6, z80rr_unknown = 127};

/* Z80 indirect address enumerations */
enum z80ia { z80ia_bc = z80r_bc,
             z80ia_de = z80r_de,
             z80ia_hl = z80r_hl,
             z80ia_hl_z380 = 3,
             z80ia_ix = z80r_ix,
             z80ia_iy = z80r_iy,
             z80ia_sp = 7,       /* Z380 only */
             z80ia_nn = 127,
             z80ia_unknown = 128
           };

/* Z80 conditional flag enumerations */
enum z80cc { z80f_nz, z80f_z, z80f_nc, z80f_c, z80f_po, z80f_pe, z80f_p, z80f_m, z80f_unknown};

typedef
struct relocoffset {
    unsigned short offset;
} relocoffset_t;

enum z80cc CheckCondMnem (const sourcefile_t *csfile);
mnemprsrfunc LookupZ80Mnemonic(enum symbols st, const char *id);
void ParseLineZ80 (sourcefile_t *csfile, const bool interpret);

unsigned char *InitRelocTable( void );
void RegisterRelocEntry (unsigned short progcnt );
void WriteRelocTable ( char *filename );
void WriteRelocRoutine( char *filename );
void FreeRelocTable ( void );

void LD (sourcefile_t *csfile);
void ADD (sourcefile_t *csfile);
void AddHL(sourcefile_t *csfile, int opcode);
void ADC (sourcefile_t *csfile);
void AdcSbcHL (sourcefile_t *csfile, int opcode);
void DEC (sourcefile_t *csfile);
void IM (sourcefile_t *csfile);
void IN (sourcefile_t *csfile);
void INC (sourcefile_t *csfile);
void JR (sourcefile_t *csfile);
void JRccOpcode(sourcefile_t *csfile);
void LD (sourcefile_t *csfile);
void OUT (sourcefile_t *csfile);
void OUT_C_r8 (sourcefile_t *csfile, enum z80r reg);
void OUT_N_A (sourcefile_t *csfile);
void RET (sourcefile_t *csfile);
void SBC (sourcefile_t *csfile);
void RST (sourcefile_t *csfile);
void AND (sourcefile_t *csfile);
void BIT (sourcefile_t *csfile);
void CALL (sourcefile_t *csfile);
void CP (sourcefile_t *csfile);
void DJNZ (sourcefile_t *csfile);
void DJNZopcode(sourcefile_t *csfile);
void EX (sourcefile_t *csfile);
void EX_SP(sourcefile_t *csfile);
void JP (sourcefile_t *csfile);
void OR (sourcefile_t *csfile);
void CCF (const sourcefile_t *csfile);
void CPD (const sourcefile_t *csfile);
void CPDR (const sourcefile_t *csfile);
void CPI (const sourcefile_t *csfile);
void CPIR (const sourcefile_t *csfile);
void CPL (const sourcefile_t *csfile);
void DAA (const sourcefile_t *csfile);
void DI (const sourcefile_t *csfile);
void EI (const sourcefile_t *csfile);
void EX_AFaf(const sourcefile_t *csfile);
void EXX (const sourcefile_t *csfile);
void HALT (const sourcefile_t *csfile);
void IND (const sourcefile_t *csfile);
void INDR (const sourcefile_t *csfile);
void INI (const sourcefile_t *csfile);
void INIR (const sourcefile_t *csfile);
void LDD (const sourcefile_t *csfile);
void LDDR (const sourcefile_t *csfile);
void LDI (const sourcefile_t *csfile);
void LDIR (const sourcefile_t *csfile);
void NEG (const sourcefile_t *csfile);
void NOP (const sourcefile_t *csfile);
void OTDR (const sourcefile_t *csfile);
void OTIR (const sourcefile_t *csfile);
void OUTD (const sourcefile_t *csfile);
void OUTI (const sourcefile_t *csfile);
void RETI (const sourcefile_t *csfile);
void RETN (const sourcefile_t *csfile);
void RLA (const sourcefile_t *csfile);
void RLCA (const sourcefile_t *csfile);
void RLD (const sourcefile_t *csfile);
void RRA (const sourcefile_t *csfile);
void RRCA (const sourcefile_t *csfile);
void RRD (const sourcefile_t *csfile);
void SCF (const sourcefile_t *csfile);
void POP (sourcefile_t *csfile);
void PUSH (sourcefile_t *csfile);
void RES (sourcefile_t *csfile);
void RL (sourcefile_t *csfile);
void RLC (sourcefile_t *csfile);
void RLW (sourcefile_t *csfile);
void RRW (sourcefile_t *csfile);
void SLAW (sourcefile_t *csfile);
void SRAW (sourcefile_t *csfile);
void SRLW (sourcefile_t *csfile);
void RR (sourcefile_t *csfile);
void RRC (sourcefile_t *csfile);
void SET (sourcefile_t *csfile);
void SLA (sourcefile_t *csfile);
void SLL (sourcefile_t *csfile);
void SRA (sourcefile_t *csfile);
void SRL (sourcefile_t *csfile);
void SUB (sourcefile_t *csfile);
void SUBW(sourcefile_t *csfile);
void SUBWr16(sourcefile_t *csfile, enum z80rr r16);
void XOR (sourcefile_t *csfile);
void LD_ira(sourcefile_t *csfile, unsigned char opcode);
void LD_HL8bit_indrct (sourcefile_t *csfile, enum z80r sourcereg);
void ImZ80(sourcefile_t *csfile, int imType);
void IncDec16bit(sourcefile_t *csfile, const enum z80rr reg16, int opcode);
void BitSrcRg8bit(sourcefile_t *csfile, int opcode, int bitnumber);
void JP_instr (sourcefile_t *csfile, int opc0, int opc);
void JP_IndReg16(sourcefile_t *csfile);
void CallJpZ80 (sourcefile_t *csfile, int opc0, int opcodecc);
void PushPop_instr (sourcefile_t *csfile, int opcode, enum z80rr qq);
void RotShift8bitReg (sourcefile_t *csfile, int opcode);
void ArithLogSrc8RegZ80(sourcefile_t *csfile, int opcode);
void AddIxIyReg16(sourcefile_t *csfile, enum z80rr acc16, int opcode);
void IncDec8bitRegZ80 (sourcefile_t *csfile, int opcode);
void ArithLog8bitZ80 (sourcefile_t *csfile, int opcode);
enum z80r CheckReg8Mnem(const sourcefile_t *csfile);
enum z80rr CheckReg16Mnem(const sourcefile_t *csfile);
enum z80rr GetLD16bitDestReg (sourcefile_t *csfile);
enum z80ia CheckIndirectAddrMnem(sourcefile_t *csfile);

#endif
