/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "config.h"
#include "datastructs.h"
#include "avltree.h"
#include "main.h"
#include "options.h"
#include "symtables.h"
#include "exprprsr.h"
#include "modules.h"
#include "section.h"
#include "errors.h"
#include "prsline.h"
#include "sourcefile.h"
#include "memfile.h"
#include "objfile.h"
#include "pass.h"


/******************************************************************************
                          Global variables
 ******************************************************************************/
labels_t *addresses = NULL;
pathlist_t *gIncludePath = NULL;
pathlist_t *gLibraryPath = NULL;

/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static labels_t *AllocAddressItem (void);
static void GetListingFilename(const module_t *module, char *strbuffer);
static bool IsListingFileOpen(const module_t *module);
static long GetListingFilePointer(const module_t *module);
static void SourceFilePass2 (sourcefile_t *csfile);
static void SetListingFilePointer(const module_t *module, const long fptr);
static void SetListingFilePtrToEnd(memfile_t *lstfile);
static void WriteListingFileHeader (module_t *module);
static void WriteSymbolTable (sourcefile_t *csfile, const char *msg, const avltree_t *root);
static void PatchByte2ListFile (const expression_t *pass2expr, const unsigned char c);
static void PatchWord2ListFile (const expression_t *pass2expr, const unsigned short c);
static void PatchWordMSB2ListFile (const expression_t *pass2expr, const unsigned short c);
static void PatchInt24ListFile (const expression_t *pass2expr, const size_t c);
static void PatchInt24MSBListFile (const expression_t *pass2expr, const size_t c);
static void PatchInt32ListFile (const expression_t *pass2expr, const size_t c);
static void PatchInt32MSBListFile (const expression_t *pass2expr, const size_t c);
static void PatchListFile (const expression_t *pass2expr, long c);
static void AddExpr2Module(expression_t *pfixexpr);
static void ReleasePathList(pathlist_t **plist);
static void WriteSymbol (const symbol_t *n);
static void DefineModuleName(module_t *module, const char *fname);

static const int PAGELEN = 66;
static int LINENO;
static int PAGENO;
static long listfileptr = 0;


/*!
 * \brief Append expression to the linked list of current module expressions
 * \details
 * Adjust the pointer to the first node and current (end of list) when necessary.
 * The linked list contains expressions that are being re-evaluated during
 * pass 2 or during linking stage
 * \param pfixexpr
 */
static
void AddExpr2Module(expression_t *pfixexpr)
{
    expressions_t *cmexpr;

    if (pfixexpr->srcfile != NULL && pfixexpr->srcfile->module != NULL) {
        cmexpr = pfixexpr->srcfile->module->expressions;

        if (cmexpr->firstexpr == NULL) {
            cmexpr->firstexpr = pfixexpr;
            cmexpr->currexpr = pfixexpr;              /* Expression header points at first expression */
        } else {
            cmexpr->currexpr->nextexpr = pfixexpr;    /* Current expression node points to new expression node */
            cmexpr->currexpr = pfixexpr;              /* Pointer to current expression node updated */
        }
    }
}


static void
DefineModuleName(module_t *module, const char *fname)
{
    const char *basefilename;
    char *tmpfilename = strclone(fname);

    if (tmpfilename != NULL) {
        basefilename = Truncate2BaseFilename(tmpfilename);
        if ((module->mname = strclone(basefilename)) == NULL) {
            /* not enough memory to assign base filename as module name! */
            ReportError (NULL, Err_Memory);
        }

        free(tmpfilename); /* temp. filename workspace used, release it.. */
    } else {
        /* not enough memory to assign base filename as module name! */
        ReportError (NULL, Err_Memory);
    }
}


static bool
IsListingFileOpen(const module_t *module)
{
    if (module != NULL && module->lstfile != NULL)
        return true;
    else
        return false;
}


static void
GetListingFilename(const module_t *module, char *strbuffer)
{
    if (module != NULL && module->lstfile != NULL && strbuffer != NULL) {
        strncpy (strbuffer, module->lstfile->fname, MAX_NAME_SIZE-1);
    }
}

static long
GetListingFilePointer(const module_t *module)
{
    if (module != NULL && module->lstfile != NULL) {
        return MemfTell (module->lstfile);
    } else {
        return 0;
    }
}


static void
SetListingFilePointer(const module_t *module, const long fptr)
{
    if (module != NULL && module->lstfile != NULL) {
        MemfSeek(module->lstfile, fptr);
    }
}


static void
SetListingFilePtrToEnd(memfile_t *lstfile)
{
    if (lstfile != NULL) {
        MemfSeekEnd(lstfile);
    }
}


/*!
 * \brief Patch byte in listing file, defined by expression.listpos
 * \param pass2expr
 * \param c
 */
static void
PatchByte2ListFile (const expression_t *pass2expr, const unsigned char c)
{
    char strbuf[8];

    /* set file pointer in list file */
    SetListingFilePointer(pass2expr->srcfile->module, pass2expr->listpos);

    snprintf(strbuf, 8, "%02X", c);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
}


/*!
 * \brief Patch 16bit word in Least Significant Byte Format into listing file, defined by expression.listpos
 * \param pass2expr
 * \param c
 */
static void
PatchWord2ListFile (const expression_t *pass2expr, const unsigned short c)
{
    char strbuf[8];

    /* set file pointer in list file */
    SetListingFilePointer(pass2expr->srcfile->module, pass2expr->listpos);

    snprintf(strbuf, 8, "%02X ", c & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X", c >> 8);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
}


/*!
 * \brief Patch 16bit word in Most Significant Byte Format into listing file, defined by expression.listpos
 * \param pass2expr
 * \param c
 */
static void
PatchWordMSB2ListFile (const expression_t *pass2expr, const unsigned short c)
{
    char strbuf[8];

    /* set file pointer in list file */
    SetListingFilePointer(pass2expr->srcfile->module, pass2expr->listpos);

    snprintf(strbuf, 8, "%02X ", c >> 8);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X", c & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
}


/*!
 * \brief Patch 24bit integer in Least Significant Byte Format into listing file, defined by expression.listpos
 * \param pass2expr
 * \param c
 */
static void
PatchInt24ListFile (const expression_t *pass2expr, const size_t c)
{
    char strbuf[8];

    /* set file pointer in list file */
    SetListingFilePointer(pass2expr->srcfile->module, pass2expr->listpos);

    snprintf(strbuf, 8, "%02X ", (unsigned int) c & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X ", (unsigned int) (c >> 8) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X", (unsigned int) (c >> 16) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
}


/*!
 * \brief Patch 24bit integer in Most Significant Byte Format into listing file, defined by expression.listpos
 * \param pass2expr
 * \param c
 */
static void
PatchInt24MSBListFile (const expression_t *pass2expr, const size_t c)
{
    char strbuf[8];

    /* set file pointer in list file */
    SetListingFilePointer(pass2expr->srcfile->module, pass2expr->listpos);

    snprintf(strbuf, 8, "%02X ", (unsigned int) (c >> 16) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X ", (unsigned int) (c >> 8) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X", (unsigned int) c & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
}


/*!
 * \brief Patch 32bit integer in Least Significant Byte Format into listing file, defined by expression.listpos
 * \param pass2expr
 * \param c
 */
static void
PatchInt32ListFile (const expression_t *pass2expr, const size_t c)
{
    char strbuf[8];

    /* set file pointer in list file */
    SetListingFilePointer(pass2expr->srcfile->module, pass2expr->listpos);

    snprintf(strbuf, 8, "%02X ", (unsigned int) c & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X ", (unsigned int) (c >> 8) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X ", (unsigned int) (c >> 16) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X", (unsigned int) (c >> 24) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
}


/*!
 * \brief Patch 32bit integer in Most Significant Byte Format into listing file, defined by expression.listpos
 * \param pass2expr
 * \param c
 */
static void
PatchInt32MSBListFile (const expression_t *pass2expr, const size_t c)
{
    char strbuf[8];

    /* set file pointer in list file */
    SetListingFilePointer(pass2expr->srcfile->module, pass2expr->listpos);

    snprintf(strbuf, 8, "%02X ", (unsigned int) (c >> 24) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X ", (unsigned int) (c >> 16) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X ", (unsigned int) (c >> 8) & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
    snprintf(strbuf, 8, "%02X", (unsigned int) c & 0xff);
    Stream2ListingFile(pass2expr->srcfile->module, strbuf);
}


static void
PatchListFile (const expression_t *pass2expr, long c)
{
    if (pass2expr == NULL)
        return;                                 /* expression not specified */
    if (IsListingFileOpen(pass2expr->srcfile->module) == false)
        return;
    if (pass2expr->listpos == -1)
        return;                                 /* listing wasn't enabled during pass 1 */

    switch (pass2expr->rangetype & RANGE) {
        case RANGE_8UNSIGN:
        case RANGE_8SIGN:
            PatchByte2ListFile(pass2expr, (unsigned char) c);
            break;

        case RANGE_PCREL8:
            c -= (gAsmpcPtr->symvalue+1); /* PC-relative offset */
            PatchByte2ListFile(pass2expr, (unsigned char) c);
            break;

        case RANGE_LSB_16CONST:
        case RANGE_LSB_16SIGN:
            PatchWord2ListFile(pass2expr, (unsigned short) c);
            break;

        case RANGE_LSB_PCREL16:
            /* 16bit signed PC relative jump is expr -= $PC, where $PC points at next instruction (2 bytes after code patch index) */
            c -= (gAsmpcPtr->symvalue+2);
            PatchWord2ListFile(pass2expr, (unsigned short) c);
            break;

        case RANGE_MSB_16CONST:
        case RANGE_MSB_16SIGN:
            PatchWordMSB2ListFile(pass2expr, (unsigned short) c);
            break;

        case RANGE_MSB_PCREL16:
            c -= (gAsmpcPtr->symvalue+2);
            PatchWordMSB2ListFile(pass2expr, (unsigned short) c);
            break;

        case RANGE_LSB_24SIGN:
            PatchInt24ListFile(pass2expr, (size_t) c);
            break;

        case RANGE_LSB_PCREL24:
            /* 24bit signed PC relative jump is expr -= $PC, where $PC points at next instruction (3 bytes after code patch index) */
            c -= (gAsmpcPtr->symvalue+3);
            PatchInt24ListFile(pass2expr, (size_t) c);
            break;

        case RANGE_MSB_24SIGN:
            PatchInt24MSBListFile(pass2expr, (size_t) c);
            break;

        case RANGE_MSB_PCREL24:
            /* 24bit signed PC relative jump is expr -= $PC, where $PC points at next instruction (3 bytes after code patch index) */
            c -= (gAsmpcPtr->symvalue+3);
            PatchInt24MSBListFile(pass2expr, (size_t) c);
            break;

        case RANGE_LSB_32SIGN:
        case RANGE_LSB_32CONST:
            PatchInt32ListFile(pass2expr, (size_t) c);
            break;

        case RANGE_MSB_32SIGN:
        case RANGE_MSB_32CONST:
            PatchInt32MSBListFile(pass2expr, (size_t) c);
            break;

        default:
            return;
    }
}


static void
WriteListingFileHeader (module_t *module)
{
    char strbuf[MAX_STRBUF_SIZE];
    char lstfilename[MAX_NAME_SIZE];
    int width;

    lstfilename[0] = '\0';
    GetListingFilename(module, lstfilename);
    width = (122 - 9 - 2 - (int) strlen (lstfilename));

    Stream2ListingFile(module, copyrightmsg);
    snprintf(strbuf, MAX_STRBUF_SIZE, "%*.*s", (int) (122 - strlen (copyrightmsg)), (int) strlen (date), date);
    Stream2ListingFile(module, strbuf);
    snprintf(strbuf, MAX_STRBUF_SIZE, "Page %03d%*s'%s'\n\n\n", ++PAGENO, width, "", lstfilename);
    Stream2ListingFile(module, strbuf);
}


static void
WriteSymbolTable (sourcefile_t *csfile, const char *msg, const avltree_t *root)
{
    /* get to the end of the listing file */
    SetListingFilePtrToEnd(csfile->module->lstfile);

    LINENO = PAGELEN+1;
    /* top of new page */
    UpdateListingFileLineCounter(csfile->module);

    Stream2ListingFile(csfile->module, "\n");
    Stream2ListingFile(csfile->module, msg);
    Stream2ListingFile(csfile->module, "\n\n");
    LINENO += 4;

    /* write symbol table */
    InOrder (root, (void (*)(void *)) WriteSymbol);
}


static void
WriteSymbol (const symbol_t *n)
{
    char strbuf[MAX_STRBUF_SIZE];

    if (n == NULL)
        return;

    /* Write only touched symbols related to current module (local or global) */
    if ( (n->type & SYMTOUCHED) && ((n->type & SYMLOCAL) || (n->type & SYMXDEF))) {

        snprintf(strbuf, MAX_STRBUF_SIZE, "%s%*s", n->symname, (int) (32-strlen(n->symname)),"");
        Stream2ListingFile(n->owner, strbuf);
        snprintf(strbuf, MAX_STRBUF_SIZE, "= %08lX", n->symvalue);
        Stream2ListingFile(n->owner, strbuf);

        Stream2ListingFile(n->owner, "\n");
        UpdateListingFileLineCounter(n->owner);
    }
}


static void
ReleasePathList(pathlist_t **plist)
{
    pathlist_t *node;

    while(*plist != NULL) {
        node = *plist;
        free(node->directory);  /* release the actual directory path string */

        node = node->nextdir;   /* point at next path node in list */
        free(*plist);           /* release this path node */
        *plist = node;          /* then get ready for releasing the next node */
    }
}


static labels_t *
AllocAddressItem (void)
{
    return (labels_t *) malloc (sizeof (labels_t));
}


static void
SourceFilePass2 (sourcefile_t *csfile)
{
    long result;
    expression_t *pass2expr;

    if (verbose) {
        puts ("Pass2...");
    }

    if (csfile != NULL && csfile->module != NULL && csfile->module->expressions != NULL)
        pass2expr = csfile->module->expressions->firstexpr;
    else
        return; /* no expressions are available for pass 2 */

    /* parse expressions available for pass 2, skip those that are deferred for re-evaluation in pass 2 linking */
    while (pass2expr != NULL) {

        if ( pass2expr->linkexpr == false) {
            /* only evaluate expression if it has not yet been flagged for linking */

            /* set assembler PC for expression evaluation (PC is relative to module beginning) */
            /* update assembler program counter, '$PC' */
            gAsmpcPtr->symvalue = (symvalue_t) pass2expr->codepos;

            /* point to beginning of expression, for error reporting reference */
            csfile->lineptr = (unsigned char *) pass2expr->infixexpr;

            /* define the current line number of the expression to be re-evaluated from the source file */
            csfile->lineno = pass2expr->curline;

            result = EvalNumExpr (pass2expr);
            if ((pass2expr->rangetype & SYMXREF) || (pass2expr->rangetype & SYMADDR)) {
                pass2expr->linkexpr = true;

                /* also store expression in relocatable object file, if enabled */
                /* (this expression will be ignored, if it has already been written to object file) */
                ObjFileWriteExpr (pass2expr, (unsigned char) (pass2expr->rangetype & RANGE));
            }

            if ( pass2expr->linkexpr == false) {
                /* update code memory in pass 2 when not marked for linking */
                UpdateMemExpr(csfile, pass2expr, result);
            }

            if (IsListingFileOpen(csfile->module) == true) {
                PatchListFile (pass2expr, result);
            }
        }

        /* get next pass 2 expression to evaluate... */
        pass2expr = pass2expr->nextexpr;
    }
}


/******************************************************************************
                               Public functions
 ******************************************************************************/


memfile_t *
CreateErrFile( const char *errflname, const char *ext)
{
    char *errfilename = AddFileExtension(errflname, ext);
    memfile_t *errfile;

    /* build a temporary new string for error file name */
    if (errfilename == NULL) {
       ReportRuntimeErrMsg (NULL, "Failed to create error filename - insufficient memory");
       return NULL;
    }

    errfile = MemfNew (errfilename, 1024, 1024); /* err file output is with initial 1K space, incremented by 1K block */
    if (errfile == NULL)
        ReportRuntimeErrMsg (NULL, "Failed to create error memory file");

    free (errfilename);

    return errfile;
}


void
CloseErrorFile(memfile_t *errfile)
{
    if (errfile == NULL)
        return;

    /* flush error file to host filing system, if it as contents, then release the memory file */
    if (errfile->filesize > 0) {
        WriteBuffer2HostFile(errfile->fname, "w", errfile->filedata, (size_t) errfile->filesize);
    }

    MemfFree(errfile);
}


memfile_t *
CreateLstFile( const char *srcflname, const char *ext)
{
    char *lstfilename = AddFileExtension(srcflname, ext);
    memfile_t *lstmemfile = NULL;

    /* build a temporary new string for listing file name */
    if (lstfilename == NULL)
       return lstmemfile;

    lstmemfile = MemfNew (lstfilename, 32768, 32768); /* list file output is with initial 32K space, incremented by 32K block */
    free (lstfilename);

    return lstmemfile;
}


void
CloseListingFile(memfile_t *lstfile)
{
    if (lstfile != NULL) {
        SetListingFilePtrToEnd(lstfile);
        MemfPutc(lstfile, 12); /* end listing file with a Form Feed */

        /* flush listing file to host filing system, then release the memory file */
        WriteBuffer2HostFile(lstfile->fname, "w", lstfile->filedata, (size_t) lstfile->filesize);
        MemfFree(lstfile);
    }
}


void
InitializeListingFile(sourcefile_t *csfile)
{
    PAGENO = 0;
    LINENO = 6;
    /* Begin list/symbol file with a header */
    WriteListingFileHeader(csfile->module);

    /* Get file position of next line in list file */
    listfileptr = GetListingFilePointer(csfile->module);
}


int
GetListingFilePageNo(void)
{
    return PAGENO;
}


/*!
 * \brief Write string to listing file, associated to specified module
 * \details if module nor (indirect) listing is not specified, nothing will be streamed
 * \param module the pointer to the module structure
 * \param str the string to stream to listing file
 */
void
Stream2ListingFile(module_t *module, const char *str)
{
    if (module != NULL ) {
        MemfPuts(module->lstfile, str);
    }
}


int
AssembleSourceFile (sourcefile_t *csfile)
{
    FILE *asmfile = NULL;

    if (csfile == NULL)
        return 0; /* abort assembly */

    if ((asmfile = fopen (MemfGetFilename(csfile->mf), "rb")) == NULL) {
        return 0; /* abort assembly */
    } else {
        /* Cache assembler source file, prepared for parsing... */
        MemfCacheHostFile (csfile->mf, asmfile);
        fclose(asmfile);
    }

    if ( (csfile->module->errfile = CreateErrFile(MemfGetFilename(csfile->mf), errext)) == NULL) {
        ReleaseSourceMemFile(csfile);
        return 0; /* abort assembly of this source code, error file failed to be created */
    }

    SourceFilePass1 (csfile);

    if (asmerror == false) {
        SourceFilePass2 (csfile);

        /* total size of code generated in this module (section code index already points at next byte = size of code) */
        if (csfile->module != NULL && csfile->module->csection != NULL) {
            CODESIZE += (size_t) csfile->module->csection->pc;
            if (verbose) {
                printf ("Size of module is %d bytes\n", csfile->module->csection->pc);
            }

            if (asmerror == false && writeobjectfile == true) {
                ObjFileWriteHeader(csfile, objfile, (size_t) csfile->module->csection->pc);
            }

            if (asmerror == false && writesymtable == true) {
                WriteSymbolTable (csfile, "Local Module Symbols:", csfile->module->localsymbols);
                WriteSymbolTable (csfile, "Global Module Symbols:", globalsymbols);
            }
        }
    }

    if (csfile->module != NULL) {
        CloseErrorFile(csfile->module->errfile);
        csfile->module->errfile = NULL;
    }

    /* Release cached source code file contents from memory - no longer used... */
    ReleaseSourceMemFileCache(csfile);

    return 1;
}


void
SourceFilePass1 (sourcefile_t *csfile)
{
    if (verbose) {
        printf ("Assembling '%s'...\nPass1...\n", MemfGetFilename(csfile->mf));
    }

    while (!MemfEof (csfile->mf)) {
        writeline = true;
        ParseLine (csfile, true);

        /* If, fatal errors, return immediately... */
        if (csfile->asmerror == Err_Memory) {
            break;
        }
    }

    while(addresses != NULL) {
        /* remove label address stack, if allocated... */
        GetAddress (&addresses);
    }

    /* Module name must be defined, use base filename as default */
    if (csfile->module != NULL && csfile->module->mname == NULL) {
        DefineModuleName(csfile->module, MemfGetFilename(csfile->mf));
    }
}


/*!
 * \brief Update section memory of expression result, according to range type
 * \param csfile
 * \param pass2expr the evaluated expression
 * \param constant the data to write to section memory
 * \return true if expression result were successfully updated to section memory
 */
bool
UpdateMemExpr(sourcefile_t *csfile, const expression_t *parsedexpr, long exprvalue)
{
    bool rangeStatus;
    int pcreloffset;
    unsigned char exprrange = parsedexpr->rangetype & RANGE;

    if ( parsedexpr->rangetype & NOTEVALUABLE ) {
        ReportError (csfile, Err_SymNotDefined);
        return false;
    }

    switch (exprrange) {
        case RANGE_8SIGN:
        case RANGE_8UNSIGN:
        case RANGE_LSB_16CONST:
        case RANGE_LSB_16SIGN:
        case RANGE_MSB_16CONST:
        case RANGE_MSB_16SIGN:
        case RANGE_LSB_24SIGN:
        case RANGE_MSB_24SIGN:
        case RANGE_LSB_32SIGN:
        case RANGE_LSB_32CONST:
        case RANGE_MSB_32SIGN:
        case RANGE_MSB_32CONST:
            rangeStatus = ObjFileExpressionRange(csfile, exprvalue, exprrange);
            break;

        case RANGE_PCREL8:
        case RANGE_LSB_PCREL16:
        case RANGE_MSB_PCREL16:
        case RANGE_LSB_PCREL24:
        case RANGE_MSB_PCREL24:
            /* 8bit/16bit/24bit signed PC relative jump is expr -= $PC, where $PC points at next instruction (1, 2 or 3 bytes after code patch index) */
            if (exprrange == RANGE_PCREL8) {pcreloffset = 1;}
            if (exprrange == RANGE_LSB_PCREL16 || exprrange == RANGE_MSB_PCREL16) {pcreloffset = 2;}
            if (exprrange == RANGE_LSB_PCREL24 || exprrange == RANGE_MSB_PCREL24) {pcreloffset = 3;}

            csfile->lineno = parsedexpr->curline; /* in case of range error, report line number of file */
            exprvalue -= (gAsmpcPtr->symvalue + pcreloffset);
            rangeStatus = ObjFileExpressionRange(csfile, exprvalue, exprrange);
            break;

        default:
            /* this happens only if object file contains unknown expression type */
            ReportSrcAsmMessage (csfile, "Unknown Expression Range Type");
            rangeStatus = false;
    }

    if (rangeStatus == false) {
        return false;
    }

    /* patch memory according to type of expression range */
    switch (exprrange) {
        case RANGE_8SIGN:
        case RANGE_8UNSIGN:
        case RANGE_PCREL8:
            UpdateStreamCodeByte (csfile, (int) parsedexpr->codepos, (int) exprvalue);
            break;

        case RANGE_LSB_PCREL16: /* 16bit signed PC relative jump is expr -= $PC, where $PC points at next instruction (2 bytes after code patch index) */
        case RANGE_LSB_16CONST:
        case RANGE_LSB_16SIGN:
            UpdateStreamCodeWord (csfile, (int) parsedexpr->codepos, (unsigned short) exprvalue);
            break;

        case RANGE_MSB_16CONST:
        case RANGE_MSB_16SIGN:
        case RANGE_MSB_PCREL16: /* 16bit signed PC relative jump is expr -= $PC, where $PC points at next instruction (2 bytes after code patch index) */
            UpdateStreamCodeWordMSB (csfile, (int) parsedexpr->codepos, (unsigned short) exprvalue);
            break;

        case RANGE_LSB_PCREL24: /* 24bit signed PC relative jump is expr -= $PC, where $PC points at next instruction (3 bytes after code patch index) */
        case RANGE_LSB_24SIGN:
            UpdateStreamCodeInt24(csfile, (int) parsedexpr->codepos, (int) exprvalue);
            break;

        case RANGE_MSB_24SIGN:
        case RANGE_MSB_PCREL24: /* 24bit signed PC relative jump is expr -= $PC, where $PC points at next instruction (3 bytes after code patch index) */
            UpdateStreamCodeInt24MSB(csfile, (int) parsedexpr->codepos, (int) exprvalue);
            break;

        case RANGE_LSB_32SIGN:
        case RANGE_LSB_32CONST:
            UpdateStreamCodeInt32(csfile, (int) parsedexpr->codepos, exprvalue);
            break;

        case RANGE_MSB_32SIGN:
        case RANGE_MSB_32CONST:
            UpdateStreamCodeInt32MSB(csfile, (int) parsedexpr->codepos, exprvalue);
            break;

        default:
            /* code never arrives here, but make compiler happy */
            rangeStatus = false;
    }

    return rangeStatus;
}


/*!
 * \brief Append (generated) byte to current module section
 * \param csfile the current file (and indirectly the current module)
 * \param byte
 */
void
StreamCodeByte (const sourcefile_t *csfile, unsigned char byte)
{
    if (csfile != NULL && csfile->module != NULL) {
        WriteByte(&csfile->module->csection, byte);
    }
}


/*!
 * \brief Write (append) 16bit word in little endian format to current module section
 * \param csfile the current file (and indirectly the current module)
 * \param word
 */
void
StreamCodeWord (const sourcefile_t *csfile, unsigned short word)
{
    if (csfile != NULL && csfile->module != NULL) {
        WriteWord(&csfile->module->csection, word);
    }
}


/*!
 * \brief Write (append) 16bit word in Most Significant Byte format to current module section
 * \param csfile the current file (and indirectly the current module)
 * \param word
 */
void
StreamCodeWordMSB (const sourcefile_t *csfile, unsigned short word)
{
    if (csfile != NULL && csfile->module != NULL) {
        WriteWordMSB(&csfile->module->csection, word);
    }
}


/*!
 * \brief Write (append) 24bit integer in Least Significant Byte format to current module section
 * \param csfile the current file (and indirectly the current module)
 * \param lw
 */
void
StreamCodeInt24 (const sourcefile_t *csfile, size_t lw)
{
    if (csfile != NULL && csfile->module != NULL) {
        WriteInt24(&csfile->module->csection, lw);
    }
}


/*!
 * \brief Write (append) 24bit integer in Most Significant Byte format to current module section
 * \param csfile the current file (and indirectly the current module)
 * \param lw
 */
void
StreamCodeInt24MSB (const sourcefile_t *csfile, size_t lw)
{
    if (csfile != NULL && csfile->module != NULL) {
        WriteInt24MSB(&csfile->module->csection, lw);
    }
}


/*!
 * \brief Write (append) 32bit integer in Least Significant Byte format to current module section
 * \param csfile the current file (and indirectly the current module)
 * \param lw
 */
void
StreamCodeInt32 (const sourcefile_t *csfile, size_t lw)
{
    if (csfile != NULL && csfile->module != NULL) {
        WriteInt32(&csfile->module->csection, lw);
    }
}


/*!
 * \brief Write (append) 32bit integer in Most Significant Byte format to current module section
 * \param csfile the current file (and indirectly the current module)
 * \param lw
 */
void
StreamCodeInt32MSB (const sourcefile_t *csfile, size_t lw)
{
    if (csfile != NULL && csfile->module != NULL) {
        WriteInt32(&csfile->module->csection, lw);
    }
}


/*!
 * \brief Get current "program counter" in current module section
 * \param csfile
 * \return returns section code index or -1 if section is unavailable
 */
int
GetStreamCodePC (const sourcefile_t *csfile)
{
    int codeidx = -1;

    if (csfile != NULL && csfile->module != NULL && csfile->module->csection != NULL) {
        codeidx = csfile->module->csection->pc;
    }

    return codeidx;
}


/*!
 * \brief Get byte at specified code index in current module section
 * \param csfile
 * \param codeidx
 * \return
 * byte at specified index in section or 0 if index out of range or section is unavailable
 */
unsigned char
GetStreamCodeByte (const sourcefile_t *csfile, int codeidx)
{
    unsigned char byte = 0;

    if (csfile != NULL && csfile->module != NULL) {
        byte = GetByte (csfile->module->csection, codeidx);
    }

    return byte;
}


/*!
 * \brief Update unsigned 8bit integer at specified code index in current module section
 * \param csfile
 * \param codeidx the code offset (relative to start of generated code) to update
 * \param byte
 * \return true if 8bit integer were successfully updated
 */
bool
UpdateStreamCodeByte (sourcefile_t *csfile, int codeidx, int byte)
{
    if (csfile != NULL && csfile->module != NULL) {
        UpdateByte(csfile->module->csection, codeidx, (unsigned char) byte);
        return true;
    } else {
        /* Memory pointer problem - since mandatory structures seem to have failed */
        ReportError (csfile, Err_Memory);
        return false;
    }
}


/*!
 * \brief
 * Update unsigned 16bit integer in Least Significant Byte format at specified code index in current module section,
 * if range check is valid
 * \param csfile
 * \param codeidx
 * \param word
 * \return true if 16bit integer were successfully updated to memory
 */
bool
UpdateStreamCodeWord (sourcefile_t *csfile, int codeidx, unsigned short word)
{
    if (csfile != NULL && csfile->module != NULL) {
        UpdateWord(csfile->module->csection, codeidx, word);
        return true;
    } else {
        ReportError (csfile, Err_Memory);
        return false;
    }
}


/*!
 * \brief
 * Update unsigned 16bit integer in Most Significant Byte format at specified code index in current module section,
 * if range check is valid
 * \param csfile
 * \param codeidx
 * \param word
 * \return true if 16bit integer were successfully updated to memory
 */
bool
UpdateStreamCodeWordMSB (sourcefile_t *csfile, int codeidx, unsigned short word)
{
    if (csfile != NULL && csfile->module != NULL) {
        UpdateWordMSB(csfile->module->csection, codeidx, word);
        return true;
    } else {
        ReportError (csfile, Err_Memory);
        return false;
    }
}


/*!
 * \brief
 * Update 24bit integer in Least Significant Byte format at specified code index in current module section,
 * if range check is valid
 * \param csfile
 * \param codeidx
 * \param lw
 * \return true if 24bit integer were successfully updated to memory
 */
bool
UpdateStreamCodeInt24 (sourcefile_t *csfile, int codeidx, int lw)
{
    if (csfile != NULL && csfile->module != NULL) {
        UpdateInt24(csfile->module->csection, codeidx, (size_t) lw);
        return true;
    } else {
        ReportError (csfile, Err_Memory);
        return false;
    }
}


/*!
 * \brief
 * Update 24bit integer in Most Significant Byte format at specified code index in current module section,
 * if range check is valid
 * \param csfile
 * \param codeidx
 * \param lw
 * \return true if 24bit integer were successfully updated to memory
 */
bool
UpdateStreamCodeInt24MSB (sourcefile_t *csfile, int codeidx, int lw)
{
    if (csfile != NULL && csfile->module != NULL) {
        UpdateInt24MSB(csfile->module->csection, codeidx, (size_t) lw);
        return true;
    } else {
        ReportError (csfile, Err_Memory);
        return false;
    }
}


/*!
 * \brief
 * Update 32bit integer in Least Significant Byte format at specified code index in current module section,
 * if range check is valid
 * \param csfile
 * \param codeidx
 * \param lw
 * \return true if 32bit integer were successfully updated to memory
 */
bool
UpdateStreamCodeInt32 (sourcefile_t *csfile, int codeidx, long lw)
{
    if (csfile != NULL && csfile->module != NULL) {
        UpdateInt32(csfile->module->csection, codeidx, (size_t) lw);
        return true;
    } else {
        ReportError (csfile, Err_Memory);
        return false;
    }
}


/*!
 * \brief
 * Update 32bit integer in Most Significant Byte format at specified code index in current module section,
 * if range check is valid
 * \param csfile
 * \param codeidx
 * \param lw
 * \return true if 32bit integer were successfully updated to memory
 */
bool
UpdateStreamCodeInt32MSB (sourcefile_t *csfile, int codeidx, long lw)
{
    if (csfile != NULL && csfile->module != NULL) {
        UpdateInt32MSB(csfile->module->csection, codeidx, (size_t) lw);
        return true;
    } else {
        ReportError (csfile, Err_Memory);
        return false;
    }
}


void
PreserveExpr (expression_t *pfixexpr,      /* pointer to header of postfix expression linked list */
              unsigned char constrange,    /* allowed size of value to be parsed */
              long byteoffset)             /* position in listing file to patch */
{
    if (uselistingfile == true) {
        byteoffset = listfileptr + 16 + 3 * byteoffset + 6 * (byteoffset / 32);
    } else {
        byteoffset = -1;    /* indicate that this expression is not going to be patched in listing file */
    }

    pfixexpr->rangetype |= constrange;
    pfixexpr->listpos = byteoffset;             /* now calculated as absolute file pointer */

    AddExpr2Module(pfixexpr);
}


/*
 * Write current source line to list file with Hex dump of assembled instruction
 */
void
WriteListFileLine (sourcefile_t *csfile)
{
    char strbuf[MAX_STRBUF_SIZE];
    int codeidx = GetStreamCodePC(csfile);
    int distance = codeidx - (int) oldPC; /* get distance of bytes written since last listing file line */
    int l;

    if (strlen (line) == 0) {
        strncpy (line, "\n", 2);
    }

    l = distance;
    if (l == 0) {
        snprintf(strbuf, MAX_STRBUF_SIZE, "%-4d  %08lX%14s%s", csfile->lineno, oldPC, "", line);    /* no bytes generated */
        Stream2ListingFile(csfile->module, strbuf);
    } else if (l <= LSTFILE_BYTESLINE) {
        snprintf(strbuf, MAX_STRBUF_SIZE, "%-4d  %08lX  ", csfile->lineno, oldPC);
        Stream2ListingFile(csfile->module, strbuf);
        for (; l; l--) {
            snprintf(strbuf, MAX_STRBUF_SIZE, "%02X ", GetStreamCodeByte(csfile, codeidx-l));
            Stream2ListingFile(csfile->module, strbuf);
        }
        snprintf(strbuf, MAX_STRBUF_SIZE, "%*s%s", (unsigned short) (LSTFILE_BYTESLINE - distance) * 3, "", line);
        Stream2ListingFile(csfile->module, strbuf);
    } else {
        while (l) {
            UpdateListingFileLineCounter (csfile->module);
            if (l) {
                snprintf(strbuf, MAX_STRBUF_SIZE, "%-4d  %08lX  ", csfile->lineno, (long) (codeidx - l));
                Stream2ListingFile(csfile->module, strbuf);
            }
            for (int k = (l - 32 > 0) ? 32 : l; k; k--) {
                snprintf(strbuf, MAX_STRBUF_SIZE, "%02X ", GetStreamCodeByte(csfile, codeidx-l));
                Stream2ListingFile(csfile->module, strbuf);
                l--;
            }
            Stream2ListingFile(csfile->module, "\n");
        }
        snprintf(strbuf, MAX_STRBUF_SIZE, "%18s%s", "", line);
        Stream2ListingFile(csfile->module, strbuf);
    }

    /* Update list file line counter - check page boundary */
    UpdateListingFileLineCounter (csfile->module);
    oldPC = (size_t) codeidx;
}


void
AddAddress (symbol_t *label, labels_t **stackpointer)
{
    labels_t *newitem;

    if ((newitem = AllocAddressItem ()) != NULL) {
        newitem->labelsym = label;
        newitem->prevlabel = *stackpointer;       /* link new node to current node */
        *stackpointer = newitem;                  /* update stackpointer to new item */
    } else {
        ReportError (NULL, Err_Memory);
    }
}


symbol_t *
GetAddress (labels_t **stackpointer)
{
    labels_t *stackitem;
    symbol_t *labelsym;

    labelsym = (*stackpointer)->labelsym;
    stackitem = *stackpointer;
    *stackpointer = (*stackpointer)->prevlabel;           /* Move stackpointer to previous item */
    free (stackitem);                                     /* release old item space to OS */
    return labelsym;
}


void
UpdateListingFileLineCounter (module_t *module)
{
    if (++LINENO > PAGELEN) {
        /* send FORM FEED to file, then header for next page */
        Stream2ListingFile(module, "\x0C\n");
        WriteListingFileHeader(module);
        LINENO = 6;
    }

    /* Get file position for beginning of next line in list file */
    listfileptr = GetListingFilePointer(module);
}


void ReleasePathInfo(void)
{
    ReleasePathList(&gIncludePath);
    ReleasePathList(&gLibraryPath);
}
