/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "datastructs.h"
#include "avltree.h"
#include "main.h"
#include "options.h"
#include "symtables.h"
#include "exprprsr.h"
#include "libraries.h"
#include "objfile.h"
#include "modules.h"
#include "section.h"
#include "macros.h"
#include "sourcefile.h"
#include "memfile.h"
#include "pass.h"
#include "prsline.h"
#include "errors.h"
#include "z80.h"
#include "crc32.h"
#include "ihex.h"
#include "lonesha256.h"


/******************************************************************************
                          Global variables
 ******************************************************************************/
modules_t *modules = NULL;


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static tracedmodule_t *AllocTracedModule (void);
static tracedmodules_t *AllocLinkHdr (void);
static module_t *AllocModule (void);
static void ReleaseModule (module_t * const module);
static modules_t *AllocModuleHdr (void);
static symbol_t *CloneGlobalSymNode (const symbol_t *symptr);
static symbol_t *RestoreGlobalSymNode (const symbol_t *symptr);
static symbol_t *FilterLibrarySymNode (const symbol_t *symptr);
static void EvaluateModuleExpression(module_t *cmodule, expression_t *postfixexpr);
static void ParseObjfileExpressions (module_t *cmodule, FILE *objf, long nextexpr, long endexpr);
static void WriteExprMsg (memfile_t *errfile, char *infixexpr);
static void EvaluateModuleExpressions (void);
static void WriteMapSymbol (const symbol_t *mapnode);
static void LinkLibReference(const symbol_t *librarynode);
static void AdjustLocalModuleAddress(symbol_t * const localsymnode);
static void CreateCodeSegmentBinFile(char *binflnm);
static void CreateRelocatableBinFile(char *binflnm);
static int LinkTracedModule (module_t *curmodule, const char *filename, long baseptr);
static char *CreateMapFilename(const char *modname);

static const char segmbinext[] = ".bn0";

static tracedmodules_t *tracedmodules;
static section_t *linkedcode = NULL;
static memfile_t *deffile = NULL;
static memfile_t *mapfile = NULL;
static bool modules_loaded = false;


/*!
 * \brief Parse & evaluate all object file expressions in current object file (or library)
 * \details
 * Patch result to final linked executable code. Object file is already opened and points to first
 * expression definition.
 * \param cmodule the current module of the object file
 * \param the open file handle of the object / library file
 * \param nextexpr object file file pointer to expression section (first expr)
 * \param endexpr object file file pointer to end of expression section (last expr)
 */
static void
ParseObjfileExpressions (module_t *cmodule, FILE *objf, long nextexpr, long endexpr)
{
    size_t offsetptr;
    unsigned char type;
    expression_t *postfixexpr;
    unsigned char *infixexpr;

    /* There is no source code line numbers with object file expressions, set to 0 */
    cmodule->cfile->lineno = 0;

    do {
        cmodule->cfile->mf = ObjFileLoadExpr(cmodule->cfile, objf, &type, &offsetptr, &nextexpr);
        if (cmodule->cfile->mf == NULL)
            return;

        infixexpr = cmodule->cfile->mf->filedata;
        cmodule->cfile->eol = false; /* reset end of line parsing flag */
        cmodule->cfile->lineptr = infixexpr;
        GetSym(cmodule->cfile); /* infix expression is ready to be parsed from file data memory... */

        /* parse numerical expression copied from object file position */
        if ((postfixexpr = ParseNumExpr (cmodule->cfile)) != NULL) {
            if (postfixexpr->rangetype & NOTEVALUABLE) {
                ReportError (cmodule->cfile, Err_SymNotDefined);
            } else {
                /* evaluate expression and patch result into linked code */
                postfixexpr->codepos = offsetptr;
                postfixexpr->rangetype = (size_t) type;
                EvaluateModuleExpression(cmodule, postfixexpr);
            }
            FreeNumExpr (postfixexpr);
        } else {
            WriteExprMsg (cmodule->errfile, (char *) infixexpr);
        }

        /* expression parsing completed - release ressources for next.. */
        MemfFree(cmodule->cfile->mf);
        cmodule->cfile->mf = NULL;

    } while (nextexpr < endexpr);
}


/*!
 * \brief Evaluate postfix expression and patch the result into the absolute position of the final linked code
 * \param the specified module containing the expression
 * \param postfixexpr postfix notation of expression, generated from ParseNumExpr()
 */
static void
EvaluateModuleExpression(module_t *cmodule, expression_t *postfixexpr)
{
    size_t modorgaddr = modules->first->origin + cmodule->startoffset;      /* Module-specific absolute ORG address */
    long exprvalue;

    /* There is no source code line numbers with module postfix expression, set to 0 */
    cmodule->cfile->lineno = 0;

    /* prepare assembler PC as absolute address, when used during expression evaluation */
    /* update assembler program counter, '$PC' */
    gAsmpcPtr->symvalue = (symvalue_t) (modorgaddr + postfixexpr->codepos);

    /* final re-evaluation of expression, in context of absolute addresses of all modules linked as one binary */
    exprvalue = EvalNumExpr (postfixexpr);
    /* preset infix expression for error reporting */
    cmodule->cfile->lineptr = (unsigned char *) postfixexpr->infixexpr;

    if (UpdateMemExpr(cmodule->cfile, postfixexpr, exprvalue) == false) {
        WriteExprMsg (cmodule->errfile, postfixexpr->infixexpr);
    } else {
        /* expression patched successfully into binary, register relocatable entry if specified */
        if (z80autorelocate == true && ((postfixexpr->rangetype & RANGE) == RANGE_LSB_16CONST) && (postfixexpr->rangetype & SYMADDR)) {
            RegisterRelocEntry((unsigned short) (modorgaddr + postfixexpr->codepos));
        }
    }
}


static void
WriteExprMsg (memfile_t *errfile, char *infixexpr)
{
    char  tmpstr[MAX_STRBUF_SIZE+1];

    snprintf(tmpstr, MAX_STRBUF_SIZE, "Error/Warning in expression %s\n\n", infixexpr);
    MemfPuts(errfile, infixexpr);
}


static
void LinkLibReference(const symbol_t *librarynode)
{
    if (librarynode == NULL)
        return;

    if ((FindSymbol (librarynode->symname, globalsymbols) == NULL) && (SearchLibraries (librarynode->symname) != 0))  {
        printf("Warning: LIB reference '%s' was not found in libraries.\n", librarynode->symname);
    }
}


static
void AdjustLocalModuleAddress(symbol_t *const localsymnode)
{
    if (localsymnode == NULL)
        return;

    if ((localsymnode->type & SYMADDR) && (localsymnode->type & SYMDEFINED)) {
        /* adjust module address to absolute linkable address */
        localsymnode->symvalue += modules->first->origin + localsymnode->owner->startoffset;
    }
}


static void
EvaluateExpressionsFromObjFile(const tracedmodule_t *curlink)
{
    long fptr_base = curlink->modulestart;
    long fptr_namedecl;
    long fptr_modname;
    long fptr_exprdecl;
    long fptr_libnmdecl;
    FILE *objf = OpenFile (curlink->objfilename, gLibraryPath, false);

    if (objf == NULL) {
        /* couldn't open relocatable file */
        ReportIOError (curlink->objfilename);
        return;
    }

    /* point at module name file pointer */
    fseek (objf, fptr_base + SIZEOF_MPMOBJHDR + 4, SEEK_SET);

    fptr_modname = ObjFileReadLong (objf);       /* get file pointer to module name */
    fptr_exprdecl = ObjFileReadLong (objf);      /* get file pointer to expression declarations */
    fptr_namedecl = ObjFileReadLong (objf);      /* get file pointer to name declarations */
    fptr_libnmdecl = ObjFileReadLong (objf);     /* get file pointer to library name declarations */

    if (fptr_exprdecl != -1) {                      /* 0xffffffff = -1 */
        fseek (objf, fptr_base + fptr_exprdecl, SEEK_SET);
        if (fptr_namedecl != -1) {
            /* Evaluate until beginning of name declarations */
            ParseObjfileExpressions (curlink->moduleinfo, objf, fptr_exprdecl, fptr_namedecl);
        } else if (fptr_libnmdecl != -1) {
            /* Evaluate until beginning of library reference declarations */
            ParseObjfileExpressions (curlink->moduleinfo, objf, fptr_exprdecl, fptr_libnmdecl);
        } else {
            /* Evaluate until beginning of module name */
            ParseObjfileExpressions (curlink->moduleinfo, objf, fptr_exprdecl, fptr_modname);
        }
    }

    fclose (objf);
}


/*!
 * \brief Evaluate in-memory expressions (from pass 2) for linking and patch in module code
 * \param cmodule the specified module containing expressions
 * \param pass2expr pointer to first expression in list to evaluate
 */
static void
EvaluateInMemoryExpressions(module_t *cmodule, expression_t *pass2expr)
{
    while (pass2expr != NULL) {
        if (pass2expr->linkexpr == true) {
            /* Only re-evaluate expressions that were delayed in pass 2 for linking stage */
            EvaluateModuleExpression(cmodule, pass2expr);
        }

        pass2expr = pass2expr->nextexpr;
    }
}


/*!
 * \brief Pass 2 of module linking: Evaluate all module expressions and patch results in binary code locations.
 * \details
    Expressions of the module might be available in-memory (from pass 2) or needs to
    be read from object file or linked library (object file) module.
 */
static void
EvaluateModuleExpressions (void)
{
    const tracedmodule_t *curlink = tracedmodules->firstlink;
    module_t *cmodule;

    if (verbose) {
        puts ("Pass2...");
    }

    while (curlink != NULL) {
        cmodule = curlink->moduleinfo;

        if (cmodule->inobjfile == true) {
            /* this module needs to fetch expressions from object file */
            EvaluateExpressionsFromObjFile(curlink);
        } else {
            if (cmodule->expressions != NULL)
                EvaluateInMemoryExpressions(cmodule, cmodule->expressions->firstexpr);
        }

        curlink = curlink->nextlink;
    }
}


static char *
CreateMapFilename(const char *modname)
{
    char *mapfilename;

    if (expl_binflnm == false) {
        /* create map filename based on first ORG (project) filename */
        mapfilename = AddFileExtension(modname, mapext);
    } else {
        /* -o option defines explicit binary output filename, use that as basis for map filename */
        mapfilename = AddFileExtension((const char *) binfilename, mapext);
    }

    return mapfilename;
}


static char *
CreateBinfilename(void)
{
    char *tmpstr;

    if (expl_binflnm == true) {
        /* use predefined filename from command line for generated binary */
        tmpstr = strclone(binfilename);
    } else {
        /* create output filename, based on project filename */
        tmpstr = AddFileExtension( (const char *) MemfGetFilename(modules->first->cfile->mf), binext);
    }

    return tmpstr;
}


static void
CreateCodeSegmentBinFile(char *binflnm)
{
    char *tmpstr;
    char binfilenumber = '0';
    size_t codeblock;
    size_t offset;

    if (CODESIZE > 16384) {
        /* executable binary larger than 16K, use different extension */
        tmpstr = AddFileExtension(binflnm, segmbinext);
        if (tmpstr == NULL) {
            return;
        }

        offset = 0;
        do {
            codeblock = (CODESIZE / 16384U) ? 16384U : CODESIZE % 16384U;
            CODESIZE -= codeblock;
            tmpstr[strlen (tmpstr) - 1] = binfilenumber++;     /* binary 16K block file number */
            WriteBuffer2HostFile(tmpstr, "wb", linkedcode->area+offset, codeblock);
            offset += codeblock;
        } while (CODESIZE);

        free(tmpstr);
    } else {
        /* split binary option enabled, but code size isn't > 16K */
        WriteBuffer2HostFile(binflnm, "wb", linkedcode->area, CODESIZE);
    }
}


static void
CreateRelocatableBinFile(char *binflnm)
{
    char *rtlfilename;

    if (z80reltablefile == true) {
        /* relocation table created as separate file with .rtl extension */
        rtlfilename = AddFileExtension(binflnm, ".rtl");
        if (rtlfilename != NULL) {
            remove (rtlfilename); /* remove any prev. relocation table file */
            WriteRelocTable(rtlfilename);
            free(rtlfilename);
        }
        remove (binflnm); /* remove any prev. binary */
    } else {
        /* First create new binary with relocation code */
        WriteRelocRoutine(binflnm);
        /* then address patch table */
        WriteRelocTable(binflnm);
    }

    /* Finally executable binary as one continous block */
    WriteBuffer2HostFile(binflnm, "ab", linkedcode->area, CODESIZE);
}


static int
LinkTracedModule (module_t *curmodule, const char *filename, long baseptr)
{
    tracedmodule_t *newm;
    char *fname;

    /* establish linked list of traced modules */
    if (tracedmodules == NULL) {
        tracedmodules = AllocLinkHdr ();
        if ( tracedmodules == NULL) {
            ReportError (NULL, Err_Memory);
            return Err_Memory;
        }
    }

    fname = strclone(filename);      /* get a Copy module file name */
    if (fname == NULL) {
        ReportError (NULL, Err_Memory);
        return Err_Memory;
    }

    if ((newm = AllocTracedModule ()) == NULL) {
        free (fname);             /* release redundant Copy of filename */
        ReportError (NULL, Err_Memory);
        return Err_Memory;
    } else {
        newm->nextlink = NULL;
        newm->objfilename = fname;
        newm->modulestart = baseptr;
        newm->moduleinfo = curmodule;             /* pointer to current (active) module structure */
    }

    if (tracedmodules->firstlink == NULL) {
        tracedmodules->firstlink = newm;
        tracedmodules->lastlink = newm; /* First module trace information */
    } else {
        tracedmodules->lastlink->nextlink = newm;       /* current/last linked module points now at new current */
        tracedmodules->lastlink = newm;                 /* pointer to current linked module updated */
    }

    return 0; /* indicate "no errors" */
}


static symbol_t *
CopyGlobalLabels (const symbol_t * symptr)
{
    symbol_t *copiedsym = NULL;

    if ((symptr != NULL) && (symptr->type & SYMXDEF) && (symptr->type & SYMADDR)) {
        copiedsym = CreateSymbol (symptr->symname, symptr->symvalue, symptr->type, symptr->owner);
    }

    return copiedsym;
}


static void
WriteMapSymbol (const symbol_t *mapnode)
{
    char strbuf[MAX_STRBUF_SIZE];

    if ((mapnode != NULL) && (mapnode->type & SYMADDR) && (mapnode->owner != NULL)) {
        /* write symbols owned by modules */
        snprintf (strbuf, MAX_STRBUF_SIZE, "%s%*s", mapnode->symname, (int) (32-strlen(mapnode->symname)),"");
        MemfPuts(mapfile, strbuf);
        snprintf (strbuf, MAX_STRBUF_SIZE, "= %08lX, ", mapnode->symvalue);
        MemfPuts(mapfile, strbuf);

        if (mapnode->type & SYMLOCAL) {
            MemfPutc(mapfile, 'L');
        } else {
            MemfPutc(mapfile, 'G');
        }

        snprintf (strbuf, MAX_STRBUF_SIZE, ": %s\n", mapnode->owner->mname);
        MemfPuts(mapfile, strbuf);
    }
}


/* ------------------------------------------------------------------------------------------
    static symbol_t *CloneGlobalSymNode (symbol_t * symptr)

    Utility function for Avltree Copy()

    Clone all true global symbols into module data structure, which is to be restored
    when module linking begins.
   ------------------------------------------------------------------------------------------ */
static symbol_t *
CloneGlobalSymNode (const symbol_t *symptr)
{
    unsigned long symtype;
    char wrnmsg[MAX_STRBUF_SIZE];

    if (symptr == NULL)
        return NULL;

    if ((symptr->type & SYMXDEF) && (symptr->type & SYMTOUCHED)) {
        if (symptr->type & SYMFUNC) {
            return NULL; /* ignore assembler function runtime variable */
        } else {
            return CreateSymbol (symptr->symname, symptr->symvalue, symptr->type, symptr->owner);
        }
    }

    /* ignore external references which has no value when restoring global systems at linking */
    if (symptr->owner == NULL) {
        return NULL;
    }

    if (validaterefs == true && ((symptr->type & SYMTOUCHED) == 0)) {
        symtype = symptr->type & SYMTYPE;

        switch (symtype)  {
            case SYMXDEF:
                snprintf (wrnmsg, MAX_STRBUF_SIZE, "Warning: In Module %s, XDEF %s is not used", symptr->owner->mname, symptr->symname);
                break;
            case SYMXREF:
                snprintf (wrnmsg, MAX_STRBUF_SIZE, "Warning: In Module %s, XREF %s is not used", symptr->owner->mname, symptr->symname);
                break;
            default:
                return NULL;
        }

        ReportWarningMessage(wrnmsg);
    }

    return NULL;
}


/* ------------------------------------------------------------------------------------------
    static symbol_t *RestoreGlobalSymNode (symbol_t * symptr)

    Utility function for Avltree Copy()

    Restore global symbols from module data structure, also adjusting module labels
    to absolute addresses of linkable code.
   ------------------------------------------------------------------------------------------ */
static symbol_t *
RestoreGlobalSymNode (const symbol_t *symptr)
{
    symvalue_t value;

    if (symptr == NULL) {
        return NULL;
    } else {
        value = symptr->symvalue;
        if ((symptr->type & SYMADDR) && (symptr->type & SYMDEFINED)) {
            value += modules->first->origin + symptr->owner->startoffset;   /* adjust module address to absolute linkable address */
        }

        return CreateSymbol (symptr->symname, value, symptr->type, symptr->owner);
    }
}


static symbol_t *
FilterLibrarySymNode (const symbol_t *symptr)
{
    if (symptr == NULL) {
        return NULL;
    } else {
        if ((symptr->type & SYMXREF) && (symptr->type & SYMDEF) && (symptr->type & SYMTOUCHED)) {
            return CreateSymbol (symptr->symname, symptr->symvalue, symptr->type, symptr->owner);
        } else {
            /* ignore everything that is not an external reference */
            return NULL;
        }
    }
}


static void
ReleaseModule (module_t *const module)
{
    if (module == NULL)
        return;

    ReleaseFile (module->cfile);
    DeleteAll (&module->localsymbols, (void (*)(void *)) FreeSym);
    DeleteAll (&module->notdeclsymbols, (void (*)(void *)) FreeSym);
    DeleteAll (&module->globalsymbols, (void (*)(void *)) FreeSym);
    DeleteAll (&module->libsymbols, (void (*)(void *)) FreeSym);
    DeleteAll (&module->macros, (void (*)(void *)) FreeMacro);

    ReleaseExprns (module->expressions);

    if (module->mname != NULL) {
        free (module->mname);
    }

    if (module->csection != NULL)
        FreeSection(&module->csection);

    if (module->errfile != NULL) {
        MemfFree(module->errfile);
    }

    if (module->lstfile != NULL) {
        MemfFree(module->lstfile);
    }

    free(module);
}


static modules_t *
AllocModuleHdr (void)
{
    modules_t *mhdr = (modules_t *) malloc (sizeof (modules_t));

    if (mhdr != NULL) {
        mhdr->first = NULL;
        mhdr->last = NULL;       /* Module header initialised */
    }

    return mhdr;
}


static module_t *
AllocModule (void)
{
    module_t *newm = (module_t *) malloc (sizeof (module_t));

    if (newm != NULL) {
        newm->nextmodule = NULL;
        newm->mname = NULL;
        newm->startoffset = CODESIZE;
        newm->origin = -1UL;
        newm->errfile = NULL;
        newm->lstfile = NULL;
        newm->cfile = NULL;
        newm->csection = NULL;
        newm->localsymbols = NULL;
        newm->notdeclsymbols = NULL;
        newm->globalsymbols = NULL;
        newm->macros = NULL;
        newm->libsymbols = NULL;
        newm->inobjfile = false;
    }

    return newm;
}


static tracedmodules_t *
AllocLinkHdr (void)
{
    tracedmodules = (tracedmodules_t *) malloc (sizeof (tracedmodules_t));

    if (tracedmodules != NULL) {
        tracedmodules->firstlink = NULL;
        tracedmodules->lastlink = NULL;     /* library header initialised */
    }

    return tracedmodules;
}


static tracedmodule_t *
AllocTracedModule (void)
{
    return (tracedmodule_t *) malloc (sizeof (tracedmodule_t));
}



/******************************************************************************
                               Public functions
 ******************************************************************************/

void
LinkModules (void)
{
    if (verbose) {
        puts ("Linking module(s)...\nPass1...");
    }

    if (z80autorelocate == true && InitRelocTable() == NULL) {
        return;    /* No more room */
    }

    /* load delayed object modules and fetch linked libraries, preparing for linking of final code */
    LoadModules ();

    if (verbose == true) {
        printf ("Code size of linked modules is %d bytes\n", (int) CODESIZE);
    }

    if (asmerror == false) {
        /*  Evaluate remaining expressions in all modules and patch results into linked binary code */
        EvaluateModuleExpressions ();
    }

    /* Release module link information */
    ReleaseLinkInfo ();
}


/*!
 * \brief After pass 2, copy all XDEF declarations to globalsymbols of specified module
 * \details
 * In this process, also remove all XREF declarations. All global symbols will be re-built
 * in-memory from all modules during linking.
 * \param cmodule
 */
void
ReferenceGlobalSymbols(module_t *cmodule)
{
    /* only copy XDEF symbols into current module (ignore XREF declarations) */
    if (!Copy (globalsymbols, &cmodule->globalsymbols, (compfunc_t) cmpidstr, (void *(*)(void *)) CloneGlobalSymNode)) {
        ReportError (NULL, Err_Memory);
    }
}


/*!
 * \brief After pass 2, copy all XREF/LIB declarations to libsymbols of specified module
 * \details This will be used to link external modules from associated libraries using in-memory data structures
 * \param cmodule
 */
void
ReferenceLibrarySymbols(module_t *cmodule)
{
    /* only copy XREF symbols into current module */
    if (!Copy (globalsymbols, &cmodule->libsymbols, (compfunc_t) cmpidstr, (void *(*)(void *)) FilterLibrarySymNode)) {
        ReportError (NULL, Err_Memory);
    }
}


/* ------------------------------------------------------------------------------------------
   void LoadModules (void)

   Pass 1 of linking:
   Load (concatanate) object file code and read symbols that is address relocated for updated
   positions of final binary. All libraries (if library linking is enabled at command line)
   are also appended after all project object modules.
   ------------------------------------------------------------------------------------------ */
void
LoadModules (void)
{
    const char *objwatermark;
    const module_t *lastobjmodule;
    size_t originAddress;
    FILE *objf;
    memfile_t *errfile;
    module_t *cmodule;
    int link_error = 0;

    if (modules_loaded == true) {
        /* modules were already loaded */
        return;
    }

    tracedmodules = NULL;
    cmodule = modules->first;     /* begin with first module */
    lastobjmodule = modules->last;      /* remember this last module, further modules are libraries */

    if (uselibraries) {
        /* Index libraries for quick lookup, before linking starts */
        IndexLibraries();
    }

    if ( (errfile = CreateErrFile(MemfGetFilename(cmodule->cfile->mf), errext)) == NULL) {
        ReportRuntimeErrMsg  (NULL, "Failed to create error file - insufficient memory");
        return;
    }

    /* Create standard '$PC' identifier in global symbol table */
    if (CreateAsmPc(cmodule->cfile) == false) {
        CloseErrorFile (errfile);
        return;
    }

    /* link machine code & read symbols in all modules */
    do {

        cmodule->errfile = errfile;
        cmodule->cfile->lineno = 0;    /* no line references on errors during linking process */

        if (cmodule->inobjfile == true) {
            /* this module needs to fetch all data from object file */
            objfilename = AddFileExtension((const char *) MemfGetFilename(cmodule->cfile->mf), objext);
            if (objfilename == NULL) {
                CloseErrorFile (errfile);
                cmodule->errfile = NULL;
                return;
            }

            if ((objf = ObjFileOpen(objfilename, &objwatermark)) == NULL) {
                CloseErrorFile (errfile);
                cmodule->errfile = NULL;
                return;
            }

            originAddress = (size_t) ObjFileReadLong (objf);     /* get ORIGIN */

            if (modules->first == cmodule) {
                /* origin of first module */
                if (z80autorelocate) {
                    cmodule->origin = 0;    /* ORG 0 on auto relocation */
                } else {
                    if (deforigin) {
                        cmodule->origin = EXPLICIT_ORIGIN;    /* use origin from command line */
                    } else {
                        cmodule->origin = originAddress;
                        if (cmodule->origin == 0xFFFFFFFF) {
                            /* if no ORG has been specified, assemble for address 0 */
                            modules->first->origin = 0;
                            ReportWarning (NULL, 0, Warn_NoOrgDefined);
                        }
                    }
                }

                if (verbose == true) {
                    printf ("ORG address for code is %04lX\n", cmodule->origin);
                }
            }
            fclose (objf);

            link_error = LinkObjfileModule (cmodule, objfilename, 0, objwatermark);

            free (objfilename);               /* release allocated file name */
            objfilename = NULL;
        } else {
            /* the module data is already in memory */

            /* Adjust module labels to absolute address for linkable code */
            InOrder (cmodule->localsymbols, (void (*)(void *)) AdjustLocalModuleAddress);

            /* Restore global variable references; local module symbols and expressions are already available for linking */
            if (!Copy (cmodule->globalsymbols, &globalsymbols, (compfunc_t) cmpidstr, (void *(*)(void *)) RestoreGlobalSymNode)) {
                ReportError (NULL, Err_Memory);
                CloseErrorFile (errfile);
                cmodule->errfile = NULL;
                return;
            }
            DeleteAll (&cmodule->globalsymbols, (void (*)(void *)) FreeSym);

            if (uselibraries) {
                /* link library modules, if any LIB references are registered */
                InOrder (cmodule->libsymbols, (void (*)(void *)) LinkLibReference);
            }

            /* register this in-memory module for expression re-evaluation (use module name as replacement for object file name) */
            LinkTracedModule (cmodule, cmodule->mname, 0);
        }

        /* remove reference of temporary error file */
        cmodule->errfile = NULL;

        /* get next module, if any */
        cmodule = cmodule->nextmodule;
    } while (cmodule != lastobjmodule->nextmodule && link_error == 0); /* parse only object modules, not added library modules */

    CloseErrorFile (errfile);

    modules_loaded = true;
}


/*!
 * \brief LinkObjfileModule
 * \param cmodule
 * \param filename
 * \param fptr_base
 * \param objwatermark
 * \return
 */
int
LinkObjfileModule (module_t *cmodule, char *filename, const long fptr_base, const char *objwatermark)
{
    long fptr_namedecl;
    long fptr_modname;
    long fptr_modcode;
    long fptr_libnmdecl;
    int size;
    int linklib_error;
    FILE *objf;

    objf = OpenFile (filename, gLibraryPath, false);  /* open object file for reading */
    fseek (objf, fptr_base + (long) strlen(objwatermark) + 4U, SEEK_SET);  /* get ready to read module name file pointer */

    fptr_modname = ObjFileReadLong (objf);         /* get file pointer to module name */
    ObjFileReadLong (objf);                        /* read past file pointer to expression declarations */
    fptr_namedecl = ObjFileReadLong (objf);        /* get file pointer to name declarations */
    fptr_libnmdecl = ObjFileReadLong (objf);       /* get file pointer to library name declarations */
    fptr_modcode = ObjFileReadLong (objf);         /* get file pointer to module code */

    if (fptr_modcode != -1) {                     /* 0xffffffff = -1 */
        fseek (objf, fptr_base + fptr_modcode, SEEK_SET);   /* set file pointer to module code */
        size = (int) ObjFileReadLong (objf);                             /* get 32bit integer code size */
        if (WriteFileBlock(&cmodule->csection, objf, size) != size) {    /* read object code into module section */
            fclose (objf);
            ReportIOError(filename);
            return Err_FileIO;
        }

        if (cmodule->startoffset == CODESIZE) {
            CODESIZE += (size_t) size;    /* a new module has been added */
        }
    }

    if (fptr_namedecl != -1) {
        fseek (objf, fptr_base + fptr_namedecl, SEEK_SET);  /* set file pointer to point at name declarations */

        if (fptr_libnmdecl != -1) {
            ObjFileReadSymbols (cmodule, objf, modules->first->origin, fptr_namedecl, fptr_libnmdecl);    /* Read symbols until library declarations */
        } else {
            ObjFileReadSymbols (cmodule, objf, modules->first->origin, fptr_namedecl, fptr_modname);    /* Read symbols until module name */
        }
    }

    fclose (objf);

    /* Indicate that this module has fetched its data from the object file */
    cmodule->inobjfile = true;

    if ((uselibraries == true) && (fptr_libnmdecl != -1)) {
        /* link library modules, if any LIB references are present */
        linklib_error = LinkObjFileLibModules (filename, fptr_base, fptr_libnmdecl, fptr_modname);    /* link library modules */
        if (linklib_error != 0) {
            return linklib_error;
        }
    }

    return LinkTracedModule (cmodule, filename, fptr_base);        /* Remember module for pass2 */
}


bool
ExistBinFile(void)
{
    char *binflnmstr;
    char binfilenumber = '0';
    size_t codeblock;
    size_t offset;
    FILE *filehandle;
    bool found = false;

    if ((binflnmstr = CreateBinfilename()) == NULL) {
        ReportRuntimeErrMsg  (NULL, "Failed to create binary filename - insufficient memory");
        return false;
    }

    if (z80codesegment == true && CODESIZE > 16384) {
        /* executable binary larger than 16K, use different extension */
        binflnmstr = AddFileExtension( binflnmstr, segmbinext);
        if (binflnmstr == NULL) {
            return false;
        }

        offset = 0;
        do {
            codeblock = (CODESIZE / 16384U) ? 16384U : CODESIZE % 16384U;
            CODESIZE -= codeblock;
            binflnmstr[strlen (binflnmstr) - 1] = binfilenumber++;     /* binary 16K block file number */
            if ((filehandle = fopen(binflnmstr, "r")) != NULL) {
                fclose(filehandle);
                free (binflnmstr);
                return true;
            }

            offset += codeblock;
        } while (CODESIZE);
    }

    if ((filehandle = fopen(binflnmstr, "r")) != NULL) {
        fclose(filehandle);
        found = true;
    }

    free (binflnmstr);

    return found;
}


void
CreateBinFile (void)
{
    module_t *cmodule;
    char *tmpstr = CreateBinfilename();

    if (tmpstr == NULL) {
        ReportRuntimeErrMsg  (NULL, "Failed to create binary output filename - insufficient memory");
        return;
    }

    linkedcode = NewSection("text", (int) CODESIZE);
    if (linkedcode == NULL) {
        ReportError (NULL, Err_Memory);
        free (tmpstr);
        return;
    }

    /* concatanate all module code sections into final linked binary */
    cmodule = modules->first;
    while(cmodule != NULL) {
        WriteBlock(&linkedcode, cmodule->csection->area, cmodule->csection->pc);
        cmodule = cmodule->nextmodule;
    }

    if (z80codesegment == true) {
        /* -cz80 option */
        CreateCodeSegmentBinFile(tmpstr);
    } else {
        if (z80autorelocate == false) {
            /* Dump executable binary as one continous block to file system */
            WriteBuffer2HostFile(tmpstr, "wb", linkedcode->area, CODESIZE);
        } else {
            /* -Rz80 or -Rz80 option */
            CreateRelocatableBinFile(tmpstr);
        }
    }

    if (verbose) {
        puts ("Code generation completed.");
    }

    free (tmpstr);
}


void
CreateIntelHexFile(void)
{
    char *tmpstr = CreateHexfilename();

    if (tmpstr != NULL) {
        WriteHexFile(tmpstr, linkedcode->area, CODESIZE);
        free(tmpstr);
    } else {
        ReportRuntimeErrMsg  (NULL, "Intel Hex file aborted - insufficient memory");
    }
}


void
CreateCrc32File(void)
{
    FILE *crc32f;
    uint32_t crc32val;
    char *tmpstr = NULL;

    if (expl_binflnm == true) {
        /* create CRC output filename, based on predefined filename from command line */
        tmpstr = AddFileExtension( (const char *) binfilename, crcext);
    } else {
        /* create CRC output filename, based on project filename */
        tmpstr = AddFileExtension( (const char *) MemfGetFilename(modules->first->cfile->mf), crcext);
    }

    if (tmpstr != NULL) {
        crc32f = fopen (AdjustPlatformFilename(tmpstr), "wb");    /* output to xxxxx.crc */
        if (crc32f != NULL) {
            crc32val = crc32(linkedcode->area, (unsigned int) CODESIZE);
            fprintf(crc32f,"%08X\r\n", crc32val);
            fclose (crc32f);
        } else {
            ReportIOError (tmpstr);
        }

        free(tmpstr);
    } else {
        ReportRuntimeErrMsg  (NULL, "Crc32 output aborted, insufficient memory");
    }
}


void
CreateSha256File(void)
{
    FILE *sha256file;
    char *tmpstr;
    unsigned char digest[ 32 ];

    if (expl_binflnm == true) {
        /* create SHA2 output filename, based on predefined filename from command line */
        tmpstr = AddFileExtension( (const char *) binfilename, ".sha2");
    } else {
        /* create SHA2 output filename, based on project filename */
        tmpstr = AddFileExtension( (const char *) MemfGetFilename(modules->first->cfile->mf), ".sha2");
    }

    if (tmpstr != NULL) {

        if ( lonesha256(digest, linkedcode->area, CODESIZE) != 0) {
            fprintf( stderr, "SHA_digest library failure\n" );
            free(tmpstr);
            return;
        }

        sha256file = fopen (AdjustPlatformFilename(tmpstr), "w");    /* output to xxxxx.sha2 */
        if (sha256file != NULL) {
            for (int i = 0; i < 32; i++ )
                fprintf(sha256file, "%02x", digest[ i ] );
            fclose (sha256file);
        } else {
            ReportIOError (tmpstr);
        }

        free(tmpstr);
    } else {
        ReportRuntimeErrMsg  (NULL, "Sha256 output aborted, insufficient memory");
    }
}


void
WriteBuffer2HostFile(char *filename, const char *mode, const unsigned char *buffer, size_t length)
{
    FILE *hostfile = fopen (AdjustPlatformFilename(filename), mode);

    if (hostfile != NULL) {
        fwrite (buffer, sizeof (char), length, hostfile);   /* write buffer as one big chunk */
        fclose (hostfile);
    } else {
        ReportIOError (filename);
    }
}


void
WriteGlobalDefFile (void)
{
    char *globaldefname;
    avltree_t *t = NULL; /* will hold global labels, ordered by address, ascending */

    LoadModules ();     /* For Global Definitions file, collect all XDEFs in project object files */

    /* use first module filename to create global definition file */
    globaldefname = AddFileExtension((const char *) MemfGetFilename(modules->first->cfile->mf), defext);
    if (globaldefname == NULL) {
        return;
    }

    if (!Copy (globalsymbols, &t, (compfunc_t) cmpidval, (void *(*)(void *)) CopyGlobalLabels)) {
        ReportError (NULL, Err_Memory);   /* No more room */
        free (globaldefname);
        return;
    }

    if ((deffile = MemfNew (AdjustPlatformFilename(globaldefname), 4096, 1024)) != NULL) {
        InOrder (t, (void (*)(void *)) WriteXdefLabel);
        WriteBuffer2HostFile(deffile->fname, "w", deffile->filedata, (size_t) deffile->filesize);
        MemfFree(deffile);
        /* Created DEFC file with global label declarations */
    } else {
        ReportIOError (globaldefname);
    }

    if (t != NULL) DeleteAll (&t, (void (*)(void *)) FreeSym);
    free (globaldefname);
}


void
WriteMapFile (void)
{
    avltree_t *maproot = NULL;
    avltree_t *newmaproot = NULL;
    module_t *cmodule;
    char *mapfilename;

    cmodule = modules->first;   /* begin with first module */
    if ((mapfilename = CreateMapFilename( MemfGetFilename(cmodule->cfile->mf))) == NULL) {
        return;
    }

    /* preset memory file size to 10 x size of output binary, increments of 4K */
    if ((mapfile = MemfNew (AdjustPlatformFilename(mapfilename), (long) CODESIZE*10, 4096)) != NULL) {
        if (verbose) {
            puts ("Creating map...");
        }

        do {
            /* Move all local address symbols alphabetically */
            if (Move (&cmodule->localsymbols, &maproot, (compfunc_t) cmpidstr)) {
                cmodule = cmodule->nextmodule;        /* alphabetically */
            } else {
                ReportError (NULL, Err_Memory);   /* No more room */
                break;
            }
        } while (cmodule != NULL);

        /* Move all global address symbols alphabetically */
        if (!Move (&globalsymbols, &maproot, (compfunc_t) cmpidstr)) {
            ReportError (NULL, Err_Memory);   /* No more room */
        }

        if (maproot == NULL) {
            MemfPuts(mapfile, "None.\n");
        } else {
            InOrder (maproot, (void (*)(void *)) WriteMapSymbol);       /* Write map symbols alphabetically */
            if (!Move (&maproot, &newmaproot, (compfunc_t) cmpidval)) {   /* then re-order symbols numerically */
                ReportError (NULL, Err_Memory);   /* No more room */
            }
            MemfPuts(mapfile, "\n\n");

            InOrder (newmaproot, (void (*)(void *)) WriteMapSymbol);    /* Write map symbols numerically */
            DeleteAll (&newmaproot, (void (*)(void *)) FreeSym);        /* then release all map symbols */
        }

        WriteBuffer2HostFile(mapfile->fname, "w", mapfile->filedata, (size_t) mapfile->filesize);
        MemfFree(mapfile);
    } else {
        ReportIOError (mapfilename);
    }

    free (mapfilename);
}


void
WriteXdefLabel (const symbol_t * node)
{
    char strbuf[MAX_STRBUF_SIZE];

    if ((node != NULL) && (node->type & SYMADDR) && (node->type & SYMXDEF) && !(node->type & SYMDEF)) {
        /* Write only global definitions - not library routines */
        snprintf (strbuf, MAX_STRBUF_SIZE, "DEFC %s", node->symname);
        MemfPuts(deffile, strbuf);
        snprintf (strbuf, MAX_STRBUF_SIZE, " = $%08lX", node->symvalue);
        MemfPuts(deffile, strbuf);
        snprintf (strbuf, MAX_STRBUF_SIZE, "; Module %s\n", node->owner->mname);
        MemfPuts(deffile, strbuf);
    }
}


module_t *
NewModule (void)
{
    module_t *newm;

    /* establish linked list of modules ... */
    if (modules == NULL) {
        modules = AllocModuleHdr ();
        if ( modules == NULL) {
            ReportError (NULL, Err_Memory);
            return NULL;
        }
    }

    if ((newm = AllocModule ()) != NULL) {
        if ((newm->expressions = AllocExprHdr ()) == NULL) {
            ReleaseModule (newm);                       /* remove partial module definition */
            return NULL;                                /* No room for linked list of expressions */
        }

        newm->csection = NewSection ("text", 4096);     /* create default module code container */
        if (newm->csection == NULL) {
            ReleaseModule (newm);                       /* remove partial module definition */
            return NULL;                                /* No room for default code section */
        }

        if (modules->first == NULL) {
            modules->first = newm;
            modules->last = newm;                     /* Add first module to list */
        } else {
            modules->last->nextmodule = newm;         /* current/last module points now at new current */
            modules->last = newm;                     /* pointer to current module updated */
        }
    }

    return newm;
}


void
ReleaseLinkInfo (void)
{
    tracedmodule_t *m;
    tracedmodule_t *n;

    if (tracedmodules == NULL) {
        return;
    }

    m = tracedmodules->firstlink;

    do {
        if (m->objfilename != NULL) {
            free (m->objfilename);
        }

        n = m->nextlink;
        free (m);
        m = n;
    } while (m != NULL);

    free (tracedmodules);

    tracedmodules = NULL;
}


void
ReleaseModules (void)
{
    module_t *nextmodptr;
    module_t *curmodtr;

    if (modules == NULL) {
        return;
    }

    curmodtr = modules->first;
    while (curmodtr != NULL) {      /* until all modules are released */
        nextmodptr = curmodtr->nextmodule;
        ReleaseModule(curmodtr);
        curmodtr = nextmodptr;
    }

    if (modules != NULL) {
        free (modules);
        modules = NULL;
    }

    /* release linked binary code space, if allocated */
    if (linkedcode != NULL) {
        FreeSection (&linkedcode);
    }
}
