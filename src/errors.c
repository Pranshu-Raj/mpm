/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"          /* mpm compiler constant definitions */
#include "datastructs.h"     /* mpm data structure definitions */
#include "main.h"
#include "options.h"
#include "modules.h"
#include "memfile.h"
#include "asmdrctv.h"
#include "prsline.h"
#include "errors.h"


/******************************************************************************
                          Global variables (none)
 ******************************************************************************/


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/

static char *errmsg[] = {
    "No errors detected",                                           /* 0  */
    "File open/read error",                                         /* 1  */
    "Syntax error",                                                 /* 2  */
    "symbol not defined",                                           /* 3  */
    "Not enough memory",                                            /* 4  */
    "Integer out of range",                                         /* 5  */
    "Syntax error in expression",                                   /* 6  */
    "Right bracket missing",                                        /* 7  */
    "Expression out of range",                                      /* 8  */
    "Source filename missing",                                      /* 9  */
    "Illegal or unknown option",                                    /* 10 */
    "Unknown identifier",                                           /* 11 */
    "Illegal identifier",                                           /* 12 */
    "Symbol already defined",                                       /* 13 */
    "Module name already defined",                                  /* 14 */
    "Module name not defined",                                      /* 15 */
    "Library reference not found",                                  /* 16 */
    "Symbol already declared local",                                /* 17 */
    "Symbol already declared global",                               /* 18 */
    "Symbol already declared external",                             /* 19 */
    "No command line arguments",                                    /* 20 */
    "Illegal source filename",                                      /* 21 */
    "Symbol declared global in another module",                     /* 22 */
    "Re-declaration not allowed",                                   /* 23 */
    "ORG already defined",                                          /* 24 */
    "PC-Relative address is out of range",                          /* 25 */
    "Mpm object file not recognized",                               /* 26 */
    "Reserved name",                                                /* 27 */
    "Couldn't open library file",                                   /* 28 */
    "Mpm library file not recognized",                              /* 29 */
    "Environment variable not defined",                             /* 30 */
    "Cannot include file recursively",                              /* 31 */
    "ORG address not yet defined for project",                      /* 32 */
    "Expression > 255 characters",                                  /* 33 */
    "Macro has already been defined",                               /* 34 */
    "Unknown macro parameter",                                      /* 35 */
    "Duplicate macro parameter name",                               /* 36 */
    "Unknown Macro Function name",                                  /* 37 */
    "Warning: No ORG specified - using 0 as base address"           /* 38 */
};

static char *
formatErrorLine(const char *srcfileptr)
{
    char *instr;
    char *errline = strclone(srcfileptr);

    if (errline) {
        /* truncate to only display current line (if multiple lines are available) */
        instr = strchr (errline, '\n');
        if (instr) *instr = '\0';
        trim(errline);
    }

    return errline;
}


/******************************************************************************
                               Public functions
 ******************************************************************************/

char *
GetErrorMessage(const mpmerror_t mpmerrno)
{
    if ( mpmerrno < Err_totalMessages ) {
        return errmsg[mpmerrno];
    } else {
        return NULL;
    }
}


void
ReportWarningMessage (const char *warningmsg)
{
    fprintf (stderr, "%s\n", warningmsg);
}


void
ReportWarning (const char *filename, const int lineno, const int warnno)
{
    char wrnstr[512];
    char wrnlinestr[32];

    wrnlinestr[0] = '\0';
    wrnstr[0] = '\0';

    if (filename != NULL) {
        snprintf (wrnstr, 511, "File '%s', ", filename);
    }

    if (lineno != 0) {
        snprintf (wrnlinestr, 31, "at line %d, ", lineno);
    }

    strncat(wrnstr, wrnlinestr, 511);
    strncat(wrnstr, errmsg[warnno], 511);

    fprintf (stderr, "%s\n", wrnstr);
}


void
ReportSrcAsmMessage  (const sourcefile_t *srcf, const char *message)
{
    char  errstr[MAX_STRBUF_SIZE+1];
    char  errflnmstr[128];
    char  errmodstr[128];
    char  errlinestr[64];
    char  *errline = NULL;
    memfile_t *errfile = NULL;

    asmerror = true;

    errflnmstr[0] = '\0';
    errmodstr[0] = '\0';
    errlinestr[0] = '\0';
    errstr[0] = '\0';

    if (srcf != NULL) {
        if (srcf->lineptr != NULL) {
            errline = formatErrorLine( (char *) srcf->lineptr);
        }

        if (srcf->macro != NULL) {
            snprintf (errflnmstr, 127,"In macro '%s(%d)', ", srcf->macro->macroname, srcf->macro->instanceCounter);
        } else {
            if (srcf->mf != NULL && MemfGetFilename(srcf->mf) != NULL)
                snprintf (errflnmstr, 127, "In file '%s', ", MemfGetFilename(srcf->mf));
        }

        if (srcf->lineno != 0) {
            snprintf (errlinestr, 63, "at line %d, ", srcf->lineno);
        }

        if (srcf->module != NULL) {
            if ( srcf->module->mname != NULL ) {
                snprintf(errmodstr, 127,"Module '%s', ", srcf->module->mname);
            }

            if (srcf->module->errfile != NULL) {
                errfile = srcf->module->errfile;
            }
        }
    }

    strncpy(errstr, errflnmstr, MAX_STRBUF_SIZE);
    strncat(errstr, errmodstr, MAX_STRBUF_SIZE);
    strncat(errstr, errlinestr, MAX_STRBUF_SIZE);
    strncat(errstr, message, MAX_STRBUF_SIZE);

    if (errfile != NULL) {
        MemfPuts(errfile, errstr);
        MemfPutc(errfile, '\n');

        if (errline) {
            MemfPuts(errfile, "Line: >>");
            MemfPuts(errfile, errline);
            MemfPuts(errfile, "<<\n");
        }
    }

    fprintf (stderr, "%s\n", errstr);
    if (errline != NULL) {
        fprintf (stderr, "Line: >>%s<<\n", errline);
        free(errline);
    }
}


void
ReportRuntimeErrMsg  (const char *filename, const char *message)
{
    char  errstr[512];

    errstr[0] = '\0';
    asmerror = true;

    if (filename != NULL) {
        snprintf (errstr, 511, "In file '%s', ", filename);
    }
    strncat(errstr, message, 511);

    fprintf (stderr, "%s\n", errstr);
}


void
ReportError (sourcefile_t *file, const int errnum)
{
    char  errstr[MAX_STRBUF_SIZE+1];
    char  errflnmstr[128];
    char  errmodstr[128];
    char  errlinestr[64];
    char  *errline = NULL;
    memfile_t *errfile = NULL;

    asmerror = true;

    errflnmstr[0] = '\0';
    errmodstr[0] = '\0';
    errlinestr[0] = '\0';
    errstr[0] = '\0';

    if (file != NULL) {
        file->asmerror = errnum;      /* set the global error variable for general error trapping */
    }

    if (file != NULL && file->lineptr != NULL) {
        errline = formatErrorLine((char *) file->lineptr);
    }

    if (file != NULL) {
        if (file->macro != NULL) {
            snprintf (errflnmstr, 127, "In macro '%s(%d)', ", file->macro->macroname, file->macro->instanceCounter);
        } else {
            if (file->mf != NULL && MemfGetFilename(file->mf) != NULL) {
                snprintf (errflnmstr, 127, "In file '%s', ", MemfGetFilename(file->mf));
            }
        }
    }

    if (clinemode == true && clineno) {
        snprintf (errlinestr, 63, "at line %d, ", clineno); /* use last known external line number reference for error report */
    } else {
        if (file != NULL && file->lineno != 0) {
            snprintf (errlinestr, 63, "at line %d, ", file->lineno);
        }
    }

    if (file != NULL && file->module != NULL) {
        if ( file->module->mname != NULL ) {
            snprintf(errmodstr, 127, "Module '%s', ", file->module->mname);
        }

        if (file->module->errfile != NULL) {
            errfile = file->module->errfile;
        }
    }

    strncpy(errstr, errflnmstr, MAX_STRBUF_SIZE);
    strncat(errstr, errmodstr, MAX_STRBUF_SIZE);
    strncat(errstr, errlinestr, MAX_STRBUF_SIZE);
    strncat(errstr, errmsg[errnum], MAX_STRBUF_SIZE);

    if (errfile != NULL) {
        MemfPuts(errfile, errstr);
        MemfPutc(errfile, '\n');

        if (errline != NULL) {
            MemfPuts(errfile, "Line: >>");
            MemfPuts(errfile, errline);
            MemfPuts(errfile, "<<\n");
        }
    }

    /* copy the error to stderr for immediate view */
    fprintf (stderr, "%s\n", errstr);
    if (errline != NULL) {
        fprintf (stderr, "Line: >>%s<<\n", errline);
    }

    if (errline != NULL)
        free(errline);
}


void
ReportIOError (const char *filename)
{
    asmerror = true;

    fprintf (stderr,"File '%s' failed with an I/O error\n", filename);
}
