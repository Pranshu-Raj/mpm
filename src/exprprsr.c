/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "config.h"
#include "datastructs.h"
#include "main.h"
#include "options.h"
#include "errors.h"
#include "symtables.h"
#include "sourcefile.h"
#include "objfile.h"
#include "modules.h"
#include "pass.h"
#include "prsline.h"
#include "memfile.h"
#include "asmdrctv.h"
#include "exprprsr.h"


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static int Condition (expression_t *pfixexpr);
static int Expression (expression_t *pfixexpr);
static int Term (expression_t *pfixexpr);
static int Pterm (expression_t *pfixexpr);
static int Factor (expression_t *pfixexpr);
static long Pw (long x, long y);
static long PopItem (pfixstack_t **stackpointer);
static expression_t *AllocExpr (void);
static pfixstack_t *AllocStackItem (void);
static postfixexpr_t *AllocPfixSymbol (void);
static void PushItem (long oprconst, pfixstack_t **stackpointer);
static void EvalExprStackOperands (enum symbols opr, pfixstack_t **stackptr);
static void NewPfixSymbol (expression_t *pfixexpr, long oprconst, enum symbols oprtype, const char *symident, unsigned long type);
static void AddStringToInfixExpr(expression_t *pfixexpr, const char *ident);
static void AddCharToInfixExpr(expression_t *pfixexpr, char c);
static void StreamExprValue(const sourcefile_t *csfile, long exprvalue, unsigned char exprtype);


static void
EvalExprStackOperands (enum symbols opr, pfixstack_t **stackptr)
{
    long rightoperand = PopItem (stackptr);    /* first get right operator */
    long leftoperand = PopItem (stackptr);     /* then get left operator... */

    switch (opr) {
    case bin_and:
        PushItem (leftoperand & rightoperand, stackptr);
        break;

    case bin_or:
        PushItem (leftoperand | rightoperand, stackptr);
        break;

    case bin_nor:
        PushItem (~(leftoperand | rightoperand), stackptr);
        break;

    case bin_xor:
        PushItem (leftoperand ^ rightoperand, stackptr);
        break;

    case plus:
        PushItem (leftoperand + rightoperand, stackptr);
        break;

    case minus:
        PushItem (leftoperand - rightoperand, stackptr);
        break;

    case multiply:
        PushItem (leftoperand * rightoperand, stackptr);
        break;

    case divi:
        if (rightoperand != 0)
            PushItem (leftoperand / rightoperand, stackptr);
        else
            PushItem (0, stackptr);
        break;

    case mod:
        if (rightoperand != 0)
            PushItem (leftoperand % rightoperand, stackptr);
        else
            PushItem (0, stackptr);
        break;

    case lshift:
        PushItem (leftoperand << (rightoperand % 32), stackptr);
        break;

    case rshift:
        PushItem ((unsigned long) leftoperand >> (rightoperand % 32), stackptr);
        break;

    case power:
        PushItem (Pw (leftoperand, rightoperand), stackptr);
        break;

    case assign:
        PushItem ((leftoperand == rightoperand), stackptr);
        break;

    case less:
        PushItem ((leftoperand < rightoperand), stackptr);
        break;

    case lessequal:
        PushItem ((leftoperand <= rightoperand), stackptr);
        break;

    case greater:
        PushItem ((leftoperand > rightoperand), stackptr);
        break;

    case greatequal:
        PushItem ((leftoperand >= rightoperand), stackptr);
        break;

    case notequal:
        PushItem ((leftoperand != rightoperand), stackptr);
        break;

    default:
        PushItem (0, stackptr);
    }
}


static void
NewPfixSymbol (expression_t *pfixexpr,
               long oprconst,
               enum symbols oprtype,
               const char *symident,
               unsigned long symtype)
{
    postfixexpr_t *newnode;

    if ((newnode = AllocPfixSymbol ()) != NULL) {
        newnode->operandconst = oprconst;
        newnode->operatortype = oprtype;
        newnode->nextoperand = NULL;
        newnode->type = symtype;

        if (symident != NULL) {
            newnode->id = strclone(symident);        /* Allocate symbol */

            if (newnode->id == NULL) {
                free (newnode);
                ReportError (NULL, Err_Memory);
                return;
            }
        } else {
            newnode->id = NULL;
        }
    } else {
        ReportError (NULL, Err_Memory);
        return;
    }

    if (pfixexpr->firstnode == NULL) {
        pfixexpr->firstnode = newnode;
        pfixexpr->currentnode = newnode;
    } else {
        pfixexpr->currentnode->nextoperand = newnode;
        pfixexpr->currentnode = newnode;
    }
}


static void
PushItem (long oprconst, pfixstack_t **stackpointer)
{
    pfixstack_t *newitem;

    if ((newitem = AllocStackItem ()) != NULL) {
        newitem->stackconstant = oprconst;
        newitem->prevstackitem = *stackpointer;   /* link new node to current node */
        *stackpointer = newitem;  /* update stackpointer to new item */
    } else {
        ReportError (NULL, Err_Memory);
    }
}


static long
PopItem (pfixstack_t **stackpointer)
{
    pfixstack_t *oldstackitem;
    long constant = 0;

    if (*stackpointer != NULL) {
        constant = (*stackpointer)->stackconstant;
        oldstackitem = *stackpointer;
        *stackpointer = (*stackpointer)->prevstackitem;       /* Move stackpointer to previous item */
        free (oldstackitem);                                  /* return old item memory to OS */
    }

    return constant;
}


static void
AddStringToInfixExpr(expression_t *pfixexpr, const char *ident)
{
    size_t identLength = strlen(ident);

    if (strlen(pfixexpr->infixexpr)+identLength <= MAX_EXPR_SIZE) {
        memcpy (pfixexpr->infixptr, ident, identLength);   /* add identifier to infix expr */
        pfixexpr->infixptr += identLength;                 /* point at null terminator */
        *pfixexpr->infixptr = 0;                           /* null-terminate */
    } else {
        ReportError (pfixexpr->srcfile, Err_ExprTooBig);
    }
}


static void
AddCharToInfixExpr(expression_t *pfixexpr, const char c)
{
    if (strlen(pfixexpr->infixexpr)+1 <= MAX_EXPR_SIZE) {
        *pfixexpr->infixptr++ = c;                /* add operator, single char to infix expression */
        *pfixexpr->infixptr = 0;                  /* null terminate */
    } else {
        ReportError (pfixexpr->srcfile, Err_ExprTooBig);
    }
}


static int
Factor (expression_t *pfixexpr)
{
    const symbol_t *symptr;
    symfunc AsmFunction;
    long constant;
    char eval_err;
    char *asmfuncArg = NULL;
    char *asmfuncEndBracket;
    int c;

    switch (pfixexpr->srcfile->sym) {
        case asmfnname:
            asmfuncArg = strchr (pfixexpr->srcfile->ident, '[');
            if (asmfuncArg != NULL) {
                *asmfuncArg = '\0';    /* temporarily cut function name and argument, to find base function name */
            }

            AsmFunction = LookupAsmFunction(pfixexpr->srcfile->ident);
            if (asmfuncArg != NULL) {
                *asmfuncArg++ = '[';    /* restore complete identifier, and point to first char of function argument text */
            }

            if (AsmFunction != NULL) {
                asmfuncEndBracket = strrchr (pfixexpr->srcfile->ident, ']');
                if (asmfuncEndBracket != NULL) {
                    *asmfuncEndBracket = '\0';
                }

                symptr = AsmFunction(pfixexpr->srcfile, asmfuncArg);

                if (asmfuncEndBracket != NULL) {
                    *asmfuncEndBracket = ']';
                }

                if (symptr != NULL) {
                    if (symptr->type & SYMDEFINED) {
                        pfixexpr->rangetype |= (symptr->type & SYMTYPE);  /* Copy appropriate type bits */
                        if (symptr->type & SYMADDR) {
                            /* If symbol is an address that always ensures current look-up value of symbol */
                            NewPfixSymbol (pfixexpr, symptr->symvalue, number, pfixexpr->srcfile->ident, symptr->type);
                        } else {
                            NewPfixSymbol (pfixexpr, symptr->symvalue, number, NULL, symptr->type);
                        }
                    } else {
                        pfixexpr->rangetype |= ((symptr->type & SYMTYPE) | NOTEVALUABLE);
                        /* Copy appropriate declaration bits */

                        NewPfixSymbol (pfixexpr, 0, number, pfixexpr->srcfile->ident, symptr->type);
                        /* symbol only declared, store symbol name */
                    }
                } else {
                    pfixexpr->rangetype |= NOTEVALUABLE;  /* expression not evaluable */
                    NewPfixSymbol (pfixexpr, 0, number, pfixexpr->srcfile->ident, SYM_NOTDEFINED);   /* symbol not found */
                }
            } else {
                pfixexpr->rangetype |= NOTEVALUABLE;  /* expression not evaluable */
                NewPfixSymbol (pfixexpr, 0, number, pfixexpr->srcfile->ident, SYM_NOTDEFINED);   /* symbol not found */
            }
            AddStringToInfixExpr(pfixexpr, pfixexpr->srcfile->ident);

            GetSym(pfixexpr->srcfile);
            break;

        case name:
            symptr = GetSymPtr (pfixexpr->srcfile, pfixexpr->srcfile->ident);
            if (symptr != NULL) {
                if (symptr->type & SYMDEFINED) {
                    pfixexpr->rangetype |= (symptr->type & SYMTYPE);  /* Copy appropriate type bits */
                    if (symptr->type & SYMADDR) {
                        /* If symbol is an address that always ensures current look-up value of symbol */
                        NewPfixSymbol (pfixexpr, symptr->symvalue, number, pfixexpr->srcfile->ident, symptr->type);
                    } else {
                        NewPfixSymbol (pfixexpr, symptr->symvalue, number, NULL, symptr->type);
                    }
                } else {
                    pfixexpr->rangetype |= ((symptr->type & SYMTYPE) | NOTEVALUABLE);
                    /* Copy appropriate declaration bits */

                    NewPfixSymbol (pfixexpr, 0, number, pfixexpr->srcfile->ident, symptr->type);
                    /* symbol only declared, store symbol name */
                }
            } else {
                pfixexpr->rangetype |= NOTEVALUABLE;  /* expression not evaluable */
                NewPfixSymbol (pfixexpr, 0, number, pfixexpr->srcfile->ident, SYM_NOTDEFINED);   /* symbol not found */
            }
            AddStringToInfixExpr(pfixexpr, pfixexpr->srcfile->ident);

            GetSym(pfixexpr->srcfile);
            break;

        case hexconst:
        case binconst:
        case decmconst:
            AddStringToInfixExpr(pfixexpr, pfixexpr->srcfile->ident);
            constant = GetConstant (pfixexpr->srcfile, &eval_err);

            if (eval_err == 1) {
                ReportError (pfixexpr->srcfile, Err_ExprSyntax);
                return 0;             /* syntax error in expression */
            } else {
                NewPfixSymbol (pfixexpr, constant, number, NULL, 0);
            }

            GetSym(pfixexpr->srcfile);
            break;

        case lparen:
        case lexpr:
            AddCharToInfixExpr(pfixexpr, separators[pfixexpr->srcfile->sym]); /* store '(' or '[' in infix expr */
            GetSym(pfixexpr->srcfile);

            if (Condition (pfixexpr)) {
                if (pfixexpr->srcfile->sym == rparen || pfixexpr->srcfile->sym == rexpr) {
                    AddCharToInfixExpr(pfixexpr, separators[pfixexpr->srcfile->sym]); /* store ')' or ']' in infix expr */
                    GetSym(pfixexpr->srcfile);
                    break;
                } else {
                    ReportError (pfixexpr->srcfile, Err_ExprBracket);
                    return 0;
                }
            } else {
                return 0;
            }

        case log_not:
            AddCharToInfixExpr(pfixexpr, separators[log_not]);
            GetSym(pfixexpr->srcfile);

            if (!Factor (pfixexpr)) {
                return 0;
            } else {
                NewPfixSymbol (pfixexpr, 0, log_not, NULL, 0);    /* Unary logical NOT... */
            }
            break;

        case greater:
            AddCharToInfixExpr(pfixexpr, '>');
            GetSym(pfixexpr->srcfile);

            if (!Factor (pfixexpr)) {
                return 0;
            } else {
                NewPfixSymbol (pfixexpr, 0, div256, NULL, 0);    /* Unary Divide By 256 */
            }
            break;

        case less:
            AddCharToInfixExpr(pfixexpr, '<');
            GetSym(pfixexpr->srcfile);

            if (!Factor (pfixexpr)) {
                return 0;
            } else {
                NewPfixSymbol (pfixexpr, 0, mod256, NULL, 0);    /* Unary Modulus 256 */
            }
            break;

        case bin_not:
            AddCharToInfixExpr(pfixexpr, separators[bin_not]);
            GetSym(pfixexpr->srcfile);

            if (!Factor (pfixexpr)) {
                return 0;
            } else {
                NewPfixSymbol (pfixexpr, 0, bin_not, NULL, 0);    /* Unary Binary NOT... */
            }
            break;

        case squote:
            AddCharToInfixExpr(pfixexpr, separators[squote]);
            c = GetChar(pfixexpr->srcfile);
            if (c == MFEOF) {
                ReportError (pfixexpr->srcfile, Err_Syntax);
                return 0;
            } else {
                AddCharToInfixExpr(pfixexpr, (char) c); /* store char in infix expr */
                if (GetSym(pfixexpr->srcfile) == squote) {
                    AddCharToInfixExpr(pfixexpr, separators[squote]);
                    NewPfixSymbol (pfixexpr, (long) c, number, NULL, 0);
                } else {
                    ReportError (pfixexpr->srcfile, Err_ExprSyntax);
                    return 0;
                }
            }

            GetSym(pfixexpr->srcfile);
            break;

        default:
            ReportError (pfixexpr->srcfile, Err_ExprSyntax);
            return 0;
    }

    return 1;  /* syntax OK */
}


static int
Pterm (expression_t *pfixexpr)
{
    if (!Factor (pfixexpr)) {
        return 0;
    }

    while (pfixexpr->srcfile->sym == power) {
        AddStringToInfixExpr(pfixexpr, "**");	/* add '**' power symbol to infix expr */

        GetSym(pfixexpr->srcfile);
        if (Factor (pfixexpr)) {
            NewPfixSymbol (pfixexpr, 0, power, NULL, 0);
        } else {
            return 0;
        }
    }

    return 1;
}


static int
Term (expression_t *pfixexpr)
{
    enum symbols mulsym;

    if (!Pterm (pfixexpr)) {
        return 0;
    }

    while ((pfixexpr->srcfile->sym == multiply) || (pfixexpr->srcfile->sym == divi) || (pfixexpr->srcfile->sym == mod)) {
        AddCharToInfixExpr(pfixexpr, separators[pfixexpr->srcfile->sym]); /* store '/', '%', '*' in infix expression */
        mulsym = pfixexpr->srcfile->sym;
        GetSym(pfixexpr->srcfile);
        if (Pterm (pfixexpr)) {
            NewPfixSymbol (pfixexpr, 0, mulsym, NULL, 0);
        } else {
            return 0;
        }
    }

    return 1;
}


static int
Expression (expression_t *pfixexpr)
{
    enum symbols addsym;

    if ((pfixexpr->srcfile->sym == plus) || (pfixexpr->srcfile->sym == minus)) {
        if (pfixexpr->srcfile->sym == minus) {
            AddCharToInfixExpr(pfixexpr, separators[minus]);
        }

        addsym = pfixexpr->srcfile->sym;
        GetSym(pfixexpr->srcfile);

        if (Term (pfixexpr)) {
            if (addsym == minus) {
                NewPfixSymbol (pfixexpr, 0, negated, NULL, 0);      /* operand is signed, plus is redundant... */
            }
        } else {
            return 0;
        }
    } else if (!Term (pfixexpr)) {
        return 0;
    }

    while ( (pfixexpr->srcfile->sym == plus) || (pfixexpr->srcfile->sym == minus) || (pfixexpr->srcfile->sym == bin_and) ||
            (pfixexpr->srcfile->sym == bin_or) || (pfixexpr->srcfile->sym == bin_nor) || (pfixexpr->srcfile->sym == bin_xor) ||
            (pfixexpr->srcfile->sym == lshift) || (pfixexpr->srcfile->sym == rshift)) {

        if (pfixexpr->srcfile->sym == lshift) {
            AddStringToInfixExpr(pfixexpr, "<<");
        } else if (pfixexpr->srcfile->sym == rshift) {
            AddStringToInfixExpr(pfixexpr, ">>");
        } else {
            AddCharToInfixExpr(pfixexpr, separators[pfixexpr->srcfile->sym]);
        }

        addsym = pfixexpr->srcfile->sym;
        GetSym(pfixexpr->srcfile);
        if (pfixexpr->srcfile->sym == minus) {
            AddCharToInfixExpr(pfixexpr, separators[pfixexpr->srcfile->sym]);
            GetSym(pfixexpr->srcfile);
            if (Term (pfixexpr)) {
                NewPfixSymbol (pfixexpr, 0, negated, NULL, 0);      /* operand is signed, negate it... */
                NewPfixSymbol (pfixexpr, 0, addsym, NULL, 0);       /* then push original operator to stack */
            }
        } else if (Term (pfixexpr)) {
            NewPfixSymbol (pfixexpr, 0, addsym, NULL, 0);
        } else {
            return 0;
        }
    }

    return 1;
}


static int
Condition (expression_t *pfixexpr)
{
    enum symbols relsym;

    if (!Expression (pfixexpr)) {
        return 0;
    }

    switch (pfixexpr->srcfile->sym) {
        case less:      /* '<' */
        case greater:   /* '>' */
        case assign:    /* '=' */
            AddCharToInfixExpr(pfixexpr, separators[pfixexpr->srcfile->sym]);
            relsym = pfixexpr->srcfile->sym;
            GetSym(pfixexpr->srcfile);
            break;

        case lessequal:
            AddStringToInfixExpr(pfixexpr, "<=");
            relsym = pfixexpr->srcfile->sym;
            GetSym(pfixexpr->srcfile);
            break;

        case greatequal:
            AddStringToInfixExpr(pfixexpr, ">=");
            relsym = pfixexpr->srcfile->sym;
            GetSym(pfixexpr->srcfile);
            break;

        case notequal:
            AddStringToInfixExpr(pfixexpr, "<>");
            relsym = pfixexpr->srcfile->sym;
            GetSym(pfixexpr->srcfile);
            break;

        default:
            return 1;                 /* implicit (left side only) expression */
    }

    if (!Expression (pfixexpr)) {
        return 0;
    } else {
        NewPfixSymbol (pfixexpr, 0, relsym, NULL, 0);       /* condition... */
    }

    return 1;
}


static long
Pw (long x, long y)
{
    long i = 1;

    while (y > 0) {
        i *= x;
        y--;
    }

    return i;
}


/**
 * @brief Stream/write expression value to current PC (size of value depends on type)
 * @param csfile
 * @param exprvalue the value to write
 * @param exprtype the expression (range) type
 */
static void
StreamExprValue(const sourcefile_t *csfile, long exprvalue, unsigned char exprtype)
{
    switch(exprtype) {
        case RANGE_8SIGN:
        case RANGE_8UNSIGN:
        case RANGE_PCREL8:
            StreamCodeByte (csfile, (unsigned char) exprvalue);
            break;

        case RANGE_LSB_16CONST:
        case RANGE_LSB_16SIGN:
        case RANGE_LSB_PCREL16:
            StreamCodeWord(csfile, (unsigned short) exprvalue);
            break;

        case RANGE_MSB_16CONST:
        case RANGE_MSB_16SIGN:
        case RANGE_MSB_PCREL16:
            StreamCodeWordMSB(csfile, (unsigned short) exprvalue);
            break;

        case RANGE_LSB_PCREL24:
        case RANGE_LSB_24SIGN:
            StreamCodeInt24( csfile, (size_t) exprvalue);
            break;

        case RANGE_MSB_24SIGN:
        case RANGE_MSB_PCREL24:
            StreamCodeInt24MSB( csfile, (size_t) exprvalue);
            break;

        case RANGE_LSB_32SIGN:
        case RANGE_LSB_32CONST:
            StreamCodeInt32( csfile, (size_t) exprvalue);
            break;

        case RANGE_MSB_32SIGN:
        case RANGE_MSB_32CONST:
            StreamCodeInt32MSB( csfile, (size_t) exprvalue);
            break;

        default:
            /* Unknown range type (shouldn't happen), signal a generic range type error */
            ReportSrcAsmMessage (csfile, GetErrorMessage(Err_IntegerRange));
    }
}


static expression_t *
AllocExpr (void)
{
    return (expression_t *) malloc (sizeof (expression_t));
}


static postfixexpr_t *
AllocPfixSymbol (void)
{
    return (postfixexpr_t *) malloc (sizeof (postfixexpr_t));
}


static pfixstack_t *
AllocStackItem (void)
{
    return (pfixstack_t *) malloc (sizeof (pfixstack_t));
}



/******************************************************************************
                               Public functions
 ******************************************************************************/


/**
 * @brief Parse expression at csfile (current source file line) position
 * @details
 * the current PC (code generation pointer) is fetched and preserved
 * inside the expression structure, to be used when the evaluated expression
 * is pathed into code memory.
 * @param csfile the current source file
 * @return return the expression structure for post-evaluation.
 */
expression_t *
ParseNumExpr (sourcefile_t *const csfile)
{
    expression_t *pfixhdr = AllocExpr();
    enum symbols constant_expression = nil;

    if (pfixhdr == NULL) {
        ReportError (NULL, Err_Memory);
        return NULL;
    } else {

        if (csfile == NULL) {
            free(pfixhdr);
            return NULL;
        }

        pfixhdr->srcfile = csfile;                 /* pointer to record containing current source file */
        pfixhdr->exprlineptr = csfile->lineptr;    /* preserve pointer to start of expression in current source file */
        pfixhdr->curline = csfile->lineno;         /* line number of expression in current source file */

        pfixhdr->nextexpr = NULL;
        pfixhdr->firstnode = NULL;
        pfixhdr->currentnode = NULL;
        pfixhdr->rangetype = 0;
        pfixhdr->linkexpr = false;
        pfixhdr->wrobjfile = false;
        pfixhdr->codepos = (size_t) GetStreamCodePC(csfile); /* get current PC for future code generation */

        if ((pfixhdr->infixexpr = (char *) calloc(MAX_EXPR_SIZE+1, sizeof(char))) == NULL) {
            ReportError (NULL, Err_Memory);
            free (pfixhdr);
            return NULL;
        } else {
            /* null-terminated buffer for infix expression prepared */
            pfixhdr->infixptr = pfixhdr->infixexpr;    /* initialise pointer to start of buffer */
        }
    }

    if (csfile->sym == cnstexpr) {
        GetSym(csfile);                /* leading ` : ignore relocatable address expression */
        constant_expression = cnstexpr;  /* convert to constant expression */
        AddCharToInfixExpr(pfixhdr, separators[cnstexpr]);
    }

    if (Condition (pfixhdr)) {
        /* parse expression... */
        if (constant_expression == cnstexpr) {
            NewPfixSymbol (pfixhdr, 0, cnstexpr, NULL, 0);    /* convert to constant expression */
        }

        return pfixhdr;
    } else {
        FreeNumExpr (pfixhdr);
        return NULL;              /* syntax error in expression or no room */
    }                             /* for postfix expression */
}


long
EvalFreeNumExpr (expression_t *pfixlist)
{
    long result = -1;

    if (pfixlist != NULL) {
        result = EvalNumExpr (pfixlist);
        FreeNumExpr(pfixlist);
    }

    return result;
}


long
EvalNumExpr (expression_t *pfixlist)
{
    pfixstack_t *stackptr = NULL;
    const postfixexpr_t *pfixexpr;
    const symbol_t *symptr;

    if (pfixlist == NULL) {
        return 0;
    }

    pfixexpr = pfixlist->firstnode;       /* initiate to first node */
    pfixlist->rangetype &= EVALUATED;     /* prefix expression as evaluated (reset previous flags) */

    while (pfixexpr != NULL) {
        switch (pfixexpr->operatortype) {
        case number:
            if (pfixexpr->id == NULL) {   /* Is operand an identifier? */
                PushItem (pfixexpr->operandconst, &stackptr);
            } else {
                if (pfixexpr->type != SYM_NOTDEFINED) {
                    /* symbol was not defined and not declared */
                    /* if all bits are set to zero */
                    if (pfixexpr->type & SYMLOCAL) {
                        symptr = FindSymbol (pfixexpr->id, pfixlist->srcfile->module->localsymbols);
                        pfixlist->rangetype |= (symptr->type & SYMTYPE);  /* Copy appropriate type
                                                                         * bits */
                        PushItem (symptr->symvalue, &stackptr);
                    } else {
                        symptr = FindSymbol (pfixexpr->id, globalsymbols);
                        if (symptr != NULL) {
                            pfixlist->rangetype |= (symptr->type & SYMTYPE);      /* Copy appropriate type
                                                                                 * bits */
                            if (symptr->type & SYMDEFINED) {
                                PushItem (symptr->symvalue, &stackptr);
                            } else {
                                pfixlist->rangetype |= NOTEVALUABLE;
                                PushItem (0, &stackptr);
                            }
                        } else {
                            pfixlist->rangetype |= NOTEVALUABLE;
                            PushItem (0, &stackptr);
                        }
                    }
                } else {
                    /* try to Find symbol now as either */

                    symptr = GetSymPtr (pfixlist->srcfile, pfixexpr->id);    /* declared local or global */
                    if (symptr != NULL) {
                        pfixlist->rangetype |= (symptr->type & SYMTYPE);  /* Copy appropriate type bits */
                        if (symptr->type & SYMDEFINED) {
                            PushItem (symptr->symvalue, &stackptr);
                        } else {
                            pfixlist->rangetype |= NOTEVALUABLE;
                            PushItem (0, &stackptr);
                        }
                    } else {
                        pfixlist->rangetype |= NOTEVALUABLE;
                        PushItem (0, &stackptr);
                    }
                }
            }
            break;

        case negated:
            if (stackptr != NULL)
                stackptr->stackconstant = -stackptr->stackconstant;
            break;

        case log_not:
            if (stackptr != NULL)
                stackptr->stackconstant = !(stackptr->stackconstant);
            break;

        case bin_not:
            if (stackptr != NULL)
                stackptr->stackconstant = ~(stackptr->stackconstant);
            break;

        case div256:
            if (stackptr != NULL)
                stackptr->stackconstant = stackptr->stackconstant / 256;
            break;

        case mod256:
            if (stackptr != NULL)
                stackptr->stackconstant = stackptr->stackconstant % 256;
            break;

        case cnstexpr:
            /* convert to constant expression */
            pfixlist->rangetype &= CLEAR_EXPRADDR;
            break;

        default:
            /* expression requiring two operands */
            EvalExprStackOperands (pfixexpr->operatortype, &stackptr);
            break;
        }

        /* get next operand in postfix expression */
        pfixexpr = pfixexpr->nextoperand;
    }

    if (stackptr != NULL) {
        return PopItem (&stackptr);
    } else {
        return 0;    /* Unbalanced stack - probably during low memory... */
    }
}


/*!
 * \brief Release all dynamically allocated memory to parse & evaluate expression
 * \param pfixexpr
 */
void
FreeNumExpr (expression_t *pfixexpr)
{
    postfixexpr_t *node;
    postfixexpr_t *tmpnode;

    if (pfixexpr == NULL) {
        return;
    }

    node = pfixexpr->firstnode;
    while (node != NULL) {
        tmpnode = node->nextoperand;
        if (node->id != NULL) {
            free (node->id);    /* Remove symbol id, if defined */
        }

        free (node);
        node = tmpnode;
    }

    if (pfixexpr->infixexpr != NULL) {
        free (pfixexpr->infixexpr);    /* release infix expr. string */
    }

    free (pfixexpr);              /* release header of postfix expression */
}


/**
 * @brief Parse, evaluate and generate code of expression according to range type
 * @details
 * Expression may be deferred for 2nd pass or linking stage, to complete code generation
 * If object file output is enabled, the expression is automatically stored if it
 * contains external reference or uses an address label.
 * @param csfile
 * @param pfixexpr pointer to pre-parsed expression, or NULL
 * @param listoffset the offset of current listing file to patch
 * @return if evaluated, return 1, otherwise 0 (error occurred)
 */
int
ProcessExpr (sourcefile_t *csfile, expression_t *pfixexpr, unsigned char exprtype, int listoffset)
{
    long exprvalue = 0xffffffff; /* expression not evaluated */

    if (pfixexpr == NULL) {
        /* no pre-parsed expression were supplied, parse it now from current source line */
        pfixexpr = ParseNumExpr (csfile);
        if (pfixexpr == NULL) {
            /* abort failed parsed expression */
            return 0;
        }
    }

    /* validate numerical expression */
    if ((pfixexpr->rangetype & SYMXREF) || (pfixexpr->rangetype & SYMADDR)) {
        /* expression contains external reference or address label which must be stored in object file and
           also be re-evaluated during linking */
        ObjFileWriteExpr (pfixexpr, exprtype);
        PreserveExpr (pfixexpr, exprtype, listoffset); /* preserve expression for in-memory linking */
        pfixexpr->linkexpr = true;
    } else {
        if (pfixexpr->rangetype & NOTEVALUABLE) {
            PreserveExpr (pfixexpr, exprtype, listoffset); /* preserve expression for pass 2 re-evaluation */
        } else {
            exprvalue = EvalFreeNumExpr (pfixexpr);
            ObjFileExpressionRange(csfile, exprvalue, exprtype);
        }
    }

    StreamExprValue(csfile, exprvalue, exprtype);
    return 1;
}


/*!
 * \brief Generate 8bit, 16bit or 24bit PC-relative address expression
 * \details
 * Current section program counter points at rel address byte, however,
 * the relative address expression is calculated from PC+X (first byte of next instruction)
 * Expression is automatically deferred for 2nd pass or linking stage, to complete code generation
 * If object file output is enabled, the expression is automatically stored.
 * \param csfile
 * \param exprtype RANGE_PCREL8, RANGE_LSB_PCREL16, RANGE_MSB_PCREL16, RANGE_LSB_PCREL24 & RANGE_MSB_PCREL24
 * \param listoffset
 */
int
ProcessPcRelativeExpr (sourcefile_t *csfile, unsigned char exprtype, int listoffset)
{
    expression_t *pfixexpr = ParseNumExpr (csfile);

    if (pfixexpr == NULL) {
        return 0;
    }

    if (pfixexpr->rangetype & SYMXREF || (pfixexpr->rangetype & SYMADDR)) {
        /* PC-relative call or jump is outside module (external reference), must be re-evaluated during linking */
        ObjFileWriteExpr (pfixexpr, exprtype);
        PreserveExpr (pfixexpr, exprtype, listoffset); /* preserve expression for in-memory linking */
        pfixexpr->linkexpr = true;
    } else {
        /* defer expression evaluation in pass 2 */
        PreserveExpr (pfixexpr, exprtype, listoffset); /* preserve expression for pass 2 re-evaluation */
    }

    /* generate code space for PC-relative offset (either 8, 16 or 24bit) */
    StreamExprValue(csfile, 0xffffffff, exprtype);
    return 1;
}


/*!
 * \brief From specified expression create a new parsed expression, appended with "+1"
 * \param expr
 * \return return new parsed "+1" expression
 */
expression_t *
CloneExprPlusOne(expression_t *expr)
{
    char infixplus1expr[MAX_EXPR_SIZE+1];
    sourcefile_t *exprplusfile;
    expression_t *newexpr;

    if (expr == NULL)
        return NULL;

    strncpy(infixplus1expr, expr->infixexpr, MAX_EXPR_SIZE);
    strncat(infixplus1expr, "+1", MAX_EXPR_SIZE);

    /* create a temp. file from parsed infix expression */
    exprplusfile = CreateFileFromString (NULL, 0, infixplus1expr);
    if (exprplusfile == NULL)
        return NULL; /* insufficient memory, abort */

    /* inherit the module reference for local symbols of current source file where expression is parsed from */
    exprplusfile->module = expr->srcfile->module;

    /* fetch first symbol of expression */
    GetSym(exprplusfile);
    newexpr = ParseNumExpr(exprplusfile);
    /* cloned expression "+1" has been parsed, the temporary "file" can now be released */
    ReleaseFile(exprplusfile);

    return newexpr;
}


/*!
 * \brief Fetch evaluable <n> constant that is part of instruction mnemonic
 * \details
 * Some instructions have an embedded constant that must be evaluable
 * which determine their opcode sequence
 * \param csfile
 * \return an evaluable constant or -1
 */
long
ParseMnemConstant (sourcefile_t *csfile)
{
    expression_t *postfixexpr;

    if ((postfixexpr = ParseNumExpr (csfile)) == NULL)
        return -1; /* syntax error or not an expression */

    if (postfixexpr->rangetype & NOTEVALUABLE) {
        /* expression contains forward referenced or external module reference name */
        ReportError (csfile, Err_SymNotDefined);
        FreeNumExpr (postfixexpr);
        return -1;
    } else {
        /* return constant of the expression */
        return EvalFreeNumExpr (postfixexpr);
    }
}


void
ReleaseExprns (expressions_t *express)
{
    expression_t *tmpexpr;
    expression_t *curexpr;

    if (express != NULL) {
        curexpr = express->firstexpr;
        while (curexpr != NULL) {
            tmpexpr = curexpr->nextexpr;
            FreeNumExpr (curexpr);
            curexpr = tmpexpr;
        }

        free (express);
    }
}


expressions_t *
AllocExprHdr (void)
{
    expressions_t *exprhdr = (expressions_t *) malloc (sizeof (expressions_t));

    if (exprhdr != NULL) {
        /* initialise Module expression header */
        exprhdr->firstexpr = NULL;
        exprhdr->currexpr = NULL;
    }

    return exprhdr;
}
