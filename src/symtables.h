/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#if ! defined MPM_SYMTABLE_HEADER_
#define MPM_SYMTABLE_HEADER_

#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

#define ASSEMBLERPC "$PC"

/* global variables */
extern avltree_t *globalsymbols;
extern avltree_t *staticsymbols;
extern symbol_t *gAsmpcPtr; /* pointer to Assembler PC symbol */

/* global functions */
char       *AllocIdentifier (size_t len);
int        cmpidstr (const symbol_t *kptr, const symbol_t *p);
int        cmpidval (const symbol_t *kptr, const symbol_t *p);
symbol_t   *CreateSymNode (const symbol_t *symptr);
symbol_t   *CreateSymbol (const char *identifier, symvalue_t value, unsigned long symboltype, module_t *symowner);
void       DeclSymGlobal (sourcefile_t *csfile, const char *identifier, unsigned long libtype);
void       DeclSymExtern (sourcefile_t *csfile, const char *identifier, unsigned long libtype);
void       FreeSym (symbol_t * node);
symbol_t   *DefineDefSym (sourcefile_t *csfile, const char *identifier, long value, avltree_t **root);
symbol_t   *DefineSymbol (sourcefile_t *csfile, const char *identifier, symvalue_t value, unsigned long symboltype);
symbol_t   *GetSymPtr (sourcefile_t *csfile, const char *identifier);
symbol_t   *FindSymbol (const char *identifier, const avltree_t * treeptr);
bool       CreateAsmPc(sourcefile_t *csfile);

#endif
