/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "datastructs.h"
#include "avltree.h"
#include "main.h"
#include "options.h"
#include "symtables.h"
#include "objfile.h"
#include "libraries.h"
#include "modules.h"
#include "errors.h"
#include "memfile.h"
#include "sourcefile.h"
#include "pass.h"
#include "prsline.h"


/******************************************************************************
                          Global variables
 ******************************************************************************/


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static int cmpmodname (const char *modname, const libobject_t *p);
static int cmplibnames (const libobject_t *kptr, const libobject_t *p);
static int LinkLibModule (const libfile_t *library, const long module_basefptr, const char *modname);
static void LoadLibraryIndex(avltree_t **libindex, libfile_t *curlib);
static void ReleaseIndexObj(libobject_t *libidxobj);
static libobject_t *FindLibModule (const char *modname);
static libobject_t *AllocLibObject (void);
static libfile_t *NewLibrary (void);
static libfile_t *AllocLibFile (void);
static libraries_t *AllocLibHdr (void);
static void ReleaseLibrariesIndex (void);

static avltree_t *libraryindex = NULL;
static libraries_t *libraries = NULL;



/**
 * @brief LinkLibModule
 * @param library
 * @param module_basefptr
 * @param modname
 * @return  0, successfully linked this LIB module to the application modules list.
            Err_Memory, if memory was not available for allocation of this LIB module object.
            Err_MaxCodeSize, if code buffer max was reached during linking of this LIB module.
 */
static int
LinkLibModule (const libfile_t *library, const long module_basefptr, const char *modname)
{
    module_t *libmodule;
    int link_error;
    char *mname;

    if ((libmodule = NewModule ()) != NULL) {
        /* create new module for library */
        mname = strclone(modname);   /* make a Copy of module name */
        if (mname != NULL) {
            libmodule->mname = mname;
            /* the library file */
            libmodule->cfile = Newfile (libmodule, NULL, library->libfilename);

            if (verbose) {
                printf ("Linking library module <%s>\n", modname);
            }

            /* link library module & read names */
            link_error = LinkObjfileModule (libmodule, library->libfilename, module_basefptr, library->libwatermark);
        } else {
            ReportError (NULL, Err_Memory);
            link_error = Err_Memory;
        }
    } else {
        ReportError (NULL, Err_Memory);
        link_error = Err_Memory;
    }

    return link_error;
}


static int
cmpmodname (const char *modname, const libobject_t *p)
{
    return strcmp (modname, p->libname);
}


static libobject_t *
FindLibModule (const char *modname)    /* pointer to module name in object file */
{
    if (libraryindex == NULL) {
        return NULL;
    } else {
        return Find (libraryindex, modname, (compfunc_t) cmpmodname);
    }
}


static int
cmplibnames (const libobject_t *kptr, const libobject_t *p)
{
    return strcmp (kptr->libname, p->libname);
}


static libobject_t *
AddLibIndexObj(avltree_t **libindex, libfile_t *curlib, const char *modname, long objfile_baseptr)
{
    libobject_t *newlibobj = AllocLibObject();

    if (newlibobj != NULL) {
        newlibobj->libname = strclone(modname);  /* Allocate area for a new module name */
        if (newlibobj->libname == NULL) {
            ReleaseIndexObj(newlibobj);
            newlibobj = NULL;
        } else {
            newlibobj->library = curlib;                /* reference to library file containing library module */
            newlibobj->modulestart = objfile_baseptr;   /* file pointer to base of linkable object in library file */

            /* Insert new library index object into LibraryIndex AVL tree */
            if (Insert (libindex, newlibobj, (compfunc_t) cmplibnames) == 0) {
                ReleaseIndexObj(newlibobj);
                newlibobj = NULL;
            }
        }
    }

    if (newlibobj == NULL) {
        ReportError (NULL, Err_Memory);
    }

    return newlibobj;
}


static void
LoadLibraryIndex(avltree_t **libindex, libfile_t *curlib)
{
    long currentlibmodule;
    long nextlibmodule;
    long modulesize;
    long fptr_mname;
    long objfile_base;
    char mname[MAX_NAME_SIZE];
    libobject_t *foundlib;
    FILE *libf;

    libf = OpenFile (curlib->libfilename, gLibraryPath, false);
    if (libf != NULL) {
        /* offset pointer into first available module in library */
        currentlibmodule = (long) strlen(curlib->libwatermark);

        do {
            /* parse all available active modules in library file */
            do {
                fseek (libf, currentlibmodule, SEEK_SET);       /* point at beginning of a object file module block */
                nextlibmodule = ObjFileReadLong (libf);         /* get file pointer to next module in library */
                modulesize = ObjFileReadLong (libf);            /* get size of current module */
            } while (modulesize == 0 && nextlibmodule != -1);   /* 0xffffffff */

            if (modulesize != 0) {
                /* point at module name file pointer            [ object file  ....................... ]
                 * (past <Next Object File> & <Object File Length> & <Object file Watermark> & <ORG address>)
                 */
                objfile_base = currentlibmodule + 4 + 4;
                fseek (libf, objfile_base + (int) strlen(curlib->libwatermark) + 4, SEEK_SET);

                fptr_mname = ObjFileReadLong (libf);                /* get module name file pointer */
                fseek (libf, objfile_base + fptr_mname, SEEK_SET);  /* point at module name */
                ObjFileReadString (libf, mname);                    /* read module name into buffer variable */

                /* check for lib module in library index */
                foundlib = FindLibModule(mname);
                if (foundlib == NULL) {
                    /* This Library module was not found in the index, Insert library module definition */
                    if (AddLibIndexObj(libindex, curlib, mname, objfile_base) == NULL) {
                        return;    /* Ups, no room left for library index, abort library index creation */
                    }
                } else {
                    /* a library module has already been loaded into the index */
                    /* issue a warning message and replace existing library module info with new info */
                    printf ("Warning: Duplicate objects not allowed in Library Index!\n"
                            "Object '%s' from library file '%s'\nwill be replaced with object from file '%s'\n",
                            foundlib->libname, foundlib->library->libfilename, curlib->libfilename);

                    /* module name is the same, but other attributes need to be replaced */
                    foundlib->library = curlib;             /* point to new library file */
                    foundlib->modulestart = objfile_base;   /* file pointer to library object in new library file */
                }
            }

            currentlibmodule = nextlibmodule;
        } while (nextlibmodule != -1); /* parse all available active modules in library file */

        fclose (libf);  /* library file parsed successfully */
    }
}


static void
ReleaseIndexObj(libobject_t *libidxobj)
{
    if (libidxobj->libname != NULL) {  /* release allocated space for library module name */
        free(libidxobj->libname);      /* library file information is released in ReleaseLibraries() */
    }

    free(libidxobj);
}


static void
ReleaseLibrariesIndex (void)
{
    DeleteAll (&libraryindex, (void (*)(void *)) ReleaseIndexObj);
    libraryindex = NULL;
}


static libfile_t *
NewLibrary (void)
{
    libfile_t *newl = AllocLibFile ();

    if (libraries == NULL) {
        libraries = AllocLibHdr ();
    }

    if (libraries != NULL && newl != NULL) {
        if (libraries->firstlib == NULL) {
            libraries->firstlib = newl;
            libraries->currlib = newl;       /* First library in list */
        } else {
            libraries->currlib->nextlib = newl;      /* current/last library points now at new current */
            libraries->currlib = newl;               /* pointer to current module updated */
        }
    } else {
        /* one or both ressources failed to be allocated */
        if (newl != NULL) {
            free(newl);
            newl = NULL;
        }
    }

    return newl;
}


static libraries_t *
AllocLibHdr (void)
{
    libraries_t *lb = (libraries_t *) malloc (sizeof (libraries_t));

    if (lb != NULL) {
        lb->firstlib = NULL;
        lb->currlib = NULL;   /* library header initialised */
    }

    return lb;
}


static libfile_t *
AllocLibFile (void)
{
    libfile_t *lf = (libfile_t *) malloc (sizeof (libfile_t));

    if (lf != NULL) {
        lf->nextlib = NULL;
        lf->libfilename = NULL;
        lf->libwatermark = NULL;
    }

    return lf;
}


static libobject_t *
AllocLibObject (void)
{
    libobject_t *lo = (libobject_t *) malloc (sizeof (libobject_t));

    if (lo != NULL) {
        lo->libname = NULL;           /* name of library module (the LIB reference name) */
        lo->library = NULL;           /* pointer to library file information */
        lo->modulestart = 0;          /* base pointer of beginning of object module inside library file */
    }

    return lo;
}


/******************************************************************************
                               Public functions
 ******************************************************************************/

/*!
 * \brief Validate Mpm Library File Watermark and return file level
 * \param watermark pointer to first 8 bytes of loaded Mpm library file
 * \return file level of Mpm Library File format or -1 if watermark is unknown
 */
int
LibFileWaterMark(const char *watermark)
{
    return ObjFileCheckWaterMark(watermark, MPMLIBRARYHEADER);
}


/**
 *  Create a library file, containing concatanated object file modules.
 *  The current linked list of modules is scanned and for each module the object file is
 *  loaded and appended to the library file.
 *
 * @brief PopulateLibrary
 * @param libraryfilename
 * @param curmodule
 */
void
PopulateLibrary (char *libraryfilename, const module_t *curmodule)
{
    int codesize;
    int fptr;
    char *filebuffer;
    char *objfname;
    FILE *objf;
    FILE *libfile;

    /* create library file... */
    if ((libfile = fopen (AdjustPlatformFilename(libraryfilename), "w+b")) == NULL) {
        ReportIOError (libraryfilename);
        return;
    } else {
        fwrite (MPMLIBRARYHEADER, sizeof (char), SIZEOF_MPMLIBHDR, libfile);   /* write Mpm library header */
    }

    if (verbose) {
        puts ("Creating library...");
    }

    do {
        objfname = NULL;
        if (curmodule->cfile != NULL)
            objfname = AddFileExtension((const char *) MemfGetFilename(curmodule->cfile->mf), objext);
        if (objfname == NULL) {
            ReportRuntimeErrMsg  (libraryfilename, "Failed to create object filename - insufficient memory");
            break;
        }

        if ((objf = fopen (AdjustPlatformFilename(objfname), "rb")) != NULL) {
            fseek(objf, 0L, SEEK_END);        /* file pointer to end of file */
            codesize = (int) ftell(objf);     /* - to get size... */
            fseek(objf, 0L, SEEK_SET);

            filebuffer = (char *) malloc ((size_t) codesize);
            if (filebuffer == NULL) {
                ReportRuntimeErrMsg  (libraryfilename, "Failed to load object file - insufficient memory");
                fclose (objf);
                free(objfname);
                break;
            }

            if (fread (filebuffer, sizeof (char), (size_t) codesize, objf) != (size_t) codesize) {    /* load object file */
                ReportIOError (objfname);
                fclose (objf);
                free(objfname);
                free(filebuffer);
                break;
            }

            fclose (objf);
            if (ObjFileWaterMark (filebuffer) != -1) {
                if (verbose) {
                    printf ("<%s> module at %04lX.\n", MemfGetFilename(curmodule->cfile->mf), ftell (libfile));
                }

                if (curmodule->nextmodule == NULL) {
                    ObjFileWriteLong (-1, libfile);    /* this is the last module, 0xffffffff */
                } else {
                    fptr = (int) ftell (libfile) + 4 + 4;
                    ObjFileWriteLong (fptr + codesize, libfile);  /* file pointer to next module */
                }

                /* size of this module */
                ObjFileWriteLong (codesize, libfile);
                /* append module to library file */
                fwrite (filebuffer, sizeof (char), (size_t) codesize, libfile);
                free (filebuffer);
            } else {
                ReportRuntimeErrMsg(objfname, GetErrorMessage(Err_Objectfile));
                free (filebuffer);
                free(objfname);
                break;
            }
        } else {
            ReportIOError (objfname);
            free(objfname);
            break;
        }

        if (objfname != NULL) {
            free(objfname);
        }

        curmodule = curmodule->nextmodule;
    } while (curmodule != NULL);

    fclose(libfile);
}


char *
CreateLibfileName (const char *filename)
{
    char *libraryfilename = NULL;

    if (strlen (filename) > 0) {
        libraryfilename = AddFileExtension(filename, libext);
    } else {
        if ((filename = getenv (ENVNAME_STDLIBRARY)) != NULL) {
            libraryfilename = AddFileExtension(filename, libext);
        }
    }

    if (libraryfilename == NULL) {
        ReportRuntimeErrMsg (NULL, "Failed to create library filename - insufficient memory");
    }

    return libraryfilename;
}


void
GetLibfile (const char *filename)
{
    const char *libwatermark;
    char *f = NULL;
    libfile_t *newlib;
    FILE *libf;

    if (strlen (filename) > 0) {
        f = AddFileExtension(filename, libext);
        if (f == NULL) {
            return;
        }
    } else {
        filename = getenv (ENVNAME_STDLIBRARY);
        if (filename != NULL) {
            f = AddFileExtension(filename, libext);
            if (f == NULL) {
                return;
            }
        }
    }

    /* Does library file exist, and is it recognized? */
    if ((libf = OpenLibraryFile(f, gLibraryPath, &libwatermark)) == NULL) {
        free(f); /* discard previously allocated library filename */
        return;
    } else {
        uselibraries = true;
        fclose(libf);
    }

    /* Library file has been recognised, insert it into linked list of libraries */
    if ((newlib = NewLibrary ()) != NULL) {
        newlib->libfilename = f;
        newlib->libwatermark = libwatermark;
    } else {
        ReportError (NULL, Err_Memory);
        free(f); /* discard previously allocated library filename */
    }
}


/**
 *  Open the specified file and evaluate that it is an Z80asm or Mpm generated
 *  library file. If successfully validated, the opened file handle is returned to the
 *  caller, and a pointer to the library file watermark type string is returned.
 *  The file pointer has been positioned at the first byte after the watermark
 *  (it points at the first byte of 'next object module file pointer').
 *
 *  If the library file couldn't be opened or is not recognized,
 *  a NULL file handle and NULL watermark is returned.
 *  The routine also reports errors to the global error system for file I/O and
 *  unrecognized library file.
 *
 * @brief OpenLibraryFile
 * @param filename
 * @param pathlist
 * @param libversion
 * @return
 */
FILE *
OpenLibraryFile(char *filename, const pathlist_t *pathlist, const char **libversion)
{
    FILE *libf;
    char watermark[64];
    *libversion = NULL;

    if ((libf = OpenFile (filename, pathlist, false)) == NULL) {
        ReportIOError (filename);
        return NULL;
    } else {
        if (fread (watermark, SIZEOF_MPMLIBHDR, 1U, libf) == 1U) {  /* try to read Mpm library file watermark */
            watermark[SIZEOF_MPMLIBHDR] = '\0';

            if (strcmp (watermark, MPMLIBRARYHEADER) == 0) {
                *libversion = MPMLIBRARYHEADER;                /* found Mpm library file */
                return libf;
            }
        }
    }

    /* library file was not recognized */
    ReportRuntimeErrMsg (filename, GetErrorMessage(Err_Libfile));

    fclose (libf);
    return NULL;
}


void
IndexLibraries(void)
{
    libfile_t *curlib;

    if (libraries != NULL) {
        curlib = libraries->firstlib;

        if (verbose) {
            puts ("Indexing libraries...");
        }

        do {
            LoadLibraryIndex(&libraryindex, curlib);
            curlib = curlib->nextlib;
        } while(curlib != NULL);     /* until all library files are parsed */
    }
}


void
ReleaseLibraries (void)
{
    libfile_t *tmpptr;
    libfile_t *curptr;

    if (libraries == NULL)
        return;

    curptr = libraries->firstlib;
    /* until all libraries are released */
    while (curptr != NULL) {
        if (curptr->libfilename != NULL) {
            free (curptr->libfilename);
        }

        tmpptr = curptr;
        curptr = curptr->nextlib;
        free (tmpptr);            /* release library */
    }

    free (libraries);             /* Release library header */
    libraries = NULL;

    ReleaseLibrariesIndex ();
}


/**
   Find reference to this LIB module name in library index and append it to the linked
   list of application modules. Then, parse linked LIB module for own LIB references
   and link them too (recursive).

 * @brief SearchLibraries
 * @param modname
 * @return
            0, successfully found and linked this LIB module into the modules list.
            Err_LibReference, if LIB reference was not found.
            Err_Memory, if memory was not available for allocation of LIB module objects.
            Err_MaxCodeSize, if code buffer max was reached during linking of LIB modules.
 */
int
SearchLibraries (const char *modname)
{
    const libobject_t *foundlib = FindLibModule (modname);    /* search for library module name in library index */

    if (foundlib != NULL) {
        return LinkLibModule (foundlib->library, foundlib->modulestart, modname);
    } else {
        return Err_LibReference;
    }
}


/*!
 * \brief
 * For each LIB name reference in this object file, look it up in the library index
 * and automatically append it to the linked list of modules.
 *
 * \param filename      name of library file
 * \param fptr_base     base file pointer of object (library) module file
 * \param nextname      relative file pointer to start of LIB name section of object file
 * \param endnames      relative file pointer to end of LIB name section of object file
 * \return
 */
int
LinkObjFileLibModules (char *filename, const long fptr_base, long nextname, const long endnames)
{
    long mnl;
    char modname[MAX_NAME_SIZE];
    FILE *objf;

    do {
        objf = OpenFile (filename, gLibraryPath, false); /* open object file for reading */
        fseek (objf, fptr_base + nextname, SEEK_SET);    /* set file pointer to point at library name declarations */
        ObjFileReadString (objf, modname);               /* read library reference name */
        fclose (objf);

        mnl = (long) strlen (modname);
        nextname += (1 + mnl);        /* remember module pointer to next name in this object module */

        if (FindSymbol (modname, globalsymbols) == NULL && SearchLibraries (modname) != 0) {
            printf("Warning: LIB reference '%s' was not found in libraries.\n", modname);
        }
    } while (nextname < endnames);

    return 0;
}
