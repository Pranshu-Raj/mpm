/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "objfile.h"
#include "options.h"
#include "avltree.h"
#include "symtables.h"
#include "sourcefile.h"
#include "modules.h"
#include "memfile.h"
#include "errors.h"


/******************************************************************************
                          Global variables
 ******************************************************************************/
FILE *objfile;

/******************************************************************************
                          Local functions and variables
 ******************************************************************************/

static void StoreLibReference (const symbol_t *node);
static void ObjFileWriteGlobalName (const symbol_t *node);
static void ObjFileWriteLocalName (const symbol_t *node);
static void ObjFileWriteName (const symbol_t *node, const unsigned long symscope);
static void ObjFileInstallSymbol(avltree_t **symbols, const char *symname, symvalue_t symval, unsigned long symtype, module_t *symowner);

static void
StoreLibReference (const symbol_t *node)
{
    size_t b;

    if (node == NULL)
        return;

    if ((node->type & SYMXREF) && (node->type & SYMDEF) && (node->type & SYMTOUCHED)) {
        b = strlen (node->symname);
        fputc ((int) b, objfile);                                 /* write length of symbol name to relocatable file */
        fwrite (node->symname, sizeof (char), b, objfile);        /* write symbol name to relocatable file */
    }
}


static void
ObjFileWriteGlobalName (const symbol_t * node)
{
    if (node == NULL) {
        return;
    } else {
        if ((node->type & SYMXDEF) && (node->type & SYMTOUCHED)) {
            ObjFileWriteName (node, SYMXDEF);
        }
    }
}


static void
ObjFileWriteLocalName (const symbol_t * node)
{
    if (node == NULL) {
        return;
    } else {
        if ((node->type & SYMLOCAL) && (node->type & SYMTOUCHED)) {
            ObjFileWriteName (node, SYMLOCAL);
        }
    }
}


static void
ObjFileWriteName (const symbol_t *node, const unsigned long scope)
{
    int b;

    if (node == NULL)
        return;

    if (scope == SYMLOCAL) {
        fputc ('L', objfile);
    }

    if (scope == SYMXDEF) {
        if (node->type & SYMDEF) {
            fputc ('X', objfile);
        } else {
            fputc ('G', objfile);
        }
    }

    if (node->type & SYMADDR) {     /* then write type of symbol */
        fputc ('A', objfile);       /* either a relocatable 32bit address */
    } else {
        fputc ('C', objfile);       /* or a 32bit constant */
    }
    ObjFileWriteLong (node->symvalue, objfile);

    b = (int) strlen (node->symname);
    fputc (b, objfile);             /* write length of symbol name to relocatable file */
    fwrite (node->symname, sizeof (char), (size_t) b, objfile);   /* write symbol name to relocatable file */
}


static void
ObjFileInstallSymbol(avltree_t **symbols, const char *symname, symvalue_t symval, unsigned long symtype, module_t *symowner)
{
    symbol_t *foundsymbol = FindSymbol (symname, *symbols);

    if (foundsymbol == NULL) {
        foundsymbol = CreateSymbol (symname, symval, symtype, symowner);
        if (foundsymbol != NULL && !Insert (symbols, foundsymbol, (compfunc_t) cmpidstr)) {
            FreeSym(foundsymbol);
            ReportError (NULL, Err_Memory);
        }
    } else {
        foundsymbol->symvalue = symval;
        foundsymbol->type |= symtype;
        foundsymbol->owner = symowner;
        printf ("Symbol <%s> redefined in module '%s'\n", symname, symowner->mname);
    }
}


/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Check Mpm Object or Library File Watermark and return file level
 * \param string pointer to first 8 bytes of loaded Mpm object or library file
 * \param watermark pointer to Mpm default watermark
 * \return file level of Mpm Object/library File format or -1 if watermark is unknown
 */
int
ObjFileCheckWaterMark(const char *string, const char *watermark)
{
    int filelevel = -1;
    char str[3];

    if (memcmp (string, watermark, SIZEOF_MPMOBJHDR-2) == 0) {
        memcpy(str, watermark+SIZEOF_MPMOBJHDR-2, 2);
        filelevel = atoi(str);
    }

    return filelevel;
}


/*!
 * \brief Validate Mpm Object File Watermark and return file level
 * \param watermark pointer to first 8 bytes of loaded Mpm object file
 * \return file level of Mpm Object File format or -1 if watermark is unknown
 */
int
ObjFileWaterMark(const char *watermark)
{
    return ObjFileCheckWaterMark(watermark, MPMOBJECTHEADER);
}


FILE *
ObjFileCreate(char *objfname)
{
    const char objhdrprefix[] = "oooomodnexprnamelibnmodc";  /* template of pointers to sections of OBJ file */
    FILE *objf = fopen (AdjustPlatformFilename(objfname), "w+b");

    /* Create relocatable object file with watermark and header */
    if (objf != NULL) {
        fwrite (MPMOBJECTHEADER, sizeof (char), strlen (MPMOBJECTHEADER), objf);
        fwrite (objhdrprefix, sizeof (char), strlen (objhdrprefix), objf);
    } else {
        ReportIOError (objfname);
    }

    return objf;
}


/*!
 * \brief Open file and evaluate if it is an Mpm Object File
 * \details
 * If successfully validated, the opened file handle is returned to the
   caller, and a pointer to the object file watermark type string is returned.
   The file pointer has been positioned at the first byte after the watermark
   (it points at the object module ORIGIN).

   If the object file couldn't be opened or is not recognized,
   a NULL file handle and NULL watermark is returned.
   The routine also reports errors to the global error system for file I/O and
   unrecognized object file.
 * \param filename
 * \param objversion
 * \return
 */
FILE *
ObjFileOpen(char *filename, const char **objversion)
{
    FILE *objf;
    char watermark[SIZEOF_MPMOBJHDR+1];

    if ((objf = fopen (AdjustPlatformFilename(filename), "rb")) == NULL) {
        ReportIOError (filename);
        *objversion = NULL;
        return NULL;
    } else {
        /* try to read Mpm object file watermark */
        if (fread (watermark, SIZEOF_MPMOBJHDR, 1U, objf) == 1U) {
            watermark[SIZEOF_MPMOBJHDR] = '\0';

            if (ObjFileWaterMark(watermark) != -1) {
                /* found Mpm object file matching default release version */
                *objversion = MPMOBJECTHEADER;
                return objf;
            }
        }
    }

    /* object file was not recognized */
    ReportRuntimeErrMsg (filename, GetErrorMessage(Err_Objectfile));
    fclose (objf);
    *objversion = NULL;
    return NULL;
}


/*!
 * \brief Fetch a string at current file position in the object file
 * \details format is <length><stringsequence>. The string is truncated at MAX_NAME_SIZE length (see config.h).
 * \param objf read-opened stream to object file
 * \param[out] string pointer to string buffer (MAX_NAME_SIZE length)
 */
void
ObjFileReadString (FILE *objf, char *const string)
{
    int strlength = fgetc (objf);

    if (strlength > MAX_NAME_SIZE) {
        strlength = MAX_NAME_SIZE;    /* truncate name to avoid buffer overflow */
    }

    strlength = (int) fread (string, sizeof (char), (size_t) strlength, objf);   /* read name */
    string[strlength] = '\0';
}


/*!
 * \brief read signed 32bit integer in LSB format from object file at current file pointer
 * \param objf the object file handle
 * \return
 */
long
ObjFileReadLong (FILE * objf)
{
    int fptr = 0;

    if (BIGENDIAN == true) {
        /* load integer as LSB order into MSB order internally */
        for (int i = 1; i <= 3; i++) {
            fptr |= fgetc (objf) << 24;
            fptr >>= 8;
        }
        fptr |= fgetc (objf) << 24;
    } else {
        /* default host architecture is LSB order... */
        fread (&fptr, 4, 1, objf);
    }

    return (long) fptr;
}


/*!
 * \brief Write signed 32bit integer to object file in LSB format
 * \param int32
 * \param fileid
 */
void
ObjFileWriteLong (long int32, FILE *objf)
{
    if (BIGENDIAN == true) {
        for (int i = 0; i < 4; i++) {
            fputc (int32 & 255, objf);
            int32 >>= 8;
        }
    } else {
        /* low byte, high byte order... */
        fwrite (&int32, 4, 1, objf);
    }
}


/*!
 * \brief Store expression (infix notation) to object file
 * \details
 * The Expression contains symbols declared as external or defined
 * as a relocatable address.
 * \param pfixexpr
 * \param exprtype the result type of the expression (for range check)
 */
void
ObjFileWriteExpr (expression_t *pfixexpr, const unsigned char exprtype)
{
    unsigned char b;

    if (writeobjectfile == true && pfixexpr->wrobjfile == false) {
        /* this expression is only written once to object file */
        pfixexpr->wrobjfile = true;

        /* type of expression */
        fputc (exprtype, objfile);
        /* write patch pointer inside code module */
        ObjFileWriteLong ( (long) pfixexpr->codepos, objfile);

        /* length prefixed string */
        b = (unsigned char) strlen (pfixexpr->infixexpr);
        fputc (b, objfile);
        fwrite (pfixexpr->infixexpr, sizeof (b), (size_t) b, objfile);

        /* null-terminate expression */
        fputc (0, objfile);
    }
}


/*!
 * \brief Load all symbols from object file (or library) into module local or global symbols
 * \details Object file is already opened and points to first symbol name definition
 * \param objf opened stream to object file
 * \param orgaddr the ORG (absolute base address) of the final output binary
 * \param nextname object file file pointer to symbol name section (first symbol name)
 * \param endnames object file file pointer to end of symbol name section (last symbol name)
 */
void
ObjFileReadSymbols (module_t *curmodule, FILE *objf, size_t orgaddr, long nextname, long endnames)
{
    char scope;
    char symid;
    char symname[MAX_NAME_SIZE];
    unsigned long symtype = SYMDEFINED;
    symvalue_t value;
    symvalue_t symval;

    do {
        scope = (char) fgetc (objf);
        symid = (char) fgetc (objf);     /* type of name */
        value = ObjFileReadLong (objf);
        ObjFileReadString (objf,symname);
        nextname += 1 + 1 + 4 + 1 + strlen (symname);

        switch (symid) {
            case 'A':
                symtype = SYMADDR | SYMDEFINED;
                value += orgaddr + curmodule->startoffset;   /* Absolute address */
                break;

            case 'C':
                symtype = SYMDEFINED;
                break;

            default:
                ReportError (NULL, Err_UnknownIdent);
        }

        symval = value;

        switch (scope) {
            case 'L':
                ObjFileInstallSymbol(&curmodule->localsymbols, symname, symval, symtype | SYMLOCAL, curmodule);
                break;

            case 'G':
                ObjFileInstallSymbol(&globalsymbols, symname, symval, symtype | SYMXDEF, curmodule);
                break;

            case 'X':
                ObjFileInstallSymbol(&globalsymbols, symname, symval, symtype | SYMXDEF | SYMDEF, curmodule);
                break;

            default:
                ReportError (NULL, Err_UnknownIdent);
        }
    } while (nextname < endnames);
}


/**
 * @brief Load expression from current object file pointer
 * @param cfile for error reporting
 * @param objf the file handle of the open object file
 * @param type return expression type (eg. 'U', 'J'...)
 * @param patchcodeidx return the relative patch code offset where expression result is updated
 * @param nextexpr return file pointer to next object file expression
 * @return alloced memory file (string) containing the expression infix expression, or NULL
 */
memfile_t *
ObjFileLoadExpr(sourcefile_t *cfile, FILE *objf, unsigned char *type, size_t *patchcodeidx, long *nextexpr)
{
    int        i;
    memfile_t  *mf;

    *type = (unsigned char) fgetc (objf);
    *patchcodeidx = (size_t) ObjFileReadLong (objf);

    /* get length of infix expression + terminator */
    i = fgetc (objf) + 1;
    /* load infix expression into file data for parsing */
    if ( (mf = MemfNew(NULL, 0, 0)) == NULL) {
        ReportError (cfile, Err_Memory);
        return NULL;
    }

    if (MemfCacheHostFileStream (mf, objf, (size_t) i) == NULL) {
        ReportError (cfile, Err_FileIO);
        MemfFree(mf);
        return NULL;
    }

    *nextexpr += 1 + 4 + 1 + i;

    return mf;
}


/**
 * @brief Validate range of expression according to object file type
 * @details If the range is not valid or unknown, report a global error, according to current source file
 * @param csfile
 * @param exprvalue
 * @param exprtype
 */
bool
ObjFileExpressionRange(sourcefile_t *csfile, long exprvalue, unsigned char exprtype)
{
    int defRangeError = Err_IntegerRange;

    switch(exprtype) {
        case RANGE_8SIGN:
        case RANGE_PCREL8:
            if ( (exprvalue >= -128) && (exprvalue <= 127) ) {
                return true;
            }

            if (exprtype == RANGE_PCREL8) {
                defRangeError = Err_PcRelRange;
            }
            break;

        case RANGE_8UNSIGN:
            if ( (exprvalue >= -128) && (exprvalue <= 255) ) {
                return true;
            }
            break;

        case RANGE_LSB_16SIGN:
        case RANGE_MSB_16SIGN:
        case RANGE_LSB_PCREL16:
        case RANGE_MSB_PCREL16:
            if ( (exprvalue >= -32768 ) && (exprvalue <= 32767) ) {
                return true;
            }

            if (exprtype == RANGE_LSB_PCREL16 || exprtype == RANGE_MSB_PCREL16) {
                defRangeError = Err_PcRelRange;
            }
            break;

        case RANGE_LSB_16CONST:
        case RANGE_MSB_16CONST:
            if ( (exprvalue >= -32768) && (exprvalue <= 65535) ) {
                return true;
            }
            break;

        case RANGE_LSB_PCREL24:
        case RANGE_MSB_PCREL24:
        case RANGE_LSB_24SIGN:
        case RANGE_MSB_24SIGN:
            if ( (exprvalue >= -8388608 ) && (exprvalue <= 8388607) ) {
                return true;
            }

            if (exprtype == RANGE_LSB_24SIGN || exprtype == RANGE_MSB_24SIGN) {
                defRangeError = Err_PcRelRange;
            }
            break;

        case RANGE_LSB_32SIGN:
        case RANGE_MSB_32SIGN:
            if ( exprvalue >= -2147483648 && exprvalue <= 2147483647 ) {
                return true;
            }
            break;

        case RANGE_LSB_32CONST:
        case RANGE_MSB_32CONST:
            if (exprvalue < 0) {
                if (exprvalue >= -2147483648) {
                    return true;
                }
            } else {
                if ( (unsigned long) exprvalue <= 4294967295 ) {
                    return true;
                }
            }
            break;

        default:
            ReportSrcAsmMessage (csfile, "Unknown Expression Range Type");
            return false;
    }

    /* according to type, expression was outside valid range */
    ReportError (csfile, defRangeError);
    return false;
}


void
ObjFileWriteHeader(sourcefile_t *csfile, FILE *objf, size_t moduleCodesize)
{
    int modnamelen;
    long fptr_exprdecl;
    long fptr_namedecl;
    long fptr_modname;
    long fptr_modcode;
    long fptr_libnmdecl;

    if (csfile != NULL && csfile->module != NULL && objf != NULL) {
        /* Write parsed data structures to object file */
        fptr_namedecl = ftell (objf);

        /* Store Local Name declarations to relocatable file */
        InOrder (csfile->module->localsymbols, (void (*)(void *)) ObjFileWriteLocalName);

        /* Store Global name declarations to relocatable file */
        InOrder (globalsymbols, (void (*)(void *)) ObjFileWriteGlobalName);

        /* Store library reference name declarations to relocatable file */
        fptr_libnmdecl = ftell (objf);
        InOrder (globalsymbols, (void (*)(void *)) StoreLibReference);

        fptr_modname = ftell (objf);
        if (csfile->module->mname != NULL) {
            modnamelen = (int) strlen (csfile->module->mname);

            /* write length of module name to relocatable file */
            fputc (modnamelen, objf);
            /* write module name to relocatable file */
            fwrite (csfile->module->mname, sizeof (char), (size_t) modnamelen, objf);
        } else {
            /* no module name defined */
            fputc (0, objf);
        }

        if (moduleCodesize == 0) {
            fptr_modcode = -1;    /* no code generated! (0xffffffff) */
        } else {
            fptr_modcode = ftell (objf);
            /* write module code size (32bit) */
            ObjFileWriteLong ((long) moduleCodesize, objf);
            /* then the actual binary code */
            fwrite (csfile->module->csection->area, sizeof (char), moduleCodesize, objf);
        }

        fseek (objf, SIZEOF_MPMOBJHDR, SEEK_SET);  /* set file pointer to point at ORG (just after watermark) */
        if ((modules->first == csfile->module) && (deforigin == true)) {
            csfile->module->origin = EXPLICIT_ORIGIN;      /* use origin from command line */
        }
        ObjFileWriteLong ((long) csfile->module->origin, objf);    /* Write Origin (32bit) */

        fptr_exprdecl = SIZEOF_MPMOBJHDR + 4+4+4+4+4+4;       /* distance to expression section... */

        if (fptr_namedecl == fptr_exprdecl) {
            fptr_exprdecl = -1;    /* no expressions (0xffffffff) */
        }
        if (fptr_libnmdecl == fptr_namedecl) {
            fptr_namedecl = -1;    /* no name declarations (0xffffffff) */
        }
        if (fptr_modname == fptr_libnmdecl) {
            fptr_libnmdecl = -1;    /* no library reference declarations (0xffffffff) */
        }

        ObjFileWriteLong (fptr_modname, objf);    /* write fptr. to module name */
        ObjFileWriteLong (fptr_exprdecl, objf);   /* write fptr. to name declarations */
        ObjFileWriteLong (fptr_namedecl, objf);   /* write fptr. to name declarations */
        ObjFileWriteLong (fptr_libnmdecl, objf);  /* write fptr. to library name declarations */
        ObjFileWriteLong (fptr_modcode, objf);    /* write fptr. to module code */
    }
}
