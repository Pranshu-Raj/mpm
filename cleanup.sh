#!/bin/bash

# *********************************************************************************************
#
#  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
#   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
#   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
#   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
#
#  Cleanup script of build-generated files
#  Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
#
#  This file is part of Mpm.
#  Mpm is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by the Free Software Foundation;
#  either version 2, or (at your option) any later version.
#  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License along with Mpm;
#  see the file COPYING.  If not, write to the
#  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# **********************************************************************************************/


# delete all auto-generated make files, if generated
rm -f Makefile src/Makefile

# delete qmake-related auto-generated files
rm -f mpm.pro.user* .qmake.stash

# delete the build/ and generated folders that are created via Qmake's Makefile
rm -fR build

# delete any listing files generated during testing
rm -f asmtests/*.lst

# remove and previous code coverage output
rm -f src/*.gcno
rm -f src/*.gcda
rm -f src/*.gcov
