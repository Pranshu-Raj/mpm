:: *************************************************************************************
::
::  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
::   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
::   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
::   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
::   MMMM       MMMM     PPPP              MMMM       MMMM
::   MMMM       MMMM     PPPP              MMMM       MMMM
::  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
::
:: Windows compile script for Mpm
:: (C) Gunther Strube (hello@bits4fun.net) 2012-2020
::
:: Requirements:
:: 1) QMake and QtCreator (via qt.io), PATH for Qmake
::    or
:: 2) Mingw32 compiler tools must have been installed
:: 3) PATH for Mingw32 compiler tools
::
::  This file is part of Mpm.
::  Mpm is free software; you can redistribute it and/or modify
::  it under the terms of the GNU General Public License as published by the Free Software Foundation;
::  either version 2, or (at your option) any later version.
::  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
::  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
::  See the GNU General Public License for more details.
::  You should have received a copy of the GNU General Public License along with Mpm;
::  see the file COPYING.  If not, write to the
::  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
::
:: *************************************************************************************

@echo off

:: --------------------------------------------------------------------------
:: remember the current path in from where this script is called
set MPM_PATH=%cd%

:: delete any previous compile output
call cleanup.bat

:: try to locate git.exe...
set gitfound=0
for %%x in (git.exe) do if not [%%~$PATH:x]==[] set gitfound=1
if "%gitfound%"=="0" goto GIT_NOT_AVAILABLE

:: output current Git revision as a 32bit hex string, to be included as part of Mpm release information
set revision_file=%cd%\src\build.h
git.exe log  -n 1 --pretty=format:"#define MPM_BUILDMSG \"%%h\"%%n" > %revision_file%

:: try to locate qmake.exe...
set qmakefound=0
for %%x in (qmake.exe) do if not [%%~$PATH:x]==[] set qmakefound=1
if "%qmakefound%"=="0" goto QMAKE_NOT_AVAILABLE

:: try to locate mingw32-make.exe...
set mingw32found=0
for %%x in (mingw32-make.exe) do if not [%%~$PATH:x]==[] set mingw32found=1
if "%mingw32found%"=="0" goto MINGW32MAKE_NOT_AVAILABLE

:: try to locate g++.exe used by Qmake...
set gcpfound=0
for %%x in (g++.exe) do if not [%%~$PATH:x]==[] set gcpfound=1
if "%gcpfound%"=="0" goto MINGW32_COMPILER_NOT_AVAILABLE

qmake -config release mpm.pro
if ERRORLEVEL 0 goto CHECK_MAKEFILE
echo qmake failed generating makefiles!
goto END

:CHECK_MAKEFILE
if exist "%MPM_PATH%\Makefile" goto COMPILE_MPM
echo "%MPM_PATH%\Makefile was not created by qmake!"
goto END

:COMPILE_MPM
mingw32-make
if ERRORLEVEL 0 goto MPM_COMPILED
echo make failed compiling mpm sources!
goto END

:MPM_COMPILED
echo mpm compiled successfully on your system.
DIR build\mpm.exe
goto END

:QMAKE_NOT_AVAILABLE
:: Qt's Qmake was not found, try to use Mingw stand-alone compiler
set gccfound=0
for %%x in (gcc.exe) do if not [%%~$PATH:x]==[] set gccfound=1
if "%gccfound%"=="0" goto MINGW32_COMPILER_NOT_AVAILABLE
COPY makefile.gcc.windows Makefile
goto COMPILE_MPM

:GIT_NOT_AVAILABLE
echo git.exe utility could not be located!
echo (PATH env. variable doesn't point to git tool)
goto END

:MINGW32MAKE_NOT_AVAILABLE
echo mingw32-make.exe utility could not be located!
echo (PATH env. variable doesn't point to Mingw32 compiler tools and libs)
goto END

:MINGW32_COMPILER_NOT_AVAILABLE
echo gcc Mingw compiler could not be located!
echo (PATH env. variable doesn't point to Mingw compiler tools and libs)
goto END

:END
@echo on
