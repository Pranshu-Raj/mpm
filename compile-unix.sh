#!/bin/bash

# *************************************************************************************
#
#  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
#   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
#   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
#   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
#
#  Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
#
#  This file is part of Mpm.
#  Mpm is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by the Free Software Foundation;
#  either version 2, or (at your option) any later version.
#  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License along with Mpm;
#  see the file COPYING.  If not, write to the
#  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# ---------------------------------------------------------------------------------------
#
# Compile script for Mpm on any Unix platform that supports make and a gcc-compatible compiler.
#
# Requirements:
#   git (for adding build string when using mpm -h)
#   qmake or make and gcc-compatible compiler
#   available in PATH
#
# *************************************************************************************


# get the relative path to this script
MPM_PATH=$PWD

command -v git >/dev/null 2>&1 || { echo "git utility was not found on system!" >&2; exit 1; }

# output current Git revision as a 32bit hex string, to be included as part of Ozvm release information
git log --pretty=format:'#define MPM_BUILDMSG "%h"%n' -n 1 > src/build.h

QMAKE_FOUND=1
QMAKE4_FOUND=1
QMAKE5_FOUND=1
command -v qmake >/dev/null 2>&1 || { QMAKE_FOUND=0; }
command -v qmake-qt4 >/dev/null 2>&1 || { QMAKE4_FOUND=0; }
command -v qmake-qt5 >/dev/null 2>&1 || { QMAKE5_FOUND=0; }

if [ $QMAKE_FOUND -eq 1 ]; then
    QMAKE_TOOL=qmake
fi
if [ $QMAKE4_FOUND -eq 1 ]; then
    QMAKE_TOOL=qmake-qt4
fi
if [ $QMAKE5_FOUND -eq 1 ]; then
    QMAKE_TOOL=qmake-qt5
fi

CC_FOUND=1
GCC_FOUND=1
command -v make >/dev/null 2>&1 || { echo "make utility was not found on system!" >&2; exit 1; }
command -v cc >/dev/null 2>&1 || { CC_FOUND=0; }
command -v gcc >/dev/null 2>&1 || { GCC_FOUND=0; }

if [ $CC_FOUND -eq 0 ] && [ $GCC_FOUND -eq 0 ]; then
    echo "C compiler was not found on system!" >&2;
    exit 1
fi

# delete any previous compile output
./cleanup.sh

# if Qmake was found, let it generate a Makefile
if [ $QMAKE_FOUND -eq 1 ] || [ $QMAKE4_FOUND -eq 1 ] || [ $QMAKE5_FOUND -eq 1 ]; then
    $QMAKE_TOOL -config release mpm.pro
    if test $? -gt 0; then
        echo "qmake failed generating makefile - fallback to generic Unix Makefile.."
        cp -f makefile.gcc.unix Makefile
    else
        if [ ! -f $MPM_PATH/Makefile ]; then
            echo "$MPM_PATH/Makefile was not created by qmake - fallback to generic Unix Makefile.."
            cp -f makefile.gcc.unix Makefile
	fi
    fi
else
    # fall-back on generic Unix makefile.gcc.unix
    cp -f makefile.gcc.unix Makefile
fi

# compile Mpm sources...
make -f Makefile
if test $? -gt 0; then
	echo "make failed compiling Mpm sources!"
else
	echo "Mpm compiled successfully on your system."
	ls -lh build/mpm
fi
