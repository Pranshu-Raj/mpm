#!/bin/bash

# *************************************************************************************
#
#  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
#   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
#   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
#   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
#
#  Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
#
#  This file is part of Mpm.
#  Mpm is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by the Free Software Foundation;
#  either version 2, or (at your option) any later version.
#  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License along with Mpm;
#  see the file COPYING.  If not, write to the
#  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# ---------------------------------------------------------------------------------------
#
# Build script to generate Ubuntu/Debian source + binary package for Mpm.
#
# Build requirements (install these packages before running this compile script):
# a) apt-get install build-essential dpkg-dev devscripts dh-make qt4-qmake
# b) import GPG keys (optional):
#	gpg --import mygpgkey_pub.asc
#	gpg --allow-secret-key-import --import mygpgkey-secret.asc
#
# *************************************************************************************

# also remember to change version in debian/changelog
MPM_PACKAGEVERSION=1.5~ppa0

# get the relative path to this script
MPM_PATH=$PWD

command -v debuild >/dev/null 2>&1 || { echo "debuild utility is not available on system!" >&2; exit 1; }
command -v dpkg-buildpackage >/dev/null 2>&1 || { echo "dpkg-buildpackage utility is not available on system!" >&2; exit 1; }
command -v tar >/dev/null 2>&1 || { echo "tar utility is not available on system!" >&2; exit 1; }
command -v qmake-qt4 >/dev/null 2>&1 || { echo "qmake utility is not available on system!" >&2; exit 1; }
command -v make >/dev/null 2>&1 || { echo "make utility is not available on system!" >&2; exit 1; }
command -v gcc >/dev/null 2>&1 || { echo "GNU C compiler is not available on system!" >&2; exit 1; }

# delete any previous build output, before generating debian source + binary packages..
rm -fR mpm-*
rm -f mpm_*
rm -f *.o
rm -fR bin
rm -fR build
rm -fR Makefile
rm -f mpm.pro.user
rm -fR *~

# generate original package, which is used to build package
tar --exclude ".*" -czf mpm_$MPM_PACKAGEVERSION.orig.tar.gz *
if test $? -gt 0; then
	echo "Unable to create mpm_$MPM_PACKAGEVERSION.orig.tar.gz archive. build-script aborted."
	exit 1
fi

mkdir mpm-$MPM_PACKAGEVERSION
if test $? -gt 0; then
	echo "Unable to create mpm-$MPM_PACKAGEVERSION folder. build-script aborted."
	exit 1
fi

# extract archive into build folder
tar xzf mpm_$MPM_PACKAGEVERSION.orig.tar.gz -C mpm-$MPM_PACKAGEVERSION
cd mpm-$MPM_PACKAGEVERSION

# build the source debian package...
debuild -i -I -S -sa

if test $? -eq 0; then
	echo "DEB source package built successfully."
fi

# build the debian architecture-specific installer package
dpkg-buildpackage -rfakeroot -us -uc -b

if test $? -eq 0; then
	echo "DEB installer package built successfully."
fi

cd $MPM_PATH
