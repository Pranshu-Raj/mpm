### Git repository for [M]ultiple [P]rocessor [M]odule Assembler

```
   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
    MMMM       MMMM     PPPP              MMMM       MMMM
    MMMM       MMMM     PPPP              MMMM       MMMM
   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
```
Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net


### Introduction

The [M]ultiple [P]rocessor [M]odule Assembler is a cross-platform command line tool, executing on any desktop OS, supporting compilation of Zilog Z80/Z180/Z380 CPU assembly source code

Mpm is an assembler and linker combined as one tool. Mpm is available on all major operating system platforms such as Windows, Mac OS X and Linux, also supporting embedded platforms such as the Raspberry Pi. Mpm is implemented in Ansi-C supporting executables on 32bit and 64bit architectures. It is designed to be a multi-platform tool, automatically handling file naming conventions and correct code generation,
byte ordering, no matter which architecture it is being executed on. Mpm is designed as an easy command line interface compiling source files into stand-alone applications.

Mpm assists the assembly language programmer with high-level features such as

* modularized source code,
* user-defined macros
* support for meta data structures,
* source file-level dependencies (include files)
* static library management
* and complete code generation

### Downloading pre-made executable for your desktop operating system

For convenience, ready-made Mpm executable is available to be downloaded & installation for Mac OS X, Linux or Windows in Project Releases:
https://gitlab.com/b4works/mpm/-/releases

Follow the ReadMe description in the Zip archive on how to install it on your system. Basically, ensure that the tool is available on the operating system PATH, so it can be accessible everywhere on your command shell.

### Compiling Mpm
Mpm is developed in the C programming language.

On Linux and Mac OS X, the GCC compiler is easily installed on the accompanying CD's (or from a download repository). For Windows, you need to download and install a free C compiler.

Use your favorite C compiler on your platform with supplied make files. Start a command shell and change directory to your check-out location. Then one of the following:
```
mingw32-make.exe -f makefile.gcc.windows [using MinGW or Cygwin GCC on Windows]
make -f makefile.gcc.unix [using GCC on GCC/Linux/Mac OS X/Unix]
qmake mpm.pro; make [using Qt/Qt-Creator installed on Windows/Mac/Linux]
```

Automated build scripts are also available that will try either Qmake or normal GCC compilation.

For Linux/BSD/Mac OSX, use
```
chmod +x compile-unix.sh
./compile-unix.sh
```

For Windows (32 or 64bit), use
```
compile-windows.bat
```

### Documentation
All documentation about Mpm is available online: https://gitlab.com/b4works/mpm/wikis/home

## [![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=bits4fun_mpm)
We use the free service provided by SonarCloud, getting updated status of the code, when committing changes. Click the image above to see a detailed status report of Mpm source code.


