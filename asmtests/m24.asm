; ***************************************************************************************************
; Mpm test library routine, compiled as part of
;
;       mpm -d -xtest.lib du16 m16 m24
;

xlib m24

; ***************************************************************************************************
;
; 24-bit unsigned multiplication
; (derived from GN_M24 / OZ, https://bitbucket.org/cambridge/oz, GPL v2)
;
; IN:
;    CDE = multiplier
;    BHL = multiplicant
;
; OUT:
;    BHL = product
;
; Registers changed after return:
;    ...CDE../IXIY  same
;    AFB...HL/....  different
;
.m24
                    push de
                    ex   de, hl                 ; BDE=BHL(in)
                    xor  a                      ; AHL=0
                    ld   h, a
                    ld   l, a
                    ex   af, af'                ;       alt
                    ld   a, c                   ; ade=CDE(in)
                    exx                         ;       alt
                    pop  de
                    ld   b, 23
.m24_1
                    sla  e                      ; ade << 1
                    rl   d
                    rl   a
                    exx                         ;       main
                    jr   nc, m24_2              ; bit set? add total
                    ex   af, af'                ;       main
                    add  hl, de                 ; AHL=AHL+BHL(in)
                    adc  a, b
                    ex   af, af'                ;       alt
.m24_2
                    ex   af, af'                ;       main
                    add  hl, hl                 ; AHL=AHL<<1
                    adc  a, a
                    exx                         ;       alt
                    ex   af, af'                ;       alt
                    djnz m24_1
                    exx                         ;       main
                    rlca                        ; last bit
                    jr   nc, m24_3
                    ex   af, af'                ;       main
                    add  hl, de                 ; AHL += BHL(in)
                    adc  a, b
                    ex   af, af'                ;       alt
.m24_3
                    ex   af, af'                ;       main
                    ld   b, a
                    ret
