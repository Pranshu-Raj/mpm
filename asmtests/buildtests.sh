#!/bin/bash

# *************************************************************************************
#
#  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
#   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
#   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
#   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
#
#  Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
#
#  This file is part of Mpm.
#  Mpm is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by the Free Software Foundation;
#  either version 2, or (at your option) any later version.
#  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License along with Mpm;
#  see the file COPYING.  If not, write to the
#  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# ---------------------------------------------------------------------------------------
#
# Script that compiles all Mpm test source code.
#
# Requirements (available in PATH):
#   mpm
#
# *************************************************************************************


# get the relative path to this script
MPM_PATH=$PWD

command -v mpm >/dev/null 2>&1 || { echo "mpm was not found on system!" >&2; exit 1; }

# compile tests for all available CPU's
mpm -mz80 -ab -crc32 z80.asm
mpm -mz180 -ab -oz180.bin -ihex z80.asm z180.asm
mpm -mzxn -ab -ozxn.bin z80.asm zxn.asm
mpm -mz380 -ab -oz380.bin -sha2 z80.asm z180.asm z380.asm

# compile test for directives and expressions
mpm -as directives.asm

# compile test for macro syntax
mpm -am macros.asm

# compile test for building a linkable library
mpm -xtest.lib du16 m16 m24

mpm -ab -Rz80 -ltest.lib ozcmd.asm
if test $? -eq 0; then
    # program compiled successfully, apply leading Z80 ELF header
    mpm -b -nMap -oozcmd ozcmd-elf.asm
fi