; ---------------------------------------------------------------------------------------------------
;
;   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
;    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
;    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
;    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
;    MMMM       MMMM     PPPP              MMMM       MMMM
;    MMMM       MMMM     PPPP              MMMM       MMMM
;   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
;
;                         ZZZZZZZZZZZZZZ     888888888888       000000000       NNNNN       NNNNN
;                       ZZZZZZZZZZZZZZ     8888888888888888    0000000000000     NNNNNN     NNNN
;                               ZZZZ       8888        8888  0000         0000   NNNNNNNN   NNNN
;                             ZZZZ           888888888888    0000         0000   NNNN NNNNN NNNN
;                           ZZZZ           8888        8888  0000         0000   NNNN   NNNNNNNN
;                         ZZZZZZZZZZZZZZ   8888888888888888    0000000000000     NNNN     NNNNNN
;                       ZZZZZZZZZZZZZZ       888888888888        000000000      NNNNNN      NNNNN
;
; Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
;
; This file is part of Mpm.
; Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
; Public License as published by the Free Software Foundation; either version 2, or (at your option)
; any later version.
; Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with Mpm;
; see the file COPYING.  If not, write to the
; Free Software Foundation, Inc., 59 Temple Place; Suite 330, Boston, MA 02111-1307, USA.
;
; ---------------------------------------------------------------------------------------------------
;
; ZXN (Z80N) CPU reference test for Mpm V1.9.4+
;
;  To see all opcodes, generate listing files: mpm -mzxn -va z80.asm zxn.asm
;  To generate a binary: mpm -mzxn -ozxn.bin -vab z80.asm zxn.asm
;  (-v is verbose compile, can be omitted)
; ---------------------------------------------------------------------------------------------------

module zxnref

; the ZXN (Z80N) specific instruction set


swap
swapnib

otib
outinb

pxdn
pixeldn
pxad
pixelad

setae

ldix
ldws
lddx
ldirx
lirx
lprx
ldpirx
ldrx
lddrx

test $91

mul d,e

; push <nn> in Most Significant Byte Format, and with optional multiple registers
push af,$8001,ix,iy

brlc de,b
bsla de,b
bsra de,b
bsrf de,b
bsrl de,b

mirr a
mirror a

jp (c)

nreg $01,$80
nreg $02,a
nextreg $03,$98
nextreg $04,a

add bc,a
add de,a
add hl,a

add bc,$4001
add de,$5001
add hl,$6001
