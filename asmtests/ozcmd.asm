; **************************************************************************************************
; Shell command test case for OZ v5
;
; Upload compiled 'ozcmd' binary to :EPR.0/bin then execute via Shell application.
; Compiled with buildtest.sh script.
;
; Copyright (C) 2022, Gunther Strube, hello@bits4fun.net
; ***************************************************************************************************

module OzShellCmd

lib m16

include "stdio.def"
include "integer.def"

; OZ & zELF shell command definitions
include "ozcmd.inc"

org EXEC_ORG


.entry
        call    initvars
        ld      hl,(hellostrptr)                ; this is address loading is to test the auto code relocation
        call    dispmsg
        ld      hl,(worldstrptr)
        call    dispmsg
        oz      OS_Nln

        ld      hl,multiply_msg
        call    dispmsg

        ld      hl, 32
        ld      de, 128
        call    m16                             ; call 16bit multiplication library routine
        ld      (hellostrptr),hl                ; result in (hellostrptr)
        ld      hl,0
        ld      (worldstrptr),hl                ; upper 16bit (of 32bit) is 0
        ld      hl,hellostrptr
        ld      de,strbuf
        push    de
        ld      a,1
        oz      GN_Pdn                          ; convert result to ascii
        xor     a
        ld      (de),a                          ; null-terminate integer ascii string
        pop     hl
        call    dispmsg                         ; write result of multiplication
        oz      OS_Nln

.exit
        oz      OS_Pout
        defm    "Press any key to exit.",0
        ld      bc,-1
        oz      OS_Kin                          ; wait for key press
        oz      OS_Nln
        ld      hl, 0                           ; SH_OK
        ret

.initvars
        ld      hl, hellostr
        ld      (hellostrptr),hl
        ld      hl, worldstr
        ld      (worldstrptr),hl
        ret
.dispmsg
        oz      OS_Sout
        ret

.hellostr
        defm    "Hello ",0
.worldstr
        defm    "World ",0
.multiply_msg
        defm    "32*128 = ",0

;------------------------------------------------------------------------------
; Workspace is located from here onwards (after end of program), RAM
.Workspace

.hellostrptr  defw 0
.worldstrptr  defw 0
.strbuf       defs 8

