#!/bin/bash

# *************************************************************************************
#
#  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
#   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
#   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
#   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#   MMMM       MMMM     PPPP              MMMM       MMMM
#  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
#
#  Copyright (C) 1991-2022, Gunther Strube, hello@bits4fun.net
#
#  This file is part of Mpm.
#  Mpm is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by the Free Software Foundation;
#  either version 2, or (at your option) any later version.
#  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License along with Mpm;
#  see the file COPYING.  If not, write to the
#  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# ---------------------------------------------------------------------------------------
#
# Compile script for Mpm Unit testing and code coverage on Linux platform with GCC and GCOV.
#
# Requirements (available in PATH):
#   git (for adding build string when using mpm -h)
#   gcc compiler
#   gcov tool
#   
# *************************************************************************************


# get the relative path to this script
MPM_PATH=$PWD

command -v git >/dev/null 2>&1 || { echo "git utility was not found on system!" >&2; exit 1; }

# output current Git revision as a 32bit hex string, to be included as part of Ozvm release information
git log --pretty=format:'#define MPM_BUILDMSG "%h"%n' -n 1 > src/build.h

GCC_FOUND=1
command -v gcc >/dev/null 2>&1 || { GCC_FOUND=0; }

if [ $GCC_FOUND -eq 0 ]; then
    echo "GCC compiler was not found on system!" >&2;
    exit 1
fi

GCOV_FOUND=1
command -v gcov >/dev/null 2>&1 || { GCOV_FOUND=0; }

if [ $GCOV_FOUND -eq 0 ]; then
    echo "GCOV tool was not found on system!" >&2;
    exit 1
fi

# delete any previous compile output
./cleanup.sh

cd src

# compile Mpm sources with code coverage...
gcc -o mpm -DUNIX=1 -std=c99 -g -coverage -Wall -Wextra -Wno-format-truncation -pedantic -lgcov *.c
if test $? -gt 0; then
	echo "gcc failed compiling Mpm sources!"
else
    # get the path to where Mpm was built
    MPM_PATH=$PWD
    cd ../asmtests

    # execute generic assembler tests to collect code coverage (*.gcda files)
    echo "Executing tests to collect GCOV statistics..."

    # run the test of no command line arguments
    $MPM_PATH/mpm > /dev/null 2>&1

    # run the test of rendering version information to console
    $MPM_PATH/mpm -version > /dev/null 2>&1

    # run the test of rendering options page to console
    $MPM_PATH/mpm -h > /dev/null 2>&1

    # compile tests for all available CPU's
    $MPM_PATH/mpm -mz80 -vab -crc32 z80.asm > /dev/null 2>&1
    $MPM_PATH/mpm -mz180 -vab -oz180.bin -ihex z80.asm z180.asm > /dev/null 2>&1
    $MPM_PATH/mpm -mzxn -vab -ozxn.bin z80.asm zxn.asm > /dev/null 2>&1
    $MPM_PATH/mpm -mz380 -vab -oz380.bin -sha2 z80.asm z180.asm z380.asm > /dev/null 2>&1

    # compile test for directive and expression syntax
    $MPM_PATH/mpm -vas directives.asm > /dev/null 2>&1

    # compile test for macro syntax
    $MPM_PATH/mpm -vam macros.asm > /dev/null 2>&1

    # compile test for building a linkable library
    $MPM_PATH/mpm -v -xtest.lib du16 m16 m24  > /dev/null 2>&1

    $MPM_PATH/mpm -vajb -Rz80 -ltest.lib ozcmd.asm  > /dev/null 2>&1
    if test $? -eq 0; then
        # program compiled successfully, apply leading Z80 ELF header
        $MPM_PATH/mpm -b -Map -oozcmd ozcmd-elf.asm
    fi

    # output the generated binaries
    ls -l *.lib *.obj *.bin ozcmd

    cd ../src

    # generate gcov files (original source code with coverage statistics embedded)
    gcov *.gcda
    echo "GCOV files generated."
fi

cd ..
